package br.com.javanei.emulation.onlinedb.thegamesdb;

import java.util.LinkedHashMap;
import java.util.Map;

public final class TheGamesDBConstant {
    public static final String BASE_API_URL = "http://thegamesdb.net/api/";
    public static final String BASE_GAME_URL = "http://thegamesdb.net/game/";

    public static final Map<String, String> getConnectionHeader() {
        Map<String, String> props = new LinkedHashMap<>();
        //props.put("Content-Type", "application/json; charset=UTF-8");
        //props.put("Accept", "text/xml; charset=utf-8");
        props.put("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; pt-BR; rv:1.9.2b5) Gecko/20091204 Firefox/3.6b5");
        return props;
    }
}
