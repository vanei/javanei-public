package br.com.javanei.emulation.game.parser;

import br.com.javanei.emulation.game.GameProcessingMessage;
import br.com.javanei.emulation.game.GameProcessingResult;
import java.io.File;
import java.util.concurrent.Callable;

public abstract class AbstractGameParser implements Callable<GameProcessingResult> {
    protected File srcFile;
    protected GameProcessingResult result = new GameProcessingResult();
    protected GameParserMonitor monitor;

    public AbstractGameParser(File srcFile, GameParserMonitor monitor) {
        this.srcFile = srcFile;
        this.monitor = monitor;
    }

    public AbstractGameParser(File srcFile) {
        this(srcFile, new DefaultMonitor());
    }

    protected void dispatchMessage(GameProcessingMessage message, int percent) {
        this.monitor.onMessage(message, percent);
    }

    protected void dispatchMessage(String msg, int percent) {
        this.monitor.onMessage(new GameProcessingMessage(GameProcessingMessage.Type.INFO, msg), percent);
    }

    protected void dispatchWarnMessage(String msg, int percent) {
        this.monitor.onMessage(new GameProcessingMessage(GameProcessingMessage.Type.WARN, msg), percent);
    }

    protected Exception dispatchErrorMessage(Exception ex, int percent) {
        this.monitor.onMessage(new GameProcessingMessage(GameProcessingMessage.Type.ERROR, ex.getMessage()), percent);
        return ex;
    }

    private static class DefaultMonitor implements GameParserMonitor {
    }
}
