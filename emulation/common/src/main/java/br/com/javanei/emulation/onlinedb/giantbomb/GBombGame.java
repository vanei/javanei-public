package br.com.javanei.emulation.onlinedb.giantbomb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "game")
public class GBombGame implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String deck;
    private String description;
    private String apiDetailUrl;
    private String dateAdded;
    private String dateLastUpdated;
    private List<String> aliases = new LinkedList<>();
    private String expectedReleaseMonth;
    private String expectedReleaseQuarter;
    private String expectedReleaseYear;
    private String expectedReleaseDay;
    //image
    private Integer numberOfUserReviews;
    //original_game_rating
    private String originalReleaseDate;
    //platforms
    private String siteDetailUrl;
    private List<GBombPlatform> platforms = new LinkedList<>();
    private List<GBombGenre> genres = new LinkedList<>();

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApiDetailUrl() {
        return apiDetailUrl;
    }

    public void setApiDetailUrl(String apiDetailUrl) {
        this.apiDetailUrl = apiDetailUrl;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(String dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public void addAlias(String alias) {
        this.aliases.add(alias);
    }

    public String getExpectedReleaseMonth() {
        return expectedReleaseMonth;
    }

    public void setExpectedReleaseMonth(String expectedReleaseMonth) {
        this.expectedReleaseMonth = expectedReleaseMonth;
    }

    public String getExpectedReleaseQuarter() {
        return expectedReleaseQuarter;
    }

    public void setExpectedReleaseQuarter(String expectedReleaseQuarter) {
        this.expectedReleaseQuarter = expectedReleaseQuarter;
    }

    public String getExpectedReleaseYear() {
        return expectedReleaseYear;
    }

    public void setExpectedReleaseYear(String expectedReleaseYear) {
        this.expectedReleaseYear = expectedReleaseYear;
    }

    public String getExpectedReleaseDay() {
        return expectedReleaseDay;
    }

    public void setExpectedReleaseDay(String expectedReleaseDay) {
        this.expectedReleaseDay = expectedReleaseDay;
    }

    public Integer getNumberOfUserReviews() {
        return numberOfUserReviews;
    }

    public void setNumberOfUserReviews(Integer numberOfUserReviews) {
        this.numberOfUserReviews = numberOfUserReviews;
    }

    public String getOriginalReleaseDate() {
        return originalReleaseDate;
    }

    public void setOriginalReleaseDate(String originalReleaseDate) {
        this.originalReleaseDate = originalReleaseDate;
    }

    public String getSiteDetailUrl() {
        return siteDetailUrl;
    }

    public void setSiteDetailUrl(String siteDetailUrl) {
        this.siteDetailUrl = siteDetailUrl;
    }

    public List<GBombPlatform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<GBombPlatform> platforms) {
        this.platforms = platforms;
    }

    public void addPlatform(GBombPlatform platform) {
        this.platforms.add(platform);
    }

    public List<GBombGenre> getGenres() {
        return genres;
    }

    public void setGenres(List<GBombGenre> genres) {
        this.genres = genres;
    }

    public void addGenre(GBombGenre genre) {
        this.genres.add(genre);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<game>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, deck, "deck");
        appendIfNotNull(sb, description, "description");
        appendIfNotNull(sb, apiDetailUrl, "apiDetailUrl");
        appendIfNotNull(sb, dateAdded, "dateAdded");
        appendIfNotNull(sb, dateLastUpdated, "dateLastUpdated");
        appendIfNotNull(sb, expectedReleaseDay, "expectedReleaseDay");
        appendIfNotNull(sb, expectedReleaseMonth, "expectedReleaseMonth");
        appendIfNotNull(sb, expectedReleaseQuarter, "expectedReleaseQuarter");
        appendIfNotNull(sb, expectedReleaseYear, "expectedReleaseYear");
        appendIfNotNull(sb, numberOfUserReviews, "numberOfUserReviews");
        appendIfNotNull(sb, originalReleaseDate, "originalReleaseDate");
        appendIfNotNull(sb, siteDetailUrl, "siteDetailUrl");
        if (!this.aliases.isEmpty()) {
            sb.append("<aliases>");
            for (int i = 0; i < this.aliases.size(); i++) {
                if (i > 0) sb.append("\n");
                sb.append(this.aliases.get(i));
            }
            sb.append("</aliases>");
        }
        if (!this.platforms.isEmpty()) {
            sb.append("<platforms>");
            for (int i = 0; i < this.platforms.size(); i++) {
                sb.append(this.platforms.get(i));
            }
            sb.append("</platforms>");
        }
        if (!this.genres.isEmpty()) {
            sb.append("<genres>");
            for (int i = 0; i < this.genres.size(); i++) {
                sb.append(this.genres.get(i));
            }
            sb.append("</genres>");
        }
        sb.append("</game>");
        return sb.toString();
    }
}
