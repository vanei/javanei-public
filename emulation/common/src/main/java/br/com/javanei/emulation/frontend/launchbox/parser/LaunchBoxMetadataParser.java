package br.com.javanei.emulation.frontend.launchbox.parser;

import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadata;
import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadataAlternateName;
import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadataEmulator;
import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadataEmulatorPlatform;
import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadataGame;
import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadataGameImage;
import br.com.javanei.emulation.frontend.launchbox.metadata.LaunchBoxMetadataPlatform;
import br.com.javanei.emulation.game.GameProcessingMessage;
import br.com.javanei.emulation.game.GameProcessingResult;
import br.com.javanei.emulation.game.parser.AbstractGameParser;
import br.com.javanei.emulation.game.parser.GameParserMonitor;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LaunchBoxMetadataParser extends AbstractGameParser {
    private int iPercentComplete = 0;
    private List<String> invalidTags = new ArrayList<>();

    public LaunchBoxMetadataParser(File srcFile) {
        super(srcFile);
    }

    public LaunchBoxMetadataParser(File srcFile, GameParserMonitor monitor) {
        super(srcFile, monitor);
    }

    public static void main(String[] args) {
        //FutureTask t1 = new FutureTask(new LaunchBoxMetadataParser(new File("F:/Emulation/LaunchBox/Metadata/Metadata.xml"),
        //        new LauchBoxMetadataParserMonitor()));
        FutureTask t1 = new FutureTask(new LaunchBoxMetadataParser(new File("F:/Emulation/LaunchBox/Metadata/Metadata.xml")));
        ExecutorService executor = Executors.newFixedThreadPool(1);

        try {
            executor.execute(t1);
            GameProcessingResult result = (GameProcessingResult) t1.get();
            LaunchBoxMetadata metadata = (LaunchBoxMetadata) result.getResult();

            List<String> plats = metadata.getPlatformsAsString();
            System.out.println("****************************************************");
            System.out.println("Total de plataformas: " + plats.size());
            System.out.println("****************************************************");
            for (String s : plats) {
                List<LaunchBoxMetadataGame> games = metadata.getGamesByPlatform(s);
                System.out.println("*** " + s + ": " + games.size() + " games");
            }
            System.out.println("****************************************************");
            System.out.println("=== Total: " + metadata.getGames().size());
            System.out.println("****************************************************");
            List<LaunchBoxMetadataGame> games = metadata.getGamesByPlatform("Nintendo Entertainment System");
            System.out.println("****** Jogos de Nintendo: ");
            for (LaunchBoxMetadataGame g : games) {
                System.out.println(g.getName());
            }
            System.out.println("****** TOTAL: " + games.size());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    @Override
    public GameProcessingResult call() throws Exception {
        if (!srcFile.exists()) {
            throw dispatchErrorMessage(new FileNotFoundException(srcFile.getAbsolutePath()), 0);
        }

        LaunchBoxMetadata metadata = new LaunchBoxMetadata();
        this.result.setResult(metadata);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        dispatchMessage("Parsing file [" + srcFile + "]", 0);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(this.srcFile);
        NodeList nodes = doc.getChildNodes();
        dispatchMessage("Processing xml", 0);

        for (int i = 0; i < nodes.getLength(); i++) {
            Node nLB = nodes.item(i);
            if (nLB.getChildNodes().getLength() == 0)
                continue;
            NodeList lbChildList = nLB.getChildNodes();
            int iTotalGames = lbChildList.getLength();
            dispatchMessage("@@@@@@@@@@@ Total de tags: " + iTotalGames, iPercentComplete);

            for (int j = 0; j < lbChildList.getLength(); j++) {
                iPercentComplete = (j * 100 / iTotalGames);
                dispatchMessage("Parsing tag line " + (j + 1) + " of " + iTotalGames, iPercentComplete);

                Node gameNode = lbChildList.item(j);
                if (gameNode.getNodeName().equalsIgnoreCase("Game")) {
                    metadata.addGame(parseGame(gameNode));
                } else if (gameNode.getNodeName().equalsIgnoreCase("Platform")) {
                    metadata.addPlatform(parsePlatform(gameNode));
                } else if (gameNode.getNodeName().equalsIgnoreCase("PlatformAlternateName")) {
                    metadata.addAlternateName(parseAlternateName(gameNode));
                } else if (gameNode.getNodeName().equalsIgnoreCase("Emulator")) {
                    metadata.addEmulator(parseEmulator(gameNode));
                } else if (gameNode.getNodeName().equalsIgnoreCase("EmulatorPlatform")) {
                    metadata.addEmulatorPlatform(parseEmulatorPlatform(gameNode));
                } else if (gameNode.getNodeName().equalsIgnoreCase("GameImage")) {
                    metadata.addGameImage(parseGameImage(gameNode));
                }
            }
        }

        return this.result;
    }

    private LaunchBoxMetadataPlatform parsePlatform(Node pNode) {
        LaunchBoxMetadataPlatform result = new LaunchBoxMetadataPlatform();
        NodeList attrs = pNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "name":
                            result.setName(node.getTextContent().trim());
                            break;
                        case "emulated":
                            result.setEmulated(node.getTextContent().trim());
                            break;
                        case "releasedate":
                            result.setReleaseDate(node.getTextContent().trim());
                            break;
                        case "developer":
                            result.setDeveloper(node.getTextContent().trim());
                            break;
                        case "manufacturer":
                            result.setManufacturer(node.getTextContent().trim());
                            break;
                        case "cpu":
                            result.setCpu(node.getTextContent().trim());
                            break;
                        case "memory":
                            result.setMemory(node.getTextContent().trim());
                            break;
                        case "graphics":
                            result.setGraphics(node.getTextContent().trim());
                            break;
                        case "sound":
                            result.setSound(node.getTextContent().trim());
                            break;
                        case "display":
                            result.setDisplay(node.getTextContent().trim());
                            break;
                        case "media":
                            result.setMedia(node.getTextContent().trim());
                            break;
                        case "maxcontrollers":
                            result.setMaxControllers(node.getTextContent().trim());
                            break;
                        case "notes":
                            result.setNotes(node.getTextContent().trim());
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains("Platform/" + node.getNodeName())) {
                                this.invalidTags.add("Platform/" + node.getNodeName());
                                System.err.println("Tag não tratada: Platform/" + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return result;
    }

    private LaunchBoxMetadataAlternateName parseAlternateName(Node pNode) {
        LaunchBoxMetadataAlternateName result = new LaunchBoxMetadataAlternateName();
        NodeList attrs = pNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "name":
                            result.setName(node.getTextContent().trim());
                            break;
                        case "alternate":
                            result.setAlternate(node.getTextContent().trim());
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains("AlternateName/" + node.getNodeName())) {
                                this.invalidTags.add("AlternateName/" + node.getNodeName());
                                System.err.println("Tag não tratada: AlternateName/" + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return result;
    }

    private LaunchBoxMetadataEmulator parseEmulator(Node pNode) {
        LaunchBoxMetadataEmulator result = new LaunchBoxMetadataEmulator();
        NodeList attrs = pNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "name":
                            result.setName(node.getTextContent().trim());
                            break;
                        case "applicablefileextensions":
                            result.setApplicableFileExtensions(node.getTextContent().trim());
                            break;
                        case "url":
                            result.setURL(node.getTextContent().trim());
                            break;
                        case "binaryfilename":
                            result.setBinaryFileName(node.getTextContent().trim());
                            break;
                        case "noquotes":
                            result.setNoQuotes(node.getTextContent().trim());
                            break;
                        case "nospace":
                            result.setNoSpace(node.getTextContent().trim());
                            break;
                        case "hideconsole":
                            result.setHideConsole(node.getTextContent().trim());
                            break;
                        case "filenameonly":
                            result.setFileNameOnly(node.getTextContent().trim());
                            break;
                        case "autoextract":
                            result.setAutoExtract(node.getTextContent().trim());
                            break;
                        case "commandline":
                            result.setCommandLine(node.getTextContent().trim());
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains("Emulator/" + node.getNodeName())) {
                                this.invalidTags.add("Emulator/" + node.getNodeName());
                                System.err.println("Tag não tratada: Emulator/" + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return result;
    }

    private LaunchBoxMetadataEmulatorPlatform parseEmulatorPlatform(Node pNode) {
        LaunchBoxMetadataEmulatorPlatform result = new LaunchBoxMetadataEmulatorPlatform();
        NodeList attrs = pNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "emulator":
                            result.setEmulator(node.getTextContent().trim());
                            break;
                        case "platform":
                            result.setPlatform(node.getTextContent().trim());
                            break;
                        case "applicablefileextensions":
                            result.setApplicableFileExtensions(node.getTextContent().trim());
                            break;
                        case "recommended":
                            result.setRecommended(node.getTextContent().trim());
                            break;
                        case "commandline":
                            result.setCommandLine(node.getTextContent().trim());
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains("EmulatorPlatform/" + node.getNodeName())) {
                                this.invalidTags.add("EmulatorPlatform/" + node.getNodeName());
                                System.err.println("Tag não tratada: EmulatorPlatform/" + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return result;
    }

    private LaunchBoxMetadataGameImage parseGameImage(Node pNode) {
        LaunchBoxMetadataGameImage result = new LaunchBoxMetadataGameImage();
        NodeList attrs = pNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "databaseid":
                            result.setDatabaseID(node.getTextContent().trim());
                            break;
                        case "filename":
                            result.setFileName(node.getTextContent().trim());
                            break;
                        case "type":
                            result.setType(node.getTextContent().trim());
                            break;
                        case "region":
                            result.setRegion(node.getTextContent().trim());
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains("GameImage/" + node.getNodeName())) {
                                this.invalidTags.add("GameImage/" + node.getNodeName());
                                System.err.println("Tag não tratada: GameImage/" + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return result;
    }

    private LaunchBoxMetadataGame parseGame(Node gameNode) {
        LaunchBoxMetadataGame game = new LaunchBoxMetadataGame();
        NodeList attrs = gameNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "name":
                            game.setName(node.getTextContent().trim());
                            break;
                        case "overview":
                            if (node.getTextContent() != null)
                                game.setOverview(node.getTextContent().trim());
                            break;
                        case "maxplayers":
                            if (node.getTextContent() != null)
                                game.setMaxPlayers(node.getTextContent().trim());
                            break;
                        case "cooperative":
                            if (node.getTextContent() != null)
                                game.setCooperative(node.getTextContent().trim());
                            break;
                        case "databaseid":
                            if (node.getTextContent() != null)
                                game.setDatabaseID(node.getTextContent().trim());
                            break;
                        case "communityrating":
                            if (node.getTextContent() != null)
                                game.setCommunityRating(node.getTextContent().trim());
                            break;
                        case "platform":
                            game.setPlatform(node.getTextContent().trim());
                            break;
                        case "genres":
                            if (node.getTextContent() != null)
                                game.setGenres(node.getTextContent().trim());
                            break;
                        case "publisher":
                            if (node.getTextContent() != null)
                                game.setPublisher(node.getTextContent().trim());
                            break;
                        case "developer":
                            if (node.getTextContent() != null)
                                game.setDeveloper(node.getTextContent().trim());
                            break;
                        case "releasedate":
                            if (node.getTextContent() != null)
                                game.setReleaseDate(node.getTextContent().trim());
                            break;
                        case "esrb":
                            if (node.getTextContent() != null)
                                game.setEsrb(node.getTextContent().trim());
                            break;
                        case "videourl":
                            if (node.getTextContent() != null)
                                game.setVideoURL(node.getTextContent().trim());
                            break;
                        case "releaseyear":
                            if (node.getTextContent() != null)
                                game.setReleaseYear(node.getTextContent().trim());
                            break;
                        case "wikipediaurl":
                            if (node.getTextContent() != null)
                                game.setWikipediaURL(node.getTextContent().trim());
                            break;
                        case "dos":
                            if (node.getTextContent() != null)
                                game.setDos(node.getTextContent().trim());
                            break;
                        case "startupfile":
                            if (node.getTextContent() != null)
                                game.setStartupFile(node.getTextContent().trim());
                            break;
                        case "startupmd5":
                            if (node.getTextContent() != null)
                                game.setStartupMD5(node.getTextContent().trim());
                            break;
                        case "startupparameters":
                            if (node.getTextContent() != null)
                                game.setStartupParameters(node.getTextContent().trim());
                            break;
                        case "setupfile":
                            if (node.getTextContent() != null)
                                game.setSetupFile(node.getTextContent().trim());
                            break;
                        case "setupmd5":
                            if (node.getTextContent() != null)
                                game.setSetupMD5(node.getTextContent().trim());
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains("Game/" + node.getNodeName())) {
                                this.invalidTags.add("Game/" + node.getNodeName());
                                System.err.println("Tag não tratada: Game/" + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return game;
    }

    private static class LauchBoxMetadataParserMonitor implements GameParserMonitor {
        @Override
        public void onMessage(GameProcessingMessage message, int percent) {
            System.out.println(percent + "%, " + message.toString());
        }
    }
}
