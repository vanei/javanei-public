package br.com.javanei.emulation.mame.common.mamesoftwarelist;

import java.io.Serializable;

public class MameSoftwareDipvalue implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String value;
    private Boolean _default; // (yes|no) "no";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getDefault() {
        return _default;
    }

    public void setDefault(Boolean _default) {
        this._default = _default;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\t\t\t\t\t<dipvalue name=\"").append(this.name).append("\" value=\"").append(this.value).append("\"");
        if (this._default != null) {
            sb.append(" default=\"").append(this._default).append("\"");
        }
        sb.append("/>").append(System.getProperty("line.separator"));

        return sb.toString();
    }
}
