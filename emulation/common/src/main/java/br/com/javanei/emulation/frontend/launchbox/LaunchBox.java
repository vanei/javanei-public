package br.com.javanei.emulation.frontend.launchbox;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LaunchBox implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<LaunchBoxGame> games = new LinkedList<>();
    private Map<String, List<LaunchBoxGame>> gamesByPlatform = new LinkedHashMap<>();

    public List<LaunchBoxGame> getGames() {
        return games;
    }

    public void setGames(List<LaunchBoxGame> games) {
        for (LaunchBoxGame g : games) {
            this.addGame(g);
        }
    }

    public void addGame(LaunchBoxGame game) {
        List<LaunchBoxGame> l = gamesByPlatform.get(game.getPlatform());
        if (l == null) {
            l = new LinkedList<>();
            this.gamesByPlatform.put(game.getPlatform(), l);
        }
        if (!l.contains(game)) {
            games.add(game);
            l.add(game);
        }
    }

    public List<LaunchBoxGame> getGamesByPlatform(String platform) {
        return this.gamesByPlatform.get(platform);
    }

    public List<String> getPlatforms() {
        List<String> result = new LinkedList<>();
        result.addAll(this.gamesByPlatform.keySet());
        return result;
    }
}
