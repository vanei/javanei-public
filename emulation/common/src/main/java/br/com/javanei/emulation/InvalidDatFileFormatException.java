package br.com.javanei.emulation;

public class InvalidDatFileFormatException extends EmulationException {

    private static final long serialVersionUID = 1L;

    public InvalidDatFileFormatException(String format) {
        super(format);
    }
}
