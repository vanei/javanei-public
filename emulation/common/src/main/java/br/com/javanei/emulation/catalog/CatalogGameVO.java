package br.com.javanei.emulation.catalog;

import br.com.javanei.emulation.game.common.ThreeStatesEnum;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "game")
public class CatalogGameVO implements Serializable, Comparable<CatalogGameVO> {

    private static final long serialVersionUID = 1L;
    private final Set<CatalogGameFileVO> roms = new HashSet<>();
    private String name;
    private String mainName;
    private String description; //ClrMamePro
    private String version;
    private int year;
    private Set<CatalogGameLanguage> languages = new HashSet<>();
    private Set<CatalogGameRegion> regions = new HashSet<>();
    private CatalogGamePublisher publisher;
    private CatalogGameDeveloper developer;
    private CatalogGameProtection protection;
    private CatalogGameLoader loader;
    //private ThreeStatesEnum alternate = ThreeStatesEnum.Unknown; //GoodTools
    private String alternate;
    private String compilation;
    private Set<String> complements = new HashSet<>();
    private ThreeStatesEnum badDump = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum fixed = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum hack = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum overdump = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum pirate = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum trained = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum oldTranslation = ThreeStatesEnum.Unknown; // GoodTools
    private ThreeStatesEnum newerTranslation = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum verifiedGoodDump = ThreeStatesEnum.Unknown; //GoodTools
    private ThreeStatesEnum unlicensed = ThreeStatesEnum.Unknown; //GoodTools
    private String catalogVersion;
    private String catalogName;
    private CatalogGameEnum catalog;
    private String catalogFile;
    private String proto; //No-Intro
    private String beta; //No-Intro
    private Boolean demo = Boolean.FALSE; //No-Intro
    private Boolean promo = Boolean.FALSE; //No-Intro
    private Boolean sample = Boolean.FALSE; //No-Intro
    private Boolean preview = Boolean.FALSE; //No-Intro
    private String serial;

    //private String license;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainName() {
        return mainName;
    }

    public void setMainName(String mainName) {
        this.mainName = mainName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Set<CatalogGameLanguage> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<CatalogGameLanguage> languages) {
        if (languages != null) {
            this.languages = languages;
        } else {
            this.languages = new HashSet<>();
        }
    }

    public void addLanguages(List<CatalogGameLanguage> languages) {
        if (languages == null) {
            this.languages = new HashSet<>();
        }
        this.languages.addAll(languages);
    }

    public Set<CatalogGameRegion> getRegions() {
        return this.regions;
    }

    public void setRegions(Set<CatalogGameRegion> regions) {
        if (regions != null) {
            this.regions = regions;
        } else {
            this.regions = new HashSet<>();
        }
    }

    public void addRegions(List<CatalogGameRegion> regions) {
        if (regions == null) {
            this.regions = new HashSet<>();
        }
        this.regions.addAll(regions);
    }

    public void addRegion(CatalogGameRegion region) {
        this.regions.add(region);
    }

    public CatalogGamePublisher getPublisher() {
        return publisher;
    }

    public void setPublisher(CatalogGamePublisher publisher) {
        this.publisher = publisher;
    }

    public CatalogGameDeveloper getDeveloper() {
        return developer;
    }

    public void setDeveloper(CatalogGameDeveloper developer) {
        this.developer = developer;
    }

    public CatalogGameProtection getProtection() {
        return protection;
    }

    public void setProtection(CatalogGameProtection protection) {
        this.protection = protection;
    }

    public CatalogGameLoader getLoader() {
        return loader;
    }

    public void setLoader(CatalogGameLoader loader) {
        this.loader = loader;
    }

    public String getAlternate() {
        return alternate;
    }

    public void setAlternate(String alternate) {
        this.alternate = alternate;
    }

    public String getCompilation() {
        return compilation;
    }

    public void setCompilation(String compilation) {
        this.compilation = compilation;
    }

    public Set<String> getComplements() {
        return complements;
    }

    public void setComplements(Set<String> complements) {
        this.complements = complements;
    }

    public void addComplement(String complement) {
        this.complements.add(complement);
    }

    public ThreeStatesEnum getBadDump() {
        return badDump;
    }

    public void setBadDump(ThreeStatesEnum badDump) {
        this.badDump = badDump;
    }

    public ThreeStatesEnum getFixed() {
        return fixed;
    }

    public void setFixed(ThreeStatesEnum fixed) {
        this.fixed = fixed;
    }

    public ThreeStatesEnum getHack() {
        return hack;
    }

    public void setHack(ThreeStatesEnum hack) {
        this.hack = hack;
    }

    public ThreeStatesEnum getOverdump() {
        return overdump;
    }

    public void setOverdump(ThreeStatesEnum overdump) {
        this.overdump = overdump;
    }

    public ThreeStatesEnum getPirate() {
        return pirate;
    }

    public void setPirate(ThreeStatesEnum pirate) {
        this.pirate = pirate;
    }

    public ThreeStatesEnum getTrained() {
        return trained;
    }

    public void setTrained(ThreeStatesEnum trained) {
        this.trained = trained;
    }

    public ThreeStatesEnum getOldTranslation() {
        return oldTranslation;
    }

    public void setOldTranslation(ThreeStatesEnum oldTranslation) {
        this.oldTranslation = oldTranslation;
    }

    public ThreeStatesEnum getNewerTranslation() {
        return newerTranslation;
    }

    public void setNewerTranslation(ThreeStatesEnum newerTranslation) {
        this.newerTranslation = newerTranslation;
    }

    public ThreeStatesEnum getVerifiedGoodDump() {
        return verifiedGoodDump;
    }

    public void setVerifiedGoodDump(ThreeStatesEnum verifiedGoodDump) {
        this.verifiedGoodDump = verifiedGoodDump;
    }

    public ThreeStatesEnum getUnlicensed() {
        return unlicensed;
    }

    public void setUnlicensed(ThreeStatesEnum unlicensed) {
        this.unlicensed = unlicensed;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(String catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public CatalogGameEnum getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogGameEnum catalog) {
        this.catalog = catalog;
    }

    public String getCatalogFile() {
        return catalogFile;
    }

    public void setCatalogFile(String catalogFile) {
        this.catalogFile = catalogFile;
    }

    public String getProto() {
        return proto;
    }

    public boolean isProto() {
        return this.proto != null;
    }

    public void setProto(String proto) {
        this.proto = proto;
    }

    public String getProtoVersion() {
        return this.proto != null && this.proto.length() > 5 ? this.proto.substring(5).trim() : "";
    }

    public String getBeta() {
        return beta;
    }

    public boolean isBeta() {
        return this.beta != null;
    }

    public void setBeta(String beta) {
        this.beta = beta;
    }

    public String getBetaVersion() {
        return this.beta != null && this.beta.length() > 4 ? this.beta.substring(4).trim() : "";
    }

    public Boolean getDemo() {
        return demo;
    }

    public void setDemo(Boolean demo) {
        this.demo = demo;
    }

    public Boolean getPromo() {
        return promo;
    }

    public void setPromo(Boolean promo) {
        this.promo = promo;
    }

    public Boolean getSample() {
        return sample;
    }

    public void setSample(Boolean sample) {
        this.sample = sample;
    }

    public Boolean getPreview() {
        return preview;
    }

    public void setPreview(Boolean preview) {
        this.preview = preview;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Set<CatalogGameFileVO> getRoms() {
        return roms;
    }

    public void addRom(CatalogGameFileVO rom) {
        if (!this.roms.contains(rom)) {
            this.roms.add(rom);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.catalogVersion);
        hash = 67 * hash + Objects.hashCode(this.catalog);
        hash = 67 * hash + Objects.hashCode(this.catalogName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatalogGameVO other = (CatalogGameVO) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.catalogVersion, other.catalogVersion)) {
            return false;
        }
        if (!Objects.equals(this.catalogName, other.catalogName)) {
            return false;
        }
        if (this.catalog != other.catalog) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(CatalogGameVO o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t<game");
        this.appendIfNoNull(sb, "name", name);
        this.appendIfNoNull(sb, "mainName", mainName);
        this.appendIfNoNull(sb, "description", description);
        this.appendIfNoNull(sb, "version", version);
        if (this.year > 0) {
            sb.append(" year=\"").append(this.year).append("\"");
        }
        if (this.languages != null && !this.languages.isEmpty()) {
            sb.append(" language=\"");
            int i = 0;
            for (CatalogGameLanguage l : this.languages) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(l.toString());
                i++;
            }
            sb.append("\"");
        }
        if (!this.regions.isEmpty()) {
            sb.append(" region=\"");
            int i = 0;
            for (CatalogGameRegion r : this.regions) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(r.toString());
                i++;
            }
            sb.append("\"");
        }
        if (this.publisher != null) {
            sb.append(" publisher=\"").append(this.publisher).append("\"");
        }
        if (this.developer != null) {
            sb.append(" developer=\"").append(this.developer).append("\"");
        }
        if (this.protection != null) {
            sb.append(" protection=\"").append(this.protection).append("\"");
        }
        if (this.loader != null) {
            sb.append(" loader=\"").append(this.loader).append("\"");
        }
        if (this.catalog != null) {
            sb.append(" catalog=\"").append(this.catalog).append("\"");
        }
        this.appendIfNoNull(sb, "catalogVersion", catalogVersion);
        this.appendIfNoNull(sb, "catalogName", catalogName);
        this.appendIfNoNull(sb, "catalogFile", catalogFile);
        this.appendIfNoNull(sb, "alternate", alternate);
        this.appendIfNoNull(sb, "compilation", compilation);
        if (!this.complements.isEmpty()) {
            sb.append(" complement=\"");
            int i = 0;
            for (String s : this.complements) {
                if (i > 0) {
                    sb.append("\t");
                }
                sb.append(s);
                i++;
            }
            sb.append("\"");
        }
        this.appendIfNoNull(sb, "badDump", badDump);
        this.appendIfNoNull(sb, "fixed", fixed);
        this.appendIfNoNull(sb, "hack", hack);
        this.appendIfNoNull(sb, "overdump", overdump);
        this.appendIfNoNull(sb, "pirate", pirate);
        this.appendIfNoNull(sb, "trained", trained);
        this.appendIfNoNull(sb, "oldTranslation", oldTranslation);
        this.appendIfNoNull(sb, "newerTranslation", newerTranslation);
        this.appendIfNoNull(sb, "verifiedGoodDump", verifiedGoodDump);
        this.appendIfNoNull(sb, "unlicensed", unlicensed);
        this.appendIfNoNull(sb, "proto", proto);
        this.appendIfNoNull(sb, "beta", beta);
        this.appendIfNoNull(sb, "demo", demo);
        this.appendIfNoNull(sb, "promo", promo);
        this.appendIfNoNull(sb, "sample", sample);
        this.appendIfNoNull(sb, "preview", preview);
        this.appendIfNoNull(sb, "serial", serial);
        sb.append(">\n");
        this.roms.stream().forEach((rom) -> {
            sb.append("\t\t").append(rom.toString());
        });
        sb.append("\t</game>\n");
        return sb.toString();
    }

    private void appendIfNoNull(StringBuilder sb, String name, String value) {
        if (value != null) {
            sb.append(" ").append(name).append("=\"").append(value).append("\"");
        }
    }

    private void appendIfNoNull(StringBuilder sb, String name, ThreeStatesEnum value) {
        if (value != null && !value.equals(ThreeStatesEnum.Unknown)) {
            sb.append(" ").append(name).append("=\"").append(value).append("\"");
        }
    }

    private void appendIfNoNull(StringBuilder sb, String name, Boolean value) {
        if (null != value && value == Boolean.TRUE) {
            sb.append(" ").append(name).append("=\"").append(value).append("\"");
        }
    }
}
