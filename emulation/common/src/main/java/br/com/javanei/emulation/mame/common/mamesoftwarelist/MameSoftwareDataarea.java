package br.com.javanei.emulation.mame.common.mamesoftwarelist;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class MameSoftwareDataarea implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String size;
    private String databits; // (8|16|32|64) "8"
    private String endian; // (big|little) "little"
    private List<MameSoftwareRom> roms = new LinkedList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDatabits() {
        return databits;
    }

    public void setDatabits(String databits) {
        this.databits = databits;
    }

    public String getEndian() {
        return endian;
    }

    public void setEndian(String endian) {
        this.endian = endian;
    }

    public List<MameSoftwareRom> getRoms() {
        return roms;
    }

    public void setRoms(List<MameSoftwareRom> roms) {
        this.roms = roms != null ? roms : new LinkedList<>();
    }

    public void addRom(MameSoftwareRom rom) {
        this.roms.add(rom);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t\t\t<dataarea name=\"").append(this.name).append("\"");
        if (this.size != null) {
            sb.append(" size=\"").append(this.size).append("\"");
        }
        if (this.databits != null) {
            sb.append(" databits=\"").append(this.databits).append("\"");
        }
        if (this.endian != null) {
            sb.append(" endian=\"").append(this.endian).append("\"");
        }
        sb.append(">").append(System.getProperty("line.separator"));

        for (MameSoftwareRom rom : this.roms) {
            sb.append(rom);
        }
        sb.append("\t\t\t\t</dataarea>").append(System.getProperty("line.separator"));

        return sb.toString();
    }
}
