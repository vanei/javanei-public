package br.com.javanei.emulation;

public class UnknownTagException extends EmulationException {

    private static final long serialVersionUID = 1L;

    public UnknownTagException(String extension) {
        //TODO: Traduzir mensagem
        super(extension);
    }
}
