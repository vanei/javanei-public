package br.com.javanei.emulation.frontend.launchbox.metadata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LaunchBoxMetadata implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<LaunchBoxMetadataGame> games = new LinkedList<>();
    private Map<String, List<LaunchBoxMetadataGame>> gamesByPlatform = new LinkedHashMap<>();
    private List<LaunchBoxMetadataGameImage> gameImages = new LinkedList<>();
    private List<LaunchBoxMetadataEmulatorPlatform> emulatorPlatforms = new LinkedList<>();
    private List<LaunchBoxMetadataEmulator> emulators = new LinkedList<>();
    private List<LaunchBoxMetadataAlternateName> alternateNames = new LinkedList<>();
    private List<LaunchBoxMetadataPlatform> platforms = new LinkedList<>();

    public List<LaunchBoxMetadataGame> getGames() {
        return games;
    }

    public void setGames(List<LaunchBoxMetadataGame> games) {
        for (LaunchBoxMetadataGame g : games) {
            this.addGame(g);
        }
    }

    public void addGame(LaunchBoxMetadataGame game) {
        List<LaunchBoxMetadataGame> gs = this.gamesByPlatform.get(game.getPlatform());
        if (gs == null) {
            gs = new LinkedList<>();
            this.gamesByPlatform.put(game.getPlatform(), gs);
        }
        if (!gs.contains(game)) {
            gs.add(game);
            this.games.add(game);
        }
    }

    public List<LaunchBoxMetadataGame> getGamesByPlatform(String platform) {
        return this.gamesByPlatform.get(platform);
    }

    public List<String> getPlatformsAsString() {
        List<String> result = new LinkedList<>();
        result.addAll(this.gamesByPlatform.keySet());
        return result;
    }

    public List<LaunchBoxMetadataGameImage> getGameImages() {
        return gameImages;
    }

    public void setGameImages(List<LaunchBoxMetadataGameImage> gameImages) {
        this.gameImages = gameImages;
    }

    public void addGameImage(LaunchBoxMetadataGameImage gameImage) {
        this.gameImages.add(gameImage);
    }

    public List<LaunchBoxMetadataEmulatorPlatform> getEmulatorPlatforms() {
        return emulatorPlatforms;
    }

    public void setEmulatorPlatforms(List<LaunchBoxMetadataEmulatorPlatform> emulatorPlatforms) {
        this.emulatorPlatforms = emulatorPlatforms;
    }

    public void addEmulatorPlatform(LaunchBoxMetadataEmulatorPlatform emulatorPlatform) {
        this.emulatorPlatforms.add(emulatorPlatform);
    }

    public List<LaunchBoxMetadataEmulator> getEmulators() {
        return emulators;
    }

    public void setEmulators(List<LaunchBoxMetadataEmulator> emulators) {
        this.emulators = emulators;
    }

    public void addEmulator(LaunchBoxMetadataEmulator emulator) {
        this.emulators.add(emulator);
    }

    public List<LaunchBoxMetadataAlternateName> getAlternateNames() {
        return alternateNames;
    }

    public void setAlternateNames(List<LaunchBoxMetadataAlternateName> alternateNames) {
        this.alternateNames = alternateNames;
    }

    public void addAlternateName(LaunchBoxMetadataAlternateName alternateName) {
        this.alternateNames.add(alternateName);
    }

    public List<LaunchBoxMetadataPlatform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<LaunchBoxMetadataPlatform> platforms) {
        this.platforms = platforms;
    }

    public void addPlatform(LaunchBoxMetadataPlatform platform) {
        this.platforms.add(platform);
    }

    public List<String> getRegions() {
        List<String> result = new ArrayList<>();
        for (LaunchBoxMetadataGameImage p : this.gameImages) {
            if (p.getRegion() != null && !result.contains(p.getRegion())) {
                result.add(p.getRegion());
            }
        }
        return result;
    }

    public List<String> getGenres() {
        List<String> result = new ArrayList<>();
        for (LaunchBoxMetadataGame p : this.games) {
            for (String s : p.getGenres()) {
                if (!result.contains(s)) {
                    result.add(s);
                }
            }
        }
        return result;
    }

    public List<String> getPublishers() {
        List<String> result = new ArrayList<>();
        for (LaunchBoxMetadataGame p : this.games) {
            if (p.getPublisher() != null && !result.contains(p.getPublisher())) {
                result.add(p.getPublisher());
            }
        }
        return result;
    }

    public List<String> getDevelopers() {
        List<String> result = new ArrayList<>();
        for (LaunchBoxMetadataGame p : this.games) {
            if (p.getDeveloper() != null && !result.contains(p.getDeveloper())) {
                result.add(p.getDeveloper());
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" standalone=\"yes\"?>\n");
        sb.append("<LaunchBox>\n");
        for (LaunchBoxMetadataGame game : this.games) {
            sb.append(game.toString());
        }
        for (LaunchBoxMetadataPlatform platform : this.platforms) {
            sb.append(platform.toString());
        }
        for (LaunchBoxMetadataAlternateName name : this.alternateNames) {
            sb.append(name.toString());
        }
        for (LaunchBoxMetadataEmulator emulator : this.emulators) {
            sb.append(emulator.toString());
        }
        for (LaunchBoxMetadataEmulatorPlatform platform : this.emulatorPlatforms) {
            sb.append(platform.toString());
        }
        for (LaunchBoxMetadataGameImage image : this.gameImages) {
            sb.append(image.toString());
        }
        sb.append("</LaunchBox>");
        return sb.toString();
    }
}
