package br.com.javanei.emulation.onlinedb.igdb;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "release_date")
public class IGDBReleaseDate implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer category;
    private Integer platform;
    private Long date;
    private Integer region;

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getRegion() {
        return region;
    }

    public void setRegion(Integer region) {
        this.region = region;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<release_date>");
        appendIfNotNull(sb, category, "category");
        appendIfNotNull(sb, platform, "platform");
        appendIfNotNull(sb, date, "date");
        appendIfNotNull(sb, region, "region");
        sb.append("</release_date>");
        return sb.toString();
    }
}
