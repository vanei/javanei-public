package br.com.javanei.emulation.common;

import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "gamedatabase")
public class GameDatabaseVO implements Serializable, Comparable<GameDatabaseVO> {
    private static final long serialVersionUID = 1L;

    private String database;
    private String databaseId;
    private String gameName;
    private String url;
    private Boolean isMain = Boolean.FALSE;

    public GameDatabaseVO() {
    }

    public GameDatabaseVO(String database) {
        this.database = database;
    }

    public GameDatabaseVO(String database, String databaseId, String gameName) {
        this.database = database;
        this.databaseId = databaseId;
        this.gameName = gameName;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getMain() {
        return isMain;
    }

    public void setMain(Boolean main) {
        isMain = main;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameDatabaseVO that = (GameDatabaseVO) o;
        return Objects.equals(database, that.database) &&
                Objects.equals(databaseId, that.databaseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(database, databaseId);
    }

    @Override
    public int compareTo(GameDatabaseVO that) {
        if (this.database.compareTo(that.database) < 0) {
            return -1;
        } else if (this.database.compareTo(that.database) > 0) {
            return 1;
        }

        if (this.databaseId.compareTo(that.databaseId) < 0) {
            return -1;
        } else if (this.databaseId.compareTo(that.databaseId) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "GameDatabaseVO{" +
                "database='" + database + '\'' +
                ", databaseId='" + databaseId + '\'' +
                ", gameName='" + gameName + '\'' +
                ", url='" + url + '\'' +
                ", isMain='" + isMain + '\'' +
                '}';
    }
}
