package br.com.javanei.emulation.onlinedb.giantbomb.parser;

import br.com.javanei.common.util.HttpRequestResult;
import br.com.javanei.common.util.HttpUtil;
import br.com.javanei.emulation.onlinedb.giantbomb.GBombCompany;
import br.com.javanei.emulation.onlinedb.giantbomb.GBombGame;
import br.com.javanei.emulation.onlinedb.giantbomb.GBombGenre;
import br.com.javanei.emulation.onlinedb.giantbomb.GBombPlatform;
import br.com.javanei.emulation.onlinedb.giantbomb.GBombRegion;
import br.com.javanei.emulation.onlinedb.giantbomb.GiantBombConstant;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONArray;
import org.json.JSONObject;

public final class GiantBombParser {
    public static List<GBombPlatform> getPlatforms(String apiKey) throws Exception {
        List<GBombPlatform> result = new LinkedList<>();

        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.PLATFORMS_BASE_URL
                            + "?api_key=" + apiKey + "&format=json&limit=100&offset=" + offset,
                    null, null);
            JSONObject json = new JSONObject(new String(httpResult.getContent()));
            GBHeader header = parseHeader(json);
            if (header.statusCode != 1) {
                throw new Exception("GiantBomb Parser error: " + header.statusCode);
            }
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                GBombPlatform platform = parsePlatform(array.getJSONObject(i));
                result.add(platform);
            }
            offset += 100;
            if (offset >= header.numberOfTotalReults) {
                break;
            }
        }

        return result;
    }

    public static GBombPlatform getPlatform(String apiKey, Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.PLATFORM_BASE_URL
                        + id
                        + "?api_key=" + apiKey + "&format=json",
                null, null);
        JSONObject json = new JSONObject(new String(httpResult.getContent()));
        GBHeader header = parseHeader(json);
        if (header.statusCode != 1) {
            throw new Exception("GiantBomb Parser error: " + header.statusCode);
        }
        GBombPlatform result = parsePlatform(json.getJSONObject("results"));
        return result;
    }

    public static List<GBombGenre> getGenres(String apiKey) throws Exception {
        List<GBombGenre> result = new LinkedList<>();

        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.GENRES_BASE_URL
                            + "?api_key=" + apiKey + "&format=json&limit=100&offset=" + offset,
                    null, null);
            JSONObject json = new JSONObject(new String(httpResult.getContent()));
            GBHeader header = parseHeader(json);
            if (header.statusCode != 1) {
                throw new Exception("GiantBomb Parser error: " + header.statusCode);
            }
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                GBombGenre genre = parseGenre(array.getJSONObject(i));
                result.add(genre);
            }
            offset += 100;
            if (offset >= header.numberOfTotalReults) {
                break;
            }
        }

        return result;
    }

    public static GBombGenre getGenre(String apiKey, Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.GENRE_BASE_URL
                        + id
                        + "?api_key=" + apiKey + "&format=json",
                null, null);
        JSONObject json = new JSONObject(new String(httpResult.getContent()));
        GBHeader header = parseHeader(json);
        if (header.statusCode != 1) {
            throw new Exception("GiantBomb Parser error: " + header.statusCode);
        }
        GBombGenre result = parseGenre(json.getJSONObject("results"));
        return result;
    }

    public static List<GBombCompany> getCompanies(String apiKey) throws Exception {
        List<GBombCompany> result = new LinkedList<>();

        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.COMPANIES_BASE_URL
                            + "?api_key=" + apiKey + "&format=json&limit=100&offset=" + offset,
                    null, null);
            JSONObject json = new JSONObject(new String(httpResult.getContent()));
            GBHeader header = parseHeader(json);
            if (header.statusCode != 1) {
                throw new Exception("GiantBomb Parser error: " + header.statusCode);
            }
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                GBombCompany company = parseCompany(array.getJSONObject(i));
                result.add(company);
            }
            offset += 100;
            if (offset >= header.numberOfTotalReults) {
                break;
            }
        }

        return result;
    }

    public static GBombCompany getCompany(String apiKey, Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.COMPANY_BASE_URL
                        + id
                        + "?api_key=" + apiKey + "&format=json",
                null, null);
        JSONObject json = new JSONObject(new String(httpResult.getContent()));
        GBHeader header = parseHeader(json);
        if (header.statusCode != 1) {
            throw new Exception("GiantBomb Parser error: " + header.statusCode);
        }
        GBombCompany result = parseCompany(json.getJSONObject("results"));
        return result;
    }

    public static List<GBombRegion> getRegions(String apiKey) throws Exception {
        List<GBombRegion> result = new LinkedList<>();

        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.REGIONS_BASE_URL
                            + "?api_key=" + apiKey + "&format=json&limit=100&offset=" + offset,
                    null, null);
            JSONObject json = new JSONObject(new String(httpResult.getContent()));
            GBHeader header = parseHeader(json);
            if (header.statusCode != 1) {
                throw new Exception("GiantBomb Parser error: " + header.statusCode);
            }
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                GBombRegion region = parseRegion(array.getJSONObject(i));
                result.add(region);
            }
            offset += 100;
            if (offset >= header.numberOfTotalReults) {
                break;
            }
        }

        return result;
    }

    public static GBombRegion getRegion(String apiKey, Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.REGION_BASE_URL
                        + id
                        + "?api_key=" + apiKey + "&format=json",
                null, null);
        JSONObject json = new JSONObject(new String(httpResult.getContent()));
        GBHeader header = parseHeader(json);
        if (header.statusCode != 1) {
            throw new Exception("GiantBomb Parser error: " + header.statusCode);
        }
        GBombRegion result = parseRegion(json.getJSONObject("results"));
        return result;
    }

    public static List<GBombGame> getGamesByPlatform(String apiKey, Long platformId) throws Exception {
        List<GBombGame> result = new LinkedList<>();

        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.GAMES_BASE_URL
                            + "?api_key=" + apiKey + "&format=json&limit=100&offset=" + offset
                            + "&platforms=" + platformId,
                    null, null);
            JSONObject json = new JSONObject(new String(httpResult.getContent()));
            GBHeader header = parseHeader(json);
            if (header.statusCode != 1) {
                throw new Exception("GiantBomb Parser error: " + header.statusCode);
            }
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                GBombGame game = parseGame(array.getJSONObject(i));
                result.add(game);
            }
            offset += 100;
            if (offset >= header.numberOfTotalReults) {
                break;
            }
        }

        return result;
    }

    public static GBombGame getGame(String apiKey, Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(GiantBombConstant.GAME_BASE_URL
                        + id
                        + "?api_key=" + apiKey + "&format=json",
                null, null);
        JSONObject json = new JSONObject(new String(httpResult.getContent()));
        GBHeader header = parseHeader(json);
        if (header.statusCode != 1) {
            throw new Exception("GiantBomb Parser error: " + header.statusCode);
        }
        GBombGame result = parseGame(json.getJSONObject("results"));
        return result;
    }

    private static GBHeader parseHeader(JSONObject json) {
        GBHeader header = new GBHeader();
        header.limit = json.getInt("limit");
        header.offset = json.getInt("offset");
        header.numberOfPageReults = json.getInt("number_of_page_results");
        header.numberOfTotalReults = json.getInt("number_of_total_results");
        header.statusCode = json.getInt("status_code");
        return header;
    }

    private static GBombCompany parseCompany(JSONObject json) {
        GBombCompany result = new GBombCompany();
        for (Object key : json.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    result.setId(json.getLong("id"));
                    break;
                case "name":
                    result.setName(json.getString("name"));
                    break;
                case "api_detail_url":
                    result.setApiDetailUrl(json.getString("api_detail_url"));
                    break;
                case "abbreviation":
                    result.setAbbreviation(getString(json, "abbreviation"));
                    break;
                case "aliases":
                    if (json.get("aliases").getClass() == String.class) {
                        StringTokenizer st = new StringTokenizer(json.getString("aliases"), "\n");
                        while (st.hasMoreTokens()) {
                            String s = st.nextToken();
                            result.addAlias(s);
                        }
                    }
                    break;
                case "date_added":
                    result.setDateAdded(json.getString("date_added"));
                    break;
                case "date_founded":
                    result.setDateFounded(getString(json, "date_founded"));
                    break;
                case "deck":
                    result.setDeck(getString(json, "deck"));
                    break;
                case "description":
                    result.setDescription(getString(json, "description"));
                    break;
                case "location_address":
                    result.setLocationAddress(getString(json, "location_address"));
                    break;
                case "location_city":
                    result.setLocationCity(getString(json, "location_city"));
                    break;
                case "location_country":
                    result.setLocationCountry(getString(json, "location_country"));
                    break;
                case "location_state":
                    result.setLocationState(getString(json, "location_state"));
                    break;
                case "phone":
                    result.setPhone(getString(json, "phone"));
                    break;
                case "site_detail_url":
                    result.setSiteDetailUrl(getString(json, "site_detail_url"));
                    break;
                case "website":
                    result.setWebsite(getString(json, "website"));
                    break;
                case "objects":
                case "people":
                case "developed_games":
                case "characters":
                case "date_last_updated":
                case "image":
                case "concepts":
                case "published_games":
                case "locations":
                    //TODO:
                    break;
                default:
                    System.err.println("EEEEEEEE Tag nao tratada company/" + key);
                    break;
            }
        }
        return result;
    }

    private static GBombPlatform parsePlatform(JSONObject json) {
        GBombPlatform platform = new GBombPlatform();
        for (Object key : json.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "abbreviation":
                    platform.setAbbreviation(json.getString("abbreviation"));
                    break;
                case "api_detail_url":
                    platform.setApiDetailUrl(json.getString("api_detail_url"));
                    break;
                case "company":
                    JSONObject obj = getJsonObject(json, "company");
                    if (obj != null) {
                        platform.setCompany(parseCompany(obj));
                    }
                    break;
                case "date_added":
                    platform.setDateAdded(json.getString("date_added"));
                    break;
                case "date_last_updated":
                    platform.setDateLastUpdated(json.getString("date_last_updated"));
                    break;
                case "deck":
                    platform.setDeck(json.getString("deck"));
                    break;
                case "description":
                    platform.setDescription(getString(json, "description"));
                    break;
                case "id":
                    platform.setId(json.getLong("id"));
                    break;
                case "image":
                    //TODO:
                    break;
                case "install_base":
                    platform.setInstallBase(getString(json, "install_base"));
                    break;
                case "name":
                    platform.setName(json.getString("name"));
                    break;
                case "online_support":
                    platform.setOnlineSupport(json.getBoolean("online_support"));
                    break;
                case "original_price":
                    platform.setOriginalPrice(getString(json, "original_price"));
                    break;
                case "release_date":
                    platform.setReleaseDate(getString(json, "release_date"));
                    break;
                case "site_detail_url":
                    platform.setSiteDetailUrl(getString(json, "site_detail_url"));
                    break;
                case "aliases":
                    if (json.get("aliases").getClass() == String.class) {
                        StringTokenizer st = new StringTokenizer(json.getString("aliases"), "\n");
                        while (st.hasMoreTokens()) {
                            String s = st.nextToken();
                            platform.addAlias(s);
                        }
                    }
                    break;
                default:
                    System.err.println("EEEEEEE Tag nao tratada platforms/" + key);
                    break;
            }
        }
        return platform;
    }

    private static GBombGenre parseGenre(JSONObject json) {
        GBombGenre result = new GBombGenre();
        for (Object key : json.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    result.setId(json.getLong("id"));
                    break;
                case "name":
                    result.setName(json.getString("name"));
                    break;
                case "description":
                    result.setDescription(getString(json, "description"));
                    break;
                case "deck":
                    result.setDeck(getString(json, "deck"));
                    break;
                case "api_detail_url":
                    result.setApiDetailUrl(json.getString("api_detail_url"));
                    break;
                case "date_added":
                    result.setDateAdded(json.getString("date_added"));
                    break;
                case "date_last_updated":
                    result.setDateLastUpdated(json.getString("date_last_updated"));
                    break;
                case "image":
                    //TODO:
                    break;
                case "site_detail_url":
                    result.setSiteDetailUrl(getString(json, "site_detail_url"));
                    break;
                default:
                    System.err.println("EEEEEEE Tag nao tratada genre/" + key);
                    break;
            }
        }
        return result;
    }

    private static GBombRegion parseRegion(JSONObject json) {
        GBombRegion result = new GBombRegion();
        for (Object key : json.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    result.setId(json.getLong("id"));
                    break;
                case "name":
                    result.setName(json.getString("name"));
                    break;
                case "api_detail_url":
                    result.setApiDetailUrl(json.getString("api_detail_url"));
                    break;
                case "date_added":
                    result.setDateAdded(json.getString("date_added"));
                    break;
                case "date_last_updated":
                    result.setDateLastUpdated(json.getString("date_last_updated"));
                    break;
                case "deck":
                    result.setDeck(getString(json, "deck"));
                    break;
                case "description":
                    result.setDescription(getString(json, "description"));
                    break;
                case "site_detail_url":
                    result.setSiteDetailUrl(getString(json, "site_detail_url"));
                    break;
                case "rating_boards":
                    //TODO:
                    break;
                case "image":
                    //TODO:
                    break;
                default:
                    System.err.println("EEEEEEEE Tag nao tratada region/" + key);
                    break;
            }
        }
        return result;
    }

    private static GBombGame parseGame(JSONObject json) {
        GBombGame result = new GBombGame();
        for (Object key : json.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    result.setId(json.getLong("id"));
                    break;
                case "name":
                    result.setName(json.getString("name"));
                    break;
                case "api_detail_url":
                    result.setApiDetailUrl(json.getString("api_detail_url"));
                    break;
                case "date_added":
                    result.setDateAdded(json.getString("date_added"));
                    break;
                case "date_last_updated":
                    result.setDateLastUpdated(json.getString("date_last_updated"));
                    break;
                case "deck":
                    result.setDeck(getString(json, "deck"));
                    break;
                case "description":
                    result.setDescription(getString(json, "description"));
                    break;
                case "concepts":
                    //TODO: Array
                    break;
                case "locations":
                    //TODO: Array
                    break;
                case "expected_release_day":
                    result.setExpectedReleaseDay(getString(json, "expected_release_day"));
                    break;
                case "expected_release_month":
                    result.setExpectedReleaseMonth(getString(json, "expected_release_month"));
                    break;
                case "expected_release_quarter":
                    result.setExpectedReleaseQuarter(getString(json, "expected_release_quarter"));
                    break;
                case "first_appearance_objects":
                    //TODO: Array
                    break;
                case "first_appearance_concepts":
                    //TODO: Array
                    break;
                case "expected_release_year":
                    result.setExpectedReleaseYear(getString(json, "expected_release_year"));
                    break;
                case "number_of_user_reviews":
                    result.setNumberOfUserReviews(json.getInt("number_of_user_reviews"));
                    break;
                case "characters":
                    //TODO:
                    break;
                case "developers":
                    //TODO: Array
                    break;
                case "first_appearance_locations":
                    //TODO: Array
                    break;
                case "first_appearance_characters":
                    //TODO: Array
                    break;
                case "first_appearance_people":
                    //TODO: Array
                    break;
                case "franchises":
                    //TODO:
                    break;
                case "genres":
                    JSONArray aag = json.getJSONArray("genres");
                    if (aag != null && aag.length() > 0) {
                        for (int ia = 0; ia < aag.length(); ia++) {
                            result.addGenre(parseGenre(aag.getJSONObject(ia)));
                        }
                    }
                    break;
                case "people":
                    //TODO: Array
                    break;
                case "objects":
                    //TODO: Array
                    break;
                case "publishers":
                    //TODO: Array
                    break;
                case "similar_games":
                    //TODO:
                    break;
                case "killed_characters":
                    //TODO:
                    break;
                case "original_game_rating":
                    //TODO: Array
                    break;
                case "original_release_date":
                    result.setOriginalReleaseDate(getString(json, "original_release_date"));
                    break;
                case "platforms":
                    JSONArray aa = json.getJSONArray("platforms");
                    if (aa != null && aa.length() > 0) {
                        for (int ia = 0; ia < aa.length(); ia++) {
                            result.addPlatform(parsePlatform(aa.getJSONObject(ia)));
                        }
                    }
                    break;
                case "site_detail_url":
                    result.setSiteDetailUrl(getString(json, "site_detail_url"));
                    break;
                case "aliases":
                    if (json.get("aliases").getClass() == String.class) {
                        StringTokenizer st = new StringTokenizer(json.getString("aliases"), "\n");
                        while (st.hasMoreTokens()) {
                            String s = st.nextToken();
                            result.addAlias(s);
                        }
                    }
                    break;
                case "rating_boards":
                    //TODO:
                    break;
                case "images":
                    //TODO:
                    break;
                case "image":
                    //TODO:
                    break;
                case "videos":
                    //TODO:
                    break;
                default:
                    System.err.println("EEEEEEEE Tag nao tratada game/" + key);
                    break;
            }
        }
        return result;
    }

    private static String getString(JSONObject json, String key) {
        if (json.get(key).getClass() == String.class) {
            return json.getString(key);
        } else {
            return null;
        }
    }

    private static JSONObject getJsonObject(JSONObject json, String key) {
        if (json.get(key).getClass() == JSONObject.class) {
            return json.getJSONObject(key);
        } else {
            return null;
        }
    }

    private static class GBHeader {
        int limit;
        int offset;
        int numberOfPageReults;
        int numberOfTotalReults;
        int statusCode;

        @Override
        public String toString() {
            return "GBHeader{" +
                    "limit=" + limit +
                    ", offset=" + offset +
                    ", numberOfPageReults=" + numberOfPageReults +
                    ", numberOfTotalReults=" + numberOfTotalReults +
                    ", statusCode=" + statusCode +
                    '}';
        }
    }
}
