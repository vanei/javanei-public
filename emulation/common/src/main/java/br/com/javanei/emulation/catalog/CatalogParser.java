package br.com.javanei.emulation.catalog;

public abstract class CatalogParser {
    public abstract GameParserInfo execute() throws Exception;
}
