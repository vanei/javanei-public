package br.com.javanei.emulation.catalog;

import br.com.javanei.emulation.game.GameProcessingMessage;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class GameParserInfo {

    private final List<GameProcessingMessage> messages = new LinkedList<>();
    private String name;
    private String description;
    private String version;
    private String comment;
    private List<CatalogGameVO> games;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<CatalogGameVO> getGames() {
        return games;
    }

    public void setGames(List<CatalogGameVO> games) {
        this.games = games;
    }

    public List<GameProcessingMessage> getMessages() {
        return messages;
    }

    public void addMessage(GameProcessingMessage msg) {
        this.messages.add(msg);
    }

    public void addMessages(List<GameProcessingMessage> msgs) {
        this.messages.addAll(msgs);
    }

    public List<GameProcessingMessage> sortMessagesByType() {
        Collections.sort(this.messages, new Comparator<GameProcessingMessage>() {

            @Override
            public int compare(GameProcessingMessage o1, GameProcessingMessage o2) {
                int result = 0;
                if (o1.isError()) {
                    if (o2.isError()) {
                        result = 0;
                    } else {
                        result = 1;
                    }
                } else if (o1.isWarn()) {
                    if (o2.isError()) {
                        result = -1;
                    } else if (o2.isWarn()) {
                        result = 0;
                    } else {
                        result = 1;
                    }
                } else {
                    if (o2.isInfo()) {
                        result = 0;
                    } else {
                        result = -1;
                    }
                }
                return result;
            }
        });
        return this.messages;
    }

    @Override
    public String toString() {
        return "GameParserInfo {" + "name=" + name + ", description=" + description + ", version=" + version + ", comment=" + comment + ", games=" + games + '}';
    }
}
