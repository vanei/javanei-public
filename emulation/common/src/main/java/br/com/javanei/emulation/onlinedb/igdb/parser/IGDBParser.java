package br.com.javanei.emulation.onlinedb.igdb.parser;

import br.com.javanei.common.util.HttpRequestResult;
import br.com.javanei.common.util.HttpUtil;
import br.com.javanei.emulation.onlinedb.igdb.IGDBCompany;
import br.com.javanei.emulation.onlinedb.igdb.IGDBConstant;
import br.com.javanei.emulation.onlinedb.igdb.IGDBGame;
import br.com.javanei.emulation.onlinedb.igdb.IGDBGenre;
import br.com.javanei.emulation.onlinedb.igdb.IGDBPlatform;
import br.com.javanei.emulation.onlinedb.igdb.IGDBReleaseDate;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class IGDBParser {
    public static List<IGDBPlatform> getPlatforms(String apiKey) throws Exception {
        List<IGDBPlatform> result = new LinkedList<>();
        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.PLATFORMS_BASE_URL
                            + "?fields=id,name,generation,updated_at,created_at,slug,url,alternative_name,summary,website&limit=50&offset=" + offset,
                    IGDBConstant.getConnectionHeader(apiKey), null);
            JSONArray array = new JSONArray(new String(httpResult.getContent()));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                result.add(parsePlatform(obj));
            }
            if (array.length() < 50)
                break;
            offset += 50;
        }
        return result;
    }

    public static IGDBPlatform getPlatform(String apiKey, String id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.PLATFORMS_BASE_URL
                        + id + "?fields=*",
                IGDBConstant.getConnectionHeader(apiKey), null);
        JSONArray array = new JSONArray(new String(httpResult.getContent()));
        if (array.length() == 1) {
            JSONObject obj = array.getJSONObject(0);
            return parsePlatform(obj);
        }
        return null;
    }

    public static List<IGDBGenre> getGenres(String apiKey) throws Exception {
        List<IGDBGenre> result = new LinkedList<>();
        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.GENRES_BASE_URL + "?fields=id,name,updated_at,created_at,slug,url&limit=50&offset=" + offset,
                    IGDBConstant.getConnectionHeader(apiKey), null);
            JSONArray array = new JSONArray(new String(httpResult.getContent()));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                result.add(parseGenre(obj));
            }
            if (array.length() < 50)
                break;
            offset += 50;
        }
        return result;
    }

    public static IGDBGenre getGenre(String apiKey, String id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.GENRES_BASE_URL
                        + id + "?fields=id,name,updated_at,created_at,slug,url",
                IGDBConstant.getConnectionHeader(apiKey), null);
        JSONArray array = new JSONArray(new String(httpResult.getContent()));
        if (array.length() == 1) {
            JSONObject obj = array.getJSONObject(0);
            return parseGenre(obj);
        }
        return null;
    }

    public static IGDBGame getGame(String apiKey, String id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.GAMES_BASE_URL
                        + id + "?fields=*",
                IGDBConstant.getConnectionHeader(apiKey), null);
        JSONArray array = new JSONArray(new String(httpResult.getContent()));
        if (array.length() == 1) {
            JSONObject obj = array.getJSONObject(0);
            return parseGame(obj);
        }
        return null;
    }

    public static List<IGDBGame> getGamesByPlatform(String apiKey, Long platformId) throws Exception {
        List<IGDBGame> result = new LinkedList<>();
        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.GAMES_BASE_URL + "?fields=*&limit=50&offset=" + offset
                            + "&filter[release_dates.platform][eq]=" + platformId,
                    IGDBConstant.getConnectionHeader(apiKey), null);
            JSONArray array = new JSONArray(new String(httpResult.getContent()));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                result.add(parseGame(obj));
            }
            if (array.length() < 50)
                break;
            offset += 50;
        }
        return result;
    }

    public static List<IGDBCompany> getCompanies(String apiKey) throws Exception {
        List<IGDBCompany> result = new LinkedList<>();
        int offset = 0;
        while (true) {
            HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.COMPANY_BASE_URL + "?fields=*&limit=50&offset=" + offset,
                    IGDBConstant.getConnectionHeader(apiKey), null);
            JSONArray array = new JSONArray(new String(httpResult.getContent()));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                result.add(parseCompany(obj));
            }
            if (array.length() < 50)
                break;
            offset += 50;
        }
        return result;
    }

    public static IGDBCompany getCompany(String apiKey, Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(IGDBConstant.COMPANY_BASE_URL
                        + id + "?fields=*",
                IGDBConstant.getConnectionHeader(apiKey), null);
        JSONArray array = new JSONArray(new String(httpResult.getContent()));
        if (array.length() == 1) {
            JSONObject obj = array.getJSONObject(0);
            return parseCompany(obj);
        }
        return null;
    }

    private static IGDBPlatform parsePlatform(JSONObject obj) {
        IGDBPlatform platform = new IGDBPlatform();
        for (Object key : obj.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    platform.setId(obj.getLong(key.toString()));
                    break;
                case "name":
                    platform.setName(obj.get(key.toString()).toString());
                    break;
                case "generation":
                    platform.setGeneration(obj.getInt(key.toString()));
                    break;
                case "updated_at":
                    platform.setUpdatedAt(obj.getLong(key.toString()));
                    break;
                case "created_at":
                    platform.setCreatedAt(obj.getLong(key.toString()));
                    break;
                case "slug":
                    platform.setSlug(obj.get(key.toString()).toString());
                    break;
                case "url":
                    platform.setUrl(obj.get(key.toString()).toString());
                    break;
                case "alternative_name":
                    platform.setAlternativeName(obj.get(key.toString()).toString());
                    break;
                case "summary":
                    platform.setSummary(obj.get(key.toString()).toString());
                    break;
                case "website":
                    platform.setWebsite(obj.get(key.toString()).toString());
                    break;
                case "games":
                    break;
                default:
                    System.err.println("EEEEE Tag nao tratada: platform/" + key);
                    break;
            }
        }
        return platform;
    }

    private static IGDBGenre parseGenre(JSONObject obj) {
        IGDBGenre genre = new IGDBGenre();
        for (Object key : obj.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    genre.setId(obj.getLong(key.toString()));
                    break;
                case "name":
                    genre.setName(obj.get(key.toString()).toString());
                    break;
                case "updated_at":
                    genre.setUpdatedAt(obj.getLong(key.toString()));
                    break;
                case "created_at":
                    genre.setCreatedAt(obj.getLong(key.toString()));
                    break;
                case "slug":
                    genre.setSlug(obj.get(key.toString()).toString());
                    break;
                case "url":
                    genre.setUrl(obj.get(key.toString()).toString());
                    break;
                case "games":
                    break;
                default:
                    System.err.println("EEEEE Tag nao tratada: genre/" + key);
                    break;
            }
        }
        return genre;
    }

    private static IGDBGame parseGame(JSONObject obj) {
        IGDBGame game = new IGDBGame();
        for (Object key : obj.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    game.setId(obj.getLong(key.toString()));
                    break;
                case "name":
                    game.setName(obj.get(key.toString()).toString());
                    break;
                case "updated_at":
                    game.setUpdatedAt(obj.getLong(key.toString()));
                    break;
                case "created_at":
                    game.setCreatedAt(obj.getLong(key.toString()));
                    break;
                case "slug":
                    game.setSlug(obj.get(key.toString()).toString());
                    break;
                case "url":
                    game.setUrl(obj.get(key.toString()).toString());
                    break;
                case "summary":
                    game.setSummary(obj.get(key.toString()).toString());
                    break;
                case "category":
                    game.setCategory(obj.get(key.toString()).toString());
                    break;
                case "status":
                    game.setStatus(obj.getInt(key.toString()));
                    break;
                case "storyline":
                    game.setStoryline(obj.get(key.toString()).toString());
                    break;
                case "collection":
                    game.setCollection(obj.getLong(key.toString()));
                    break;
                case "hypes":
                    game.setHypes(obj.getLong(key.toString()));
                    break;
                case "rating":
                    game.setRating(obj.getDouble(key.toString()));
                    break;
                case "rating_count":
                    game.setRatingCount(obj.getInt(key.toString()));
                    break;
                case "developers":
                    JSONArray developers = obj.getJSONArray("developers");
                    for (int z = 0; z < developers.length(); z++) {
                        game.addDeveloper(developers.getInt(z));
                    }
                    break;
                case "publishers":
                    JSONArray publishers = obj.getJSONArray("publishers");
                    for (int z = 0; z < publishers.length(); z++) {
                        game.addPublisher(publishers.getInt(z));
                    }
                    break;
                case "alternative_names":
                    JSONArray alternative_names = obj.getJSONArray("alternative_names");
                    for (int z = 0; z < alternative_names.length(); z++) {
                        game.addAlternativeName(alternative_names.getJSONObject(z).getString("name"));
                    }
                    break;
                case "keywords":
                    JSONArray keywords = obj.getJSONArray("keywords");
                    for (int z = 0; z < keywords.length(); z++) {
                        game.addKeyword(keywords.getInt(z));
                    }
                    break;
                case "themes":
                    JSONArray themes = obj.getJSONArray("themes");
                    for (int z = 0; z < themes.length(); z++) {
                        game.addTheme(themes.getInt(z));
                    }
                    break;
                case "genres":
                    JSONArray genres = obj.getJSONArray("genres");
                    for (int z = 0; z < genres.length(); z++) {
                        game.addGenre(genres.getInt(z));
                    }
                    break;
                case "time_to_beat":
                    //TODO:
                    break;
                case "esrb":
                    //TODO:
                    break;
                case "pegi":
                    //TODO:
                    break;
                case "videos":
                    //TODO:
                    break;
                case "screenshots":
                    //TODO:
                    break;
                case "cover":
                    //TODO:
                    break;
                case "release_dates":
                    JSONArray release_dates = obj.getJSONArray("release_dates");
                    for (int z = 0; z < release_dates.length(); z++) {
                        JSONObject rd = release_dates.getJSONObject(z);
                        IGDBReleaseDate releaseDate = new IGDBReleaseDate();
                        for (Object rdKey : rd.keySet()) {
                            switch (rdKey.toString().toLowerCase()) {
                                case "category":
                                    releaseDate.setCategory(rd.getInt("category"));
                                    break;
                                case "platform":
                                    releaseDate.setPlatform(rd.getInt("platform"));
                                    break;
                                case "date":
                                    releaseDate.setDate(rd.getLong("date"));
                                    break;
                                case "region":
                                    releaseDate.setRegion(rd.getInt("region"));
                                    break;
                                default:
                                    System.err.println("EEEEEEE Tag nao tratada: game/release_dates/" + rdKey);
                                    break;
                            }
                        }
                        game.addReleaseDate(releaseDate);
                    }
                    break;
                case "game_engines":
                    //TODO:
                    break;
                case "aggregated_rating":
                    //TODO:
                    break;
                case "franchise":
                    //TODO:
                    break;
                default:
                    System.err.println("EEEEE Tag nao tratada: game/" + key);
                    break;
            }
        }
        return game;
    }

    private static IGDBCompany parseCompany(JSONObject obj) {
        IGDBCompany company = new IGDBCompany();
        for (Object key : obj.keySet()) {
            switch (key.toString().toLowerCase()) {
                case "id":
                    company.setId(obj.getLong(key.toString()));
                    break;
                case "name":
                    company.setName(obj.get(key.toString()).toString());
                    break;
                case "updated_at":
                    company.setUpdatedAt(obj.getLong(key.toString()));
                    break;
                case "created_at":
                    company.setCreatedAt(obj.getLong(key.toString()));
                    break;
                case "slug":
                    company.setSlug(obj.get(key.toString()).toString());
                    break;
                case "url":
                    company.setUrl(obj.get(key.toString()).toString());
                    break;
                case "start_date_category":
                    company.setStartDateCategory(obj.getInt(key.toString()));
                    break;
                case "change_date_category":
                    company.setChangeDateCategory(obj.getInt(key.toString()));
                    break;
                case "country":
                    company.setCountry(obj.getInt("country"));
                    break;
                case "website":
                    company.setWebsite(obj.getString("website"));
                    break;
                case "description":
                    company.setDescription(obj.getString("description"));
                    break;
                case "twitter":
                    company.setTwitter(obj.getString("twitter"));
                    break;
                case "logo":
                    //TODO: System.out.println("logo: " + obj.get("logo"));
                    break;
                case "start_date":
                    company.setStartDate(obj.getLong("start_date"));
                    break;
                case "change_date":
                    company.setChangeDate(obj.getLong("change_date"));
                    break;
                case "parent":
                    company.setParent(obj.getLong("parent"));
                    break;
                case "changed_company_id":
                    company.setChangedCompanyId(obj.getLong("changed_company_id"));
                    break;
                case "published":
                    JSONArray published = obj.getJSONArray("published");
                    for (int z = 0; z < published.length(); z++) {
                        company.addPublished(published.getLong(z));
                    }
                    break;
                case "developed":
                    JSONArray developed = obj.getJSONArray("developed");
                    for (int z = 0; z < developed.length(); z++) {
                        company.addDeveloped(developed.getLong(z));
                    }
                    break;
                default:
                    System.err.println("EEEEE Tag nao tratada: company/" + key);
                    break;
            }
        }
        return company;
    }
}
