package br.com.javanei.emulation.mame.common;

import java.io.Serializable;

public class MameDriver implements Serializable {
    private static final long serialVersionUID = 1L;

    private String status; // (good|imperfect|preliminary)
    private String emulation; // (good|imperfect|preliminary)
    private String color; // (good|imperfect|preliminary)
    private String sound; // (good|imperfect|preliminary)
    private String graphic; // (good|imperfect|preliminary)
    private String cocktail; // (good|imperfect|preliminary)
    private String protection; // (good|imperfect|preliminary)
    private String savestate; // (supported|unsupported)

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmulation() {
        return emulation;
    }

    public void setEmulation(String emulation) {
        this.emulation = emulation;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getGraphic() {
        return graphic;
    }

    public void setGraphic(String graphic) {
        this.graphic = graphic;
    }

    public String getCocktail() {
        return cocktail;
    }

    public void setCocktail(String cocktail) {
        this.cocktail = cocktail;
    }

    public String getProtection() {
        return protection;
    }

    public void setProtection(String protection) {
        this.protection = protection;
    }

    public String getSavestate() {
        return savestate;
    }

    public void setSavestate(String savestate) {
        this.savestate = savestate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t<driver");
        if (this.status != null) {
            sb.append(" status=\"").append(this.status).append("\"");
        }
        if (this.emulation != null) {
            sb.append(" emulation=\"").append(this.emulation).append("\"");
        }
        if (this.color != null) {
            sb.append(" color=\"").append(this.color).append("\"");
        }
        if (this.sound != null) {
            sb.append(" sound=\"").append(this.sound).append("\"");
        }
        if (this.graphic != null) {
            sb.append(" graphic=\"").append(this.graphic).append("\"");
        }
        if (this.cocktail != null) {
            sb.append(" cocktail=\"").append(this.cocktail).append("\"");
        }
        if (this.protection != null) {
            sb.append(" protection=\"").append(this.protection).append("\"");
        }
        if (this.savestate != null) {
            sb.append(" savestate=\"").append(this.savestate).append("\"");
        }
        sb.append("/>").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
