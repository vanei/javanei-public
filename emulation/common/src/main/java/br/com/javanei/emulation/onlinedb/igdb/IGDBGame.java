package br.com.javanei.emulation.onlinedb.igdb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "game")
public class IGDBGame implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String slug;
    private String url;
    private Long createdAt;
    private Long updatedAt;
    private String summary;
    private String storyline;
    private Long collection;
    private Long hypes;
    private List<Integer> developers = new LinkedList<>();
    private List<Integer> publishers = new LinkedList<>();
    private String category;
    private List<Integer> keywords = new LinkedList<>();
    private List<Integer> themes = new LinkedList<>();
    private List<Integer> genres = new LinkedList<>();
    private List<IGDBReleaseDate> releaseDates = new LinkedList<>();
    private List<String> alternativeNames = new LinkedList<>();
    //screenshots
    //videos
    //cover
    //time_to_beat
    //esrb
    //pegi
    private Integer status;
    private Double rating;
    private Integer ratingCount;

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStoryline() {
        return storyline;
    }

    public void setStoryline(String storyline) {
        this.storyline = storyline;
    }

    public Long getCollection() {
        return collection;
    }

    public void setCollection(Long collection) {
        this.collection = collection;
    }

    public Long getHypes() {
        return hypes;
    }

    public void setHypes(Long hypes) {
        this.hypes = hypes;
    }

    public List<Integer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Integer> developers) {
        this.developers = developers;
    }

    public void addDeveloper(Integer developer) {
        this.developers.add(developer);
    }

    public List<Integer> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Integer> publishers) {
        this.publishers = publishers;
    }

    public void addPublisher(Integer publisher) {
        this.publishers.add(publisher);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Integer> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Integer> keywords) {
        this.keywords = keywords;
    }

    public void addKeyword(Integer keyword) {
        this.keywords.add(keyword);
    }

    public List<Integer> getThemes() {
        return themes;
    }

    public void setThemes(List<Integer> themes) {
        this.themes = themes;
    }

    public void addTheme(Integer theme) {
        this.themes.add(theme);
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public void addGenre(Integer genre) {
        this.getGenres().add(genre);
    }

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public void addAlternativeName(String name) {
        this.alternativeNames.add(name);
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public List<IGDBReleaseDate> getReleaseDates() {
        return releaseDates;
    }

    public void setReleaseDates(List<IGDBReleaseDate> releaseDates) {
        this.releaseDates = releaseDates;
    }

    public void addReleaseDate(IGDBReleaseDate releaseDate) {
        this.releaseDates.add(releaseDate);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<game>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, slug, "slug");
        appendIfNotNull(sb, url, "url");
        appendIfNotNull(sb, createdAt, "createdAt");
        appendIfNotNull(sb, updatedAt, "updatedAt");
        appendIfNotNull(sb, summary, "summary");
        appendIfNotNull(sb, storyline, "storyline");
        appendIfNotNull(sb, collection, "collection");
        appendIfNotNull(sb, hypes, "hypes");
        appendIfNotNull(sb, category, "category");
        appendIfNotNull(sb, status, "status");
        appendIfNotNull(sb, rating, "rating");
        appendIfNotNull(sb, ratingCount, "ratingCount");
        if (!developers.isEmpty()) {
            sb.append("<developers>");
            for (int i = 0; i < developers.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(developers.get(i));
            }
            sb.append("</developers>");
        }
        if (!publishers.isEmpty()) {
            sb.append("<publishers>");
            for (int i = 0; i < publishers.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(publishers.get(i));
            }
            sb.append("</publishers>");
        }
        if (!keywords.isEmpty()) {
            sb.append("<keywords>");
            for (int i = 0; i < keywords.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(keywords.get(i));
            }
            sb.append("</keywords>");
        }
        if (!themes.isEmpty()) {
            sb.append("<themes>");
            for (int i = 0; i < themes.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(themes.get(i));
            }
            sb.append("</themes>");
        }
        if (!genres.isEmpty()) {
            sb.append("<genres>");
            for (int i = 0; i < genres.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(genres.get(i));
            }
            sb.append("</genres>");
        }
        if (!releaseDates.isEmpty()) {
            sb.append("<releaseDates>");
            for (int i = 0; i < releaseDates.size(); i++) {
                sb.append(releaseDates.get(i));
            }
            sb.append("</releaseDates>");
        }
        if (!alternativeNames.isEmpty()) {
            sb.append("<alternativeNames>");
            for (int i = 0; i < alternativeNames.size(); i++) {
                sb.append("<name>").append(alternativeNames.get(i)).append("</name>");
            }
            sb.append("</alternativeNames>");
        }
        sb.append("</game>");
        return sb.toString();
    }
}
