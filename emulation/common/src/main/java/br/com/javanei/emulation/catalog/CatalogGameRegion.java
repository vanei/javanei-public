package br.com.javanei.emulation.catalog;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "region")
public class CatalogGameRegion implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Map<String, CatalogGameRegion> regions = new HashMap<>();

    static {
        regions.put("USA", new CatalogGameRegion("USA")); //GoodSet
        regions.put("World", new CatalogGameRegion("World")); //GoodSet
        regions.put("Japan, USA", new CatalogGameRegion("Japan, USA")); //GoodSet
        regions.put("Europe", new CatalogGameRegion("Europe")); //GoodSet
        regions.put("Japan & Korea", new CatalogGameRegion("Japan & Korea")); //GoodSet
        regions.put("Australia", new CatalogGameRegion("Australia")); //GoodSet
        regions.put("China", new CatalogGameRegion("China")); //GoodSet
        regions.put("France", new CatalogGameRegion("France")); //GoodSet
        regions.put("French Canadian", new CatalogGameRegion("French Canadian")); //GoodSet
        regions.put("Finland", new CatalogGameRegion("Finland")); //GoodSet
        regions.put("Germany", new CatalogGameRegion("Germany")); //GoodSet
        regions.put("Greece", new CatalogGameRegion("Greece")); //GoodSet
        regions.put("Hong Kong", new CatalogGameRegion("Hong Kong")); //GoodSet
        regions.put("Holland", new CatalogGameRegion("Holland")); //GoodSet
        regions.put("USA & BrazilNTSC", new CatalogGameRegion("USA & BrazilNTSC")); //GoodSet
        regions.put("Japan", new CatalogGameRegion("Japan")); //GoodSet
        regions.put("Korea", new CatalogGameRegion("Korea")); //GoodSet
        regions.put("Netherlands", new CatalogGameRegion("Netherlands")); //GoodSet
        regions.put("Public Domain", new CatalogGameRegion("Public Domain")); //GoodSet
        regions.put("Spain", new CatalogGameRegion("Spain")); //GoodSet
        regions.put("Sweden", new CatalogGameRegion("Sweden")); //GoodSet
        regions.put("England", new CatalogGameRegion("England")); //GoodSet
        regions.put("Italy", new CatalogGameRegion("Italy")); //GoodSet
        regions.put("non USA", new CatalogGameRegion("non USA")); //GoodSet
        regions.put("Asia", new CatalogGameRegion("Asia")); //GoodSet
        regions.put("Unknown", new CatalogGameRegion("Unknown")); //GoodSet
        regions.put("Unlicensed", new CatalogGameRegion("Unlicensed")); //No-Intro
        //Unknown_Country("Unknown Country"), //GoodSet
    }

    private final String region;

    private CatalogGameRegion(String region) {
        this.region = region;
    }

    public static boolean isRegion(String names) {
        boolean result = true;
        for (String s : names.split(",")) {
            if (!regions.containsKey(s.trim())) {
                result = false;
            }
        }
        return result;
    }

    public static List<CatalogGameRegion> fromNames(String names) {
        List<CatalogGameRegion> result = new LinkedList<>();
        for (String s : names.split(",")) {
            CatalogGameRegion reg = regions.get(s.trim());
            if (reg == null) {
                //TODO: Criar uma exception
                throw new IllegalArgumentException(s.trim());
            }
            result.add(reg);
        }
        return result;
    }

    public static CatalogGameRegion getRegion(String name) {
        return regions.get(name);
    }

    public String getRegion() {
        return region;
    }

    @Override
    public String toString() {
        return this.region;
    }
}
