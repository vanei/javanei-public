package br.com.javanei.emulation.onlinedb.giantbomb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "platform")
public class GBombPlatform implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String abbreviation;
    private String apiDetailUrl;
    private GBombCompany company;
    private String dateAdded;
    private String dateLastUpdated;
    private String deck;
    private String description;
    //image
    private String installBase;
    private Boolean onlineSupport;
    private String originalPrice;
    private String releaseDate;
    private String siteDetailUrl;
    private List<String> aliases = new LinkedList<>();

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getApiDetailUrl() {
        return apiDetailUrl;
    }

    public void setApiDetailUrl(String apiDetailUrl) {
        this.apiDetailUrl = apiDetailUrl;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(String dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstallBase() {
        return installBase;
    }

    public void setInstallBase(String installBase) {
        this.installBase = installBase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOnlineSupport() {
        return onlineSupport;
    }

    public void setOnlineSupport(Boolean onlineSupport) {
        this.onlineSupport = onlineSupport;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getSiteDetailUrl() {
        return siteDetailUrl;
    }

    public void setSiteDetailUrl(String siteDetailUrl) {
        this.siteDetailUrl = siteDetailUrl;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public void addAlias(String alias) {
        this.aliases.add(alias);
    }

    public GBombCompany getCompany() {
        return company;
    }

    public void setCompany(GBombCompany company) {
        this.company = company;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<platform>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, abbreviation, "abbreviation");
        appendIfNotNull(sb, dateAdded, "dateAdded");
        appendIfNotNull(sb, dateLastUpdated, "dateLastUpdated");
        appendIfNotNull(sb, deck, "deck");
        appendIfNotNull(sb, description, "description");
        appendIfNotNull(sb, installBase, "installBase");
        appendIfNotNull(sb, onlineSupport, "onlineSupport");
        appendIfNotNull(sb, originalPrice, "originalPrice");
        appendIfNotNull(sb, releaseDate, "releaseDate");
        appendIfNotNull(sb, siteDetailUrl, "siteDetailUrl");
        if (!this.aliases.isEmpty()) {
            sb.append("<aliases>");
            for (int i = 0; i < this.aliases.size(); i++) {
                if (i > 0) sb.append("\n");
                sb.append(this.aliases.get(i));
            }
            sb.append("</aliases>");
        }
        if (this.company != null)
            sb.append(company.toString());
        sb.append("</platform>");
        return sb.toString();
    }
}
