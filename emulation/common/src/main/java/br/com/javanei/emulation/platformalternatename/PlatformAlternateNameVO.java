package br.com.javanei.emulation.platformalternatename;

import java.util.Objects;

public class PlatformAlternateNameVO {
    private String name;
    private String alternate;

    public PlatformAlternateNameVO() {
    }

    public PlatformAlternateNameVO(String name, String alternate) {
        this.name = name;
        this.alternate = alternate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlternate() {
        return alternate;
    }

    public void setAlternate(String alternate) {
        this.alternate = alternate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlatformAlternateNameVO that = (PlatformAlternateNameVO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(alternate, that.alternate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, alternate);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<platformAlternateName>");
        sb.append(" <name>").append(this.name).append("</name>");
        sb.append(" <alternate>").append(this.alternate).append("</alternate>");
        sb.append("</platformAlternateName>");
        return sb.toString();
    }

    public String toJSON() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"name\":\"").append(name).append("\",\"alternate\":\"").append(alternate).append("\"").append("}");
        return sb.toString();
    }
}
