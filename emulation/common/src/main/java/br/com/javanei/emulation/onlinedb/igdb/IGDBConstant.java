package br.com.javanei.emulation.onlinedb.igdb;

import java.util.LinkedHashMap;
import java.util.Map;

public class IGDBConstant {
    public static final String BASE_URL = "https://igdbcom-internet-game-database-v1.p.mashape.com/";
    public static final String PLATFORMS_BASE_URL = BASE_URL + "platforms/";
    public static final String GENRES_BASE_URL = BASE_URL + "genres/";
    public static final String GAMES_BASE_URL = BASE_URL + "games/";
    public static final String COMPANY_BASE_URL = BASE_URL + "companies/";

    public static final Map<String, String> getConnectionHeader(String apiKey) {
        Map<String, String> props = new LinkedHashMap<>();
        props.put("X-Mashape-Key", apiKey);
        return props;
    }
}
