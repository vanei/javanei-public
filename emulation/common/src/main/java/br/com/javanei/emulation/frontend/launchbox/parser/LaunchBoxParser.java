package br.com.javanei.emulation.frontend.launchbox.parser;

import br.com.javanei.emulation.frontend.launchbox.LaunchBox;
import br.com.javanei.emulation.frontend.launchbox.LaunchBoxGame;
import br.com.javanei.emulation.game.GameProcessingMessage;
import br.com.javanei.emulation.game.GameProcessingResult;
import br.com.javanei.emulation.game.parser.AbstractGameParser;
import br.com.javanei.emulation.game.parser.GameParserMonitor;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LaunchBoxParser extends AbstractGameParser {
    private int iPercentComplete = 0;
    private List<String> invalidTags = new ArrayList<>();

    public LaunchBoxParser(File srcFile) {
        super(srcFile);
    }

    public LaunchBoxParser(File srcFile, GameParserMonitor monitor) {
        super(srcFile, monitor);
    }

    public static void main(String[] args) {
        //FutureTask t1 = new FutureTask(new LaunchBoxMetadataParser(new File("F:/Emulation/LaunchBox/Metadata/Metadata.xml"),
        //        new LauchBoxParserMonitor()));
        FutureTask t1 = new FutureTask(new LaunchBoxParser(new File("F:/Emulation/LaunchBox/LaunchBox.xml")));
        ExecutorService executor = Executors.newFixedThreadPool(1);

        try {
            executor.execute(t1);
            GameProcessingResult result = (GameProcessingResult) t1.get();
            LaunchBox metadata = (LaunchBox) result.getResult();

            List<String> plats = metadata.getPlatforms();
            System.out.println("****************************************************");
            System.out.println("Total de plataformas: " + plats.size());
            System.out.println("****************************************************");
            for (String s : plats) {
                List<LaunchBoxGame> games = metadata.getGamesByPlatform(s);
                System.out.println("*** " + s + ": " + games.size() + " games");
            }
            System.out.println("****************************************************");
            System.out.println("=== Total: " + metadata.getGames().size());
            System.out.println("****************************************************");
            List<LaunchBoxGame> games = metadata.getGamesByPlatform("Nintendo Entertainment System");
            System.out.println("****** Jogos de Nintendo");
            for (LaunchBoxGame g : games) {
                System.out.println(g.getTitle());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    @Override
    public GameProcessingResult call() throws Exception {
        if (!srcFile.exists()) {
            throw dispatchErrorMessage(new FileNotFoundException(srcFile.getAbsolutePath()), 0);
        }

        LaunchBox metadata = new LaunchBox();
        this.result.setResult(metadata);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        dispatchMessage("Parsing file [" + srcFile + "]", 0);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(this.srcFile);
        NodeList nodes = doc.getChildNodes();
        dispatchMessage("Processing xml", 0);

        for (int i = 0; i < nodes.getLength(); i++) {
            Node nLB = nodes.item(i);
            if (nLB.getChildNodes().getLength() == 0)
                continue;
            NodeList lbChildList = nLB.getChildNodes();
            int iTotalGames = lbChildList.getLength();
            dispatchMessage("@@@@@@@@@@@ Total de tags: " + iTotalGames, iPercentComplete);

            for (int j = 0; j < lbChildList.getLength(); j++) {
                iPercentComplete = (j * 100 / iTotalGames);
                dispatchMessage("Parsing tag line " + (j + 1) + " of " + iTotalGames, iPercentComplete);

                Node gameNode = lbChildList.item(j);
                if (gameNode.getNodeName().equalsIgnoreCase("Game")) {
                    metadata.addGame(parseGame(gameNode));
                } else if (gameNode.getNodeName().equalsIgnoreCase("Settings")) {
                    //TODO:
                } else if (gameNode.getNodeName().equalsIgnoreCase("EmulatorPlatform")) {
                    //TODO:
                } else if (gameNode.getNodeName().equalsIgnoreCase("Platform")) {
                    //TODO:
                } else if (gameNode.getNodeName().equalsIgnoreCase("PlatformFolder")) {
                    //TODO:
                } else if (gameNode.getNodeName().equalsIgnoreCase("BigBoxSettings")) {
                    //TODO:
                } else if (gameNode.getNodeName().equalsIgnoreCase("Emulator")) {
                    //TODO:
                } else {
                    //TODO:
                    if (!gameNode.getNodeName().trim().equalsIgnoreCase("#text") && !this.invalidTags.contains(gameNode.getNodeName().trim())) {
                        this.invalidTags.add(gameNode.getNodeName().trim());
                        System.err.println("@@@ Tag principal nao tratada: " + gameNode.getNodeName().trim());
                    }
                }
            }
        }

        return this.result;
    }

    private LaunchBoxGame parseGame(Node gameNode) {
        LaunchBoxGame game = new LaunchBoxGame();
        NodeList attrs = gameNode.getChildNodes();
        if (attrs != null && attrs.getLength() > 0) {
            for (int i = 0; i < attrs.getLength(); i++) {
                Node node = attrs.item(i);
                if (node != null && node.getNodeName() != null) {
                    switch (node.getNodeName().toLowerCase()) {
                        case "title":
                            game.setTitle(node.getTextContent().trim());
                            break;
                        case "platform":
                            game.setPlatform(node.getTextContent().trim());
                            break;
                        case "applicationpath":
                            game.setApplicationPath(node.getTextContent().trim());
                            break;
                        case "commandline":
                            //TODO:
                            break;
                        case "completed":
                            //TODO:
                            break;
                        case "configurationcommandline":
                            //TODO:
                            break;
                        case "configurationpath":
                            //TODO:
                            break;
                        case "dateadded":
                            //TODO:
                            break;
                        case "datemodified":
                            //TODO:
                            break;
                        case "developer":
                            //TODO:
                            break;
                        case "dosboxconfigurationpath":
                            //TODO:
                            break;
                        case "emulator":
                            //TODO:
                            break;
                        case "favorite":
                            //TODO:
                            break;
                        case "id":
                            //TODO:
                            break;
                        case "manualpath":
                            //TODO:
                            break;
                        case "musicpath":
                            //TODO:
                            break;
                        case "notes":
                            //TODO:
                            break;
                        case "publisher":
                            //TODO:
                            break;
                        case "rating":
                            //TODO:
                            break;
                        case "releasedate":
                            //TODO:
                            break;
                        case "rootfolder":
                            //TODO:
                            break;
                        case "scummvmaspectcorrection":
                            //TODO:
                            break;
                        case "scummvmfullscreen":
                            //TODO:
                            break;
                        case "scummvmgamedatafolderpath":
                            //TODO:
                            break;
                        case "scummvmgametype":
                            //TODO:
                            break;
                        case "sorttitle":
                            //TODO:
                            break;
                        case "source":
                            //TODO:
                            break;
                        case "starrating":
                            //TODO:
                            break;
                        case "status":
                            //TODO:
                            break;
                        case "databaseid":
                            //TODO:
                            break;
                        case "wikipediaurl":
                            //TODO:
                            break;
                        case "usedosbox":
                            //TODO:
                            break;
                        case "usescummvm":
                            //TODO:
                            break;
                        case "version":
                            //TODO:
                            break;
                        case "series":
                            //TODO:
                            break;
                        case "playmode":
                            //TODO:
                            break;
                        case "region":
                            //TODO:
                            break;
                        case "playcount":
                            //TODO:
                            break;
                        case "portable":
                            //TODO:
                            break;
                        case "videopath":
                            //TODO:
                            break;
                        case "hide":
                            //TODO:
                            break;
                        case "broken":
                            //TODO:
                            break;
                        case "genre":
                            //TODO:
                            break;
                        case "lastplayeddate":
                            //TODO:
                            break;
                        case "#text":
                            // Ignora
                            break;
                        default:
                            if (!this.invalidTags.contains(node.getNodeName().trim())) {
                                this.invalidTags.add(node.getNodeName().trim());
                                System.err.println("Tag game não tratada: " + node.getNodeName());
                            }
                            break;
                    }
                }
            }
        }
        return game;
    }

    private static class LauchBoxParserMonitor implements GameParserMonitor {
        @Override
        public void onMessage(GameProcessingMessage message, int percent) {
            System.out.println(percent + "%, " + message.toString());
        }
    }
}
