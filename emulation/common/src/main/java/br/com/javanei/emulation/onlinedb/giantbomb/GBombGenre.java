package br.com.javanei.emulation.onlinedb.giantbomb;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "platform")
public class GBombGenre implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;
    private String deck;
    private String apiDetailUrl;
    private String dateAdded;
    private String dateLastUpdated;
    //image
    private String siteDetailUrl;

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getApiDetailUrl() {
        return apiDetailUrl;
    }

    public void setApiDetailUrl(String apiDetailUrl) {
        this.apiDetailUrl = apiDetailUrl;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(String dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public String getSiteDetailUrl() {
        return siteDetailUrl;
    }

    public void setSiteDetailUrl(String siteDetailUrl) {
        this.siteDetailUrl = siteDetailUrl;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<genre>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");

        appendIfNotNull(sb, description, "description");
        appendIfNotNull(sb, deck, "deck");
        appendIfNotNull(sb, apiDetailUrl, "apiDetailUrl");
        appendIfNotNull(sb, dateAdded, "dateAdded");
        appendIfNotNull(sb, dateLastUpdated, "dateLastUpdated");
        appendIfNotNull(sb, siteDetailUrl, "siteDetailUrl");
        sb.append("</genre>");
        return sb.toString();
    }
}
