package br.com.javanei.emulation.common;

import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "genre")
public class GenreVO implements Serializable, Comparable<GenreVO> {
    private static final long serialVersionUID = 1L;

    private String name;
    private Long igdbId;
    private Long giantbombId;

    public GenreVO() {
    }

    public GenreVO(String name) {
        this.name = name;
    }

    public GenreVO(String name, Long igdbId, Long giantbombId) {
        this.name = name;
        this.igdbId = igdbId;
        this.giantbombId = giantbombId;
    }

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIgdbId() {
        return igdbId;
    }

    public void setIgdbId(Long igdbId) {
        this.igdbId = igdbId;
    }

    public Long getGiantbombId() {
        return giantbombId;
    }

    public void setGiantbombId(Long giantbombId) {
        this.giantbombId = giantbombId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenreVO genreVO = (GenreVO) o;
        return Objects.equals(name, genreVO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(GenreVO that) {
        if (this.name.compareTo(that.name) < 0) {
            return -1;
        } else if (this.name.compareTo(that.name) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<genre>");
        appendIfNotNull(sb, this.name, "name");
        appendIfNotNull(sb, this.igdbId, "igdbId");
        appendIfNotNull(sb, this.giantbombId, "giantbombId");
        sb.append("</genre>");
        return sb.toString();
    }

    public String toJSON() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"name\":\"").append(this.name).append("\"");
        if (igdbId != null)
            sb.append(",\"igdbId\":\"").append(this.igdbId).append("\"");
        if (giantbombId != null)
            sb.append(",\"giantbombId\":\"").append(this.giantbombId).append("\"");
        sb.append("}");
        return sb.toString();
    }
}
