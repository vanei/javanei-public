package br.com.javanei.emulation.onlinedb.igdb;

public enum IGDBGameCategoryEnum {
    MainGame(0),
    DLC_Addon(1),
    Expansion(2),
    Bundle(3),
    StandaloneExpansion(4);

    private final Integer value;

    private IGDBGameCategoryEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
