package br.com.javanei.emulation.game;

public class GameNamingUtil {
    public static String normalizeName(String name) {
        if (name.toLowerCase().endsWith(", the")
                || name.toLowerCase().endsWith(",the")) {
            return "The " + name.substring(0, name.lastIndexOf(",")).trim();
        } else {
            int pos = name.toLowerCase().indexOf(", the");
            if (pos > 0) {
                return "The " + name.substring(0, pos) + name.substring(pos + 5);
            } else {
                pos = name.toLowerCase().indexOf(",the");
                if (pos > 0) {
                    return "The " + name.substring(0, pos) + name.substring(pos + 4);
                }
            }
        }
        return name;
    }

    public static String encodeName(String name) {
        name = name.toLowerCase();
        StringBuilder sb = new StringBuilder();
        for (String s : name.split(" ")) {
            switch (s) {
                //case "the":
                case "of":
                    //case "a":
                    //Ignora
                    break;
                default:
                    for (char c : s.toCharArray()) {
                        switch (c) {
                            case ':':
                            case '.':
                            case '-':
                            case '\'':
                            case '!':
                            case ',':
                            case '*':
                            case '+':
                                //Ignora:
                                break;
                            case '&':
                                sb.append("and");
                                break;
                            default:
                                sb.append(c);
                                break;
                        }
                    }
            }
        }
        return sb.toString();
    }
}
