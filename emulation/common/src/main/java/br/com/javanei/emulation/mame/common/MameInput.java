package br.com.javanei.emulation.mame.common;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class MameInput implements Serializable {
    private static final long serialVersionUID = 1L;

    private Boolean service; // (yes|no) "no"
    private Boolean tilt; // (yes|no) "no"
    private Integer players;
    private Integer coins;

    private List<MameInputControl> controls = new LinkedList<>();

    public Boolean getService() {
        return service;
    }

    public void setService(Boolean service) {
        this.service = service;
    }

    public void setService(String service) {
        if (service != null)
            this.service = service.equalsIgnoreCase("yes") || service.equalsIgnoreCase("true");
    }

    public Boolean getTilt() {
        return tilt;
    }

    public void setTilt(Boolean tilt) {
        this.tilt = tilt;
    }

    public void setTilt(String tilt) {
        if (tilt != null)
            this.tilt = tilt.equalsIgnoreCase("yes") || tilt.equalsIgnoreCase("true");
    }

    public Integer getPlayers() {
        return players;
    }

    public void setPlayers(Integer players) {
        this.players = players;
    }

    public void setPlayers(String players) {
        this.players = new Integer(players);
    }

    public Integer getCoins() {
        return coins;
    }

    public void setCoins(Integer coins) {
        this.coins = coins;
    }

    public void setCoins(String coins) {
        this.coins = new Integer(coins);
    }

    public List<MameInputControl> getControls() {
        return controls;
    }

    public void setControls(List<MameInputControl> controls) {
        this.controls = controls;
    }

    public void addControl(MameInputControl control) {
        this.controls.add(control);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t<input");
        //<input players="1" coins="2" service="yes" tilt="yes">
        if (this.players != null) {
            sb.append(" players=\"").append(this.players).append("\"");
        }
        if (this.coins != null) {
            sb.append(" coins=\"").append(this.coins).append("\"");
        }
        if (this.service != null) {
            sb.append(" service=\"").append(this.service ? "yes" : "no").append("\"");
        }
        if (this.tilt != null) {
            sb.append(" tilt=\"").append(this.tilt ? "yes" : "no").append("\"");
        }
        sb.append(">").append(Mame.LINE_SEPARATOR);

        for (MameInputControl control : this.controls) {
            sb.append(control.toString());
        }

        sb.append("\t\t</input>").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
