package br.com.javanei.emulation;

import java.io.Serializable;

public abstract class EmulationException extends Exception implements Serializable {
    public EmulationException() {
    }

    public EmulationException(String message) {
        super(message);
    }

    public EmulationException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmulationException(Throwable cause) {
        super(cause.getMessage(), cause);
    }
}
