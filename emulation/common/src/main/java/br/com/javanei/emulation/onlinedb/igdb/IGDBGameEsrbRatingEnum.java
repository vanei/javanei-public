package br.com.javanei.emulation.onlinedb.igdb;

public enum IGDBGameEsrbRatingEnum {
    THREE(1),
    SEVEN(2),
    TWELVE(3),
    SIXTEEN(4),
    EIGHTEEN(5);

    private final Integer value;

    private IGDBGameEsrbRatingEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
