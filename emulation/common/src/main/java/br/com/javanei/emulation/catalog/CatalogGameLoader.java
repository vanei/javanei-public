package br.com.javanei.emulation.catalog;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Utilizado no Commodore 64
 */
@XmlRootElement(name = "loader")
public class CatalogGameLoader implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Map<String, CatalogGameLoader> loaders = new HashMap<>();

    static {
        loaders.put("Ace Of Aces Loader", new CatalogGameLoader("Ace Of Aces Loader"));
        loaders.put("Anirog", new CatalogGameLoader("Anirog"));
        loaders.put("Atlantis Loader", new CatalogGameLoader("Atlantis Loader"));
        loaders.put("Audiogenic Loader", new CatalogGameLoader("Audiogenic Loader"));
        loaders.put("Bleepload", new CatalogGameLoader("Bleepload"));
        loaders.put("Burner", new CatalogGameLoader("Burner"));
        loaders.put("CHR Loader", new CatalogGameLoader("CHR Loader"));
        loaders.put("CHR", new CatalogGameLoader("CHR Loader"));
        loaders.put("Cyberload", new CatalogGameLoader("Cyberload"));
        loaders.put("Cyberload+ROM Loader", new CatalogGameLoader("Cyberload+ROM Loader"));
        loaders.put("Enigma Loader", new CatalogGameLoader("Enigma Loader"));
        loaders.put("Firebird", new CatalogGameLoader("Firebird"));
        loaders.put("Flashload", new CatalogGameLoader("Flashload"));
        loaders.put("Freeload", new CatalogGameLoader("Freeload"));
        loaders.put("Graftgold", new CatalogGameLoader("Graftgold"));
        loaders.put("Hi TEC Loader", new CatalogGameLoader("Hi TEC Loader"));
        loaders.put("Hit-load", new CatalogGameLoader("Hit-load"));
        loaders.put("IK Tape Loader", new CatalogGameLoader("IK Tape Loader"));
        loaders.put("Invade-A-Load!", new CatalogGameLoader("Invade-A-Load!"));
        loaders.put("Jet-Load", new CatalogGameLoader("Jet-Load"));
        loaders.put("Load N Play", new CatalogGameLoader("Load N Play"));
        loaders.put("Martech", new CatalogGameLoader("Martech"));
        loaders.put("Mastertronic", new CatalogGameLoader("Mastertronic"));
        loaders.put("Microload", new CatalogGameLoader("Microload"));
        loaders.put("Novaload", new CatalogGameLoader("Novaload"));
        loaders.put("Ocean Loader (New)", new CatalogGameLoader("Ocean Loader (New)"));
        loaders.put("Ocean-Imagine Loader", new CatalogGameLoader("Ocean-Imagine Loader"));
        loaders.put("Palace Loader", new CatalogGameLoader("Palace Loader"));
        loaders.put("Pavloda", new CatalogGameLoader("Pavloda"));
        loaders.put("ROM Loader", new CatalogGameLoader("ROM Loader"));
        loaders.put("Rack-it", new CatalogGameLoader("Rack-it"));
        loaders.put("Rasterload", new CatalogGameLoader("Rasterload"));
        loaders.put("SEUCK Loader", new CatalogGameLoader("SEUCK Loader"));
        loaders.put("Snakeload", new CatalogGameLoader("Snakeload"));
        loaders.put("Super Pavloda", new CatalogGameLoader("Super Pavloda"));
        loaders.put("Super Tape", new CatalogGameLoader("Super Tape"));
        loaders.put("TDI Loader", new CatalogGameLoader("TDI Loader"));
        loaders.put("Tengen-Domark Loader", new CatalogGameLoader("Tengen-Domark Loader"));
        loaders.put("Turbo-Tape Compatible", new CatalogGameLoader("Turbo-Tape Compatible"));
        loaders.put("Turrican Loader", new CatalogGameLoader("Turrican Loader"));
        loaders.put("U.S. Gold & Cyberload Loaders", new CatalogGameLoader("U.S. Gold & Cyberload Loaders"));
        loaders.put("U.S. Gold Loader", new CatalogGameLoader("U.S. Gold Loader"));
        loaders.put("Unknown", new CatalogGameLoader("Unknown"));
        loaders.put("Virgin Loader", new CatalogGameLoader("Virgin Loader"));
        loaders.put("Visiload", new CatalogGameLoader("Visiload"));
        loaders.put("Wildsave", new CatalogGameLoader("Wildsave"));
        // ActivisionHit-load?
        // ActivisionWildsave?
    }

    private final String loader;

    private CatalogGameLoader(String loader) {
        this.loader = loader;
    }

    public static boolean isLoader(String name) {
        return loaders.containsKey(name);
    }

    public static CatalogGameLoader fromName(String name) {
        CatalogGameLoader load = loaders.get(name);
        if (load == null) {
            //TODO: Criar uma exception
            throw new IllegalArgumentException(name);
        }
        return load;
    }

    public static CatalogGameLoader getLoader(String name) {
        return loaders.get(name);
    }

    @Override
    public String toString() {
        return this.loader;
    }

}
