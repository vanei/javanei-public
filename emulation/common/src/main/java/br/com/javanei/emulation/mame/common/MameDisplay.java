package br.com.javanei.emulation.mame.common;

import java.io.Serializable;

public class MameDisplay implements Serializable {
    private static final long serialVersionUID = 1L;

    private String tag;
    private String type; // (raster|vector|lcd|unknown)
    private Integer rotate; // (0|90|180|270)
    private Boolean flipx; // (yes|no) "no"
    private Integer width;
    private Integer height;
    private String refresh;
    private Integer pixclock;
    private Integer htotal;
    private Integer hbend;
    private Integer hbstart;
    private Integer vtotal;
    private Integer vbend;
    private Integer vbstart;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRotate() {
        return rotate;
    }

    public void setRotate(Integer rotate) {
        this.rotate = rotate;
    }

    public void setRotate(String rotate) {
        this.rotate = new Integer(rotate);
    }

    public Boolean getFlipx() {
        return flipx;
    }

    public void setFlipx(Boolean flipx) {
        this.flipx = flipx;
    }

    public void setFlipx(String flipx) {
        if (flipx != null)
            this.flipx = flipx.equalsIgnoreCase("yes") || flipx.equalsIgnoreCase("true");
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public void setWidth(String width) {
        this.width = new Integer(width);
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void setHeight(String height) {
        this.height = new Integer(height);
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public Integer getPixclock() {
        return pixclock;
    }

    public void setPixclock(Integer pixclock) {
        this.pixclock = pixclock;
    }

    public void setPixclock(String pixclock) {
        this.pixclock = new Integer(pixclock);
    }

    public Integer getHtotal() {
        return htotal;
    }

    public void setHtotal(Integer htotal) {
        this.htotal = htotal;
    }

    public void setHtotal(String htotal) {
        this.htotal = new Integer(htotal);
    }

    public Integer getHbend() {
        return hbend;
    }

    public void setHbend(Integer hbend) {
        this.hbend = hbend;
    }

    public void setHbend(String hbend) {
        this.hbend = new Integer(hbend);
    }

    public Integer getHbstart() {
        return hbstart;
    }

    public void setHbstart(Integer hbstart) {
        this.hbstart = hbstart;
    }

    public void setHbstart(String hbstart) {
        this.hbstart = new Integer(hbstart);
    }

    public Integer getVtotal() {
        return vtotal;
    }

    public void setVtotal(Integer vtotal) {
        this.vtotal = vtotal;
    }

    public void setVtotal(String vtotal) {
        this.vtotal = new Integer(vtotal);
    }

    public Integer getVbend() {
        return vbend;
    }

    public void setVbend(Integer vbend) {
        this.vbend = vbend;
    }

    public void setVbend(String vbend) {
        this.vbend = new Integer(vbend);
    }

    public Integer getVbstart() {
        return vbstart;
    }

    public void setVbstart(Integer vbstart) {
        this.vbstart = vbstart;
    }

    public void setVbstart(String vbstart) {
        this.vbstart = new Integer(vbstart);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t<display");
        if (this.tag != null) {
            sb.append(" tag=\"").append(this.tag).append("\"");
        }
        if (this.type != null) {
            sb.append(" type=\"").append(this.type).append("\"");
        }
        if (this.rotate != null) {
            sb.append(" rotate=\"").append(this.rotate).append("\"");
        }
        if (this.flipx != null) {
            sb.append(" flipx=\"").append(this.flipx ? "yes" : "no").append("\"");
        }
        if (this.width != null) {
            sb.append(" width=\"").append(this.width).append("\"");
        }
        if (this.height != null) {
            sb.append(" height=\"").append(this.height).append("\"");
        }
        if (this.refresh != null) {
            sb.append(" refresh=\"").append(this.refresh).append("\"");
        }
        if (this.pixclock != null) {
            sb.append(" pixclock=\"").append(this.pixclock).append("\"");
        }
        if (this.htotal != null) {
            sb.append(" htotal=\"").append(this.htotal).append("\"");
        }
        if (this.hbend != null) {
            sb.append(" hbend=\"").append(this.hbend).append("\"");
        }
        if (this.hbstart != null) {
            sb.append(" hbstart=\"").append(this.hbstart).append("\"");
        }
        if (this.vtotal != null) {
            sb.append(" vtotal=\"").append(this.vtotal).append("\"");
        }
        if (this.vbend != null) {
            sb.append(" vbend=\"").append(this.vbend).append("\"");
        }
        if (this.vbstart != null) {
            sb.append(" vbstart=\"").append(this.vbstart).append("\"");
        }
        sb.append(" />").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
