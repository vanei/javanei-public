package br.com.javanei.emulation.catalog;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "protection")
public class CatalogGameProtection implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Map<String, CatalogGameProtection> protections = new HashMap<>();

    static {
        protections.put("RapidLok 1", new CatalogGameProtection("RapidLok 1")); //GoodSet
        protections.put("RapidLok 5", new CatalogGameProtection("RapidLok 5")); //GoodSet
        protections.put("RapidLok 6", new CatalogGameProtection("RapidLok 6")); //GoodSet
        protections.put("V-MAX", new CatalogGameProtection("V-MAX")); //GoodSet
        protections.put("V-MAX 2", new CatalogGameProtection("V-MAX 2")); //GoodSet
        protections.put("Vorpal", new CatalogGameProtection("Vorpal")); //GoodSet
        // Mindscape?
        // Datasoft?
    }

    private final String protection;

    private CatalogGameProtection(String protection) {
        this.protection = protection;
    }

    public static boolean isProtection(String names) {
        boolean result = true;
        for (String s : names.split(",")) {
            if (!protections.containsKey(s.trim())) {
                result = false;
            }
        }
        return result;
    }

    public static CatalogGameProtection fromName(String name) {
        CatalogGameProtection prot = protections.get(name);
        if (prot == null) {
            //TODO: Criar uma exception
            throw new IllegalArgumentException(name);
        }
        return prot;
    }

    public static CatalogGameProtection getProtection(String name) {
        return protections.get(name);
    }

    @Override
    public String toString() {
        return this.protection;
    }
}
