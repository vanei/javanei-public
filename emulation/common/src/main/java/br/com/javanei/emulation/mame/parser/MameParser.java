package br.com.javanei.emulation.mame.parser;

import br.com.javanei.common.util.FileUtil;
import br.com.javanei.emulation.game.GameProcessingMessage;
import br.com.javanei.emulation.game.GameProcessingResult;
import br.com.javanei.emulation.game.parser.AbstractGameParser;
import br.com.javanei.emulation.game.parser.GameParserMonitor;
import br.com.javanei.emulation.mame.common.Mame;
import br.com.javanei.emulation.mame.common.MameAdjuster;
import br.com.javanei.emulation.mame.common.MameAnalog;
import br.com.javanei.emulation.mame.common.MameBiosset;
import br.com.javanei.emulation.mame.common.MameChip;
import br.com.javanei.emulation.mame.common.MameConfiguration;
import br.com.javanei.emulation.mame.common.MameConfsetting;
import br.com.javanei.emulation.mame.common.MameDevice;
import br.com.javanei.emulation.mame.common.MameDeviceExtension;
import br.com.javanei.emulation.mame.common.MameDeviceInstance;
import br.com.javanei.emulation.mame.common.MameDeviceref;
import br.com.javanei.emulation.mame.common.MameDipswitch;
import br.com.javanei.emulation.mame.common.MameDipvalue;
import br.com.javanei.emulation.mame.common.MameDisk;
import br.com.javanei.emulation.mame.common.MameDisplay;
import br.com.javanei.emulation.mame.common.MameDriver;
import br.com.javanei.emulation.mame.common.MameInput;
import br.com.javanei.emulation.mame.common.MameInputControl;
import br.com.javanei.emulation.mame.common.MameMachine;
import br.com.javanei.emulation.mame.common.MamePort;
import br.com.javanei.emulation.mame.common.MameRamoption;
import br.com.javanei.emulation.mame.common.MameRom;
import br.com.javanei.emulation.mame.common.MameSample;
import br.com.javanei.emulation.mame.common.MameSlot;
import br.com.javanei.emulation.mame.common.MameSlotoption;
import br.com.javanei.emulation.mame.common.MameSoftwarelist;
import br.com.javanei.emulation.mame.common.MameSound;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MameParser extends AbstractGameParser {
    private static int machineCount = 0;
    private static int biossetCount = 0;
    private static int romCount = 0;
    private static int diskCount = 0;
    private static int devicerefCount = 0;
    private static int sampleCount = 0;
    private static int chipCount = 0;
    private static int displayCount = 0;
    private static int soundCount = 0;
    private static int inputCount = 0;
    private static int controlCount = 0;
    private static int dipswitchCount = 0;
    private static int dipvalueCount = 0;
    private static int configurationCount = 0;
    private static int confsettingCount = 0;
    private static int portCount = 0;
    private static int analogCount = 0;
    private static int adjusterCount = 0;
    private static int driverCount = 0;
    private static int deviceCount = 0;
    private static int instanceCount = 0;
    private static int extensionCount = 0;
    private static int slotCount = 0;
    private static int slotoptionCount = 0;
    private static int softwarelistCount = 0;
    private static int ramoptionCount = 0;

    private int iPercentComplete = 0;

    public MameParser(File srcFile) {
        super(srcFile);
    }

    public MameParser(File srcFile, GameParserMonitor monitor) {
        super(srcFile, monitor);
    }

    public static void main(String[] args) {
        FutureTask t1 = new FutureTask(new MameParser(new File("F:/tmp/mame173/listxml.xml")));
        ExecutorService executor = Executors.newFixedThreadPool(1);

        try {
            executor.execute(t1);
            GameProcessingResult result = (GameProcessingResult) t1.get();
            Mame mame = (Mame) result.getResult();
            FileUtil.writeFile(new File("F:/tmp/mame173/_listxml.txt"), mame.toString().getBytes());
            System.out.println("Estatisticas");
            System.out.println("===================================");
            System.out.println("Machine..........: " + machineCount);
            System.out.println("Biosset..........: " + biossetCount);
            System.out.println("ROM..............: " + romCount);
            System.out.println("Disk.............: " + diskCount);
            System.out.println("Deviceref........: " + devicerefCount);
            System.out.println("Sample...........: " + sampleCount);
            System.out.println("Chip.............: " + chipCount);
            System.out.println("Display..........: " + displayCount);
            System.out.println("Sound............: " + soundCount);
            System.out.println("Input............: " + inputCount);
            System.out.println("Input Control....: " + controlCount);
            System.out.println("Dipswitch........: " + dipswitchCount);
            System.out.println("Dipvalue.........: " + dipvalueCount);
            System.out.println("Configuration....: " + configurationCount);
            System.out.println("Confsetting......: " + confsettingCount);
            System.out.println("Port.............: " + portCount);
            System.out.println("Analog...........: " + analogCount);
            System.out.println("Adjuster.........: " + adjusterCount);
            System.out.println("Driver...........: " + driverCount);
            System.out.println("Device...........: " + deviceCount);
            System.out.println("Device Instance..: " + instanceCount);
            System.out.println("Device Extension.: " + extensionCount);
            System.out.println("Slot.............: " + slotCount);
            System.out.println("Sloptoption......: " + slotoptionCount);
            System.out.println("Softwarelit......: " + softwarelistCount);
            System.out.println("Ramoption........: " + ramoptionCount);

            Map<String, Integer> map = new LinkedHashMap<>();
            Map<String, String> maps = new LinkedHashMap<>();
            calculaTamanhos(mame, map, maps, null);
            Iterator<String> it = map.keySet().iterator();
            while (it.hasNext()) {
                String key = it.next();
                System.out.println(key + "=" + map.get(key) + " -> " + maps.get(key + ".value"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    private static MameBiosset parseBiosset(Node node) {
        MameBiosset biosset = new MameBiosset();
        biossetCount++;
        NamedNodeMap attrs = node.getAttributes();
        if (attrs != null) {
            if (attrs.getNamedItem("name") != null)
                biosset.setName(attrs.getNamedItem("name").getNodeValue());
            if (attrs.getNamedItem("description") != null)
                biosset.setDescription(attrs.getNamedItem("description").getNodeValue());
            if (attrs.getNamedItem("default") != null)
                biosset.setDefault(attrs.getNamedItem("default").getNodeValue());
        }
        return biosset;
    }

    private static void calculaTamanhos(Object o, Map<String, Integer> map, Map<String, String> maps, String parent) throws Exception {
        Method[] ms = o.getClass().getDeclaredMethods();
        for (Method m : ms) {
            if (m.getName().startsWith("get") && m.getParameterCount() == 0) {
                String key = parent != null ? parent + "." : "";
                key += m.getName().substring(3);
                if (m.getReturnType().getCanonicalName().equals(String.class.getCanonicalName())) {
                    Integer i = map.get(key);
                    String s = (String) m.invoke(o);
                    if (s != null) {
                        if (i == null || s.length() > i.intValue()) {
                            i = s.length();
                            map.put(key, i);
                            maps.put(key + ".value", s);
                        }
                    }
                }
            }
        }
        for (Method m : ms) {
            if (m.getName().startsWith("get") && m.getParameterCount() == 0) {
                String key = parent != null ? parent + "." : "";
                key += m.getName().substring(3);
                if (m.getReturnType().equals(List.class)) {
                    List l = (List) m.invoke(o);
                    if (l != null) {
                        for (Object obj : l) {
                            calculaTamanhos(obj, map, maps, key);
                        }
                    }
                }
            }
        }
    }

    @Override
    public GameProcessingResult call() throws Exception {
        if (!srcFile.exists()) {
            throw dispatchErrorMessage(new FileNotFoundException(srcFile.getAbsolutePath()), 0);
        }

        Mame mame = new Mame();
        this.result.setResult(mame);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        dispatchMessage("Parsing file [" + srcFile + "]", 0);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(this.srcFile);
        NodeList nodes = doc.getChildNodes();
        dispatchMessage("Processing xml", 0);
        for (int i = 0; i < nodes.getLength(); i++) {
            Node nMame = nodes.item(i);
            if (nMame.getChildNodes().getLength() == 0)
                continue;
            NamedNodeMap attrs = nMame.getAttributes();
            if (attrs != null) {
                if (attrs.getNamedItem("build") != null)
                    mame.setBuild(attrs.getNamedItem("build").getNodeValue());
                if (attrs.getNamedItem("debug") != null)
                    mame.setDebug(attrs.getNamedItem("debug").getNodeValue());
                if (attrs.getNamedItem("mameconfig") != null)
                    mame.setMameconfig(attrs.getNamedItem("mameconfig").getNodeValue());
            }
            NodeList mameChildList = nMame.getChildNodes();
            int iTotalMachines = mameChildList.getLength();
            for (int j = 0; j < mameChildList.getLength(); j++) {
                iPercentComplete = (j * 100 / iTotalMachines);
                dispatchMessage("Parsing machine line " + (j + 1) + " of " + iTotalMachines, iPercentComplete);
                Node machineNode = mameChildList.item(j);
                if (machineNode.getNodeName().equals("machine")) {
                    MameMachine machine = new MameMachine();
                    machineCount++;
                    mame.addMachine(machine);
                    attrs = machineNode.getAttributes();
                    if (attrs != null) {
                        for (int k = 0; k < attrs.getLength(); k++) {
                            Node node = attrs.item(k);
                            if (node != null && node.getNodeName() != null) {
                                switch (node.getNodeName()) {
                                    case "name":
                                        machine.setName(node.getNodeValue());
                                        break;
                                    case "sourcefile":
                                        machine.setSourcefile(node.getNodeValue());
                                        break;
                                    case "isbios":
                                        machine.setIsbios(node.getNodeValue());
                                        break;
                                    case "isdevice":
                                        machine.setIsdevice(node.getNodeValue());
                                        break;
                                    case "ismechanical":
                                        machine.setIsmechanical(node.getNodeValue());
                                        break;
                                    case "runnable":
                                        machine.setRunnable(node.getNodeValue());
                                        break;
                                    case "cloneof":
                                        machine.setCloneof(node.getNodeValue());
                                        break;
                                    case "romof":
                                        machine.setRomof(node.getNodeValue());
                                        break;
                                    case "sampleof":
                                        machine.setSampleof(node.getNodeValue());
                                        break;
                                    case "#text":
                                        break;
                                    default:
                                        dispatchWarnMessage("Unknown Machine attribute: " + node.getNodeName(), iPercentComplete);
                                }
                            }
                        }
                    }
                    NodeList machineChildNodes = machineNode.getChildNodes();
                    for (int k = 0; k < machineChildNodes.getLength(); k++) {
                        Node machineChild = machineChildNodes.item(k);
                        if (machineChild.getNodeName() != null) {
                            switch (machineChild.getNodeName()) {
                                case "description":
                                    machine.setDescription(machineChild.getTextContent().trim());
                                    break;
                                case "year":
                                    machine.setYear(machineChild.getTextContent().trim());
                                    break;
                                case "manufacturer":
                                    machine.setManufacturer(machineChild.getTextContent().trim());
                                    break;
                                case "biosset":
                                    machine.addBiosset(parseBiosset(machineChild));
                                    break;
                                case "rom":
                                    machine.addRom(parseRom(machineChild));
                                    break;
                                case "disk":
                                    machine.addDisk(parseDisk(machineChild));
                                    break;
                                case "device_ref":
                                    machine.addDeviceref(parseDeviceref(machineChild));
                                    break;
                                case "sample":
                                    machine.addSample(parseSample(machineChild));
                                    break;
                                case "chip":
                                    machine.addChip(parseChip(machineChild));
                                    break;
                                case "display":
                                    machine.addDisplay(parseDisplay(machineChild));
                                    break;
                                case "sound":
                                    machine.setSound(parseSound(machineChild));
                                    break;
                                case "input":
                                    machine.setInput(parseInput(machineChild));
                                    break;
                                case "dipswitch":
                                    machine.addDipswitch(parseDipswitch(machineChild));
                                    break;
                                case "configuration":
                                    machine.addConfiguration(parseConfiguration(machineChild));
                                    break;
                                case "port":
                                    machine.addPort(parsePort(machineChild));
                                    break;
                                case "adjuster":
                                    machine.addAdjuster(parseAdjuster(machineChild));
                                    break;
                                case "driver":
                                    machine.setDriver(parseDriver(machineChild));
                                    break;
                                case "device":
                                    machine.addDevice(parseDevice(machineChild));
                                    break;
                                case "slot":
                                    machine.addSlot(parseSlot(machineChild));
                                    break;
                                case "softwarelist":
                                    machine.addSoftwarelist(parseSoftwarelist(machineChild));
                                    break;
                                case "ramoption":
                                    machine.addRamoption(parseRamoption(machineChild));
                                    break;
                                case "#text":
                                    // Espaço em branco no xml
                                    break;
                                default:
                                    dispatchWarnMessage("Unknown Machine Node: " + machineChild.getNodeName() + "->[" + machineChild.getTextContent() + "]", iPercentComplete);
                            }
                        }
                    }
                }
            }
        }

        return this.result;
    }

    private MameRom parseRom(Node node) {
        MameRom rom = new MameRom();
        romCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    rom.setName(value);
                    break;
                case "bios":
                    rom.setBios(value);
                    break;
                case "size":
                    rom.setSize(value);
                    break;
                case "crc":
                    rom.setCrc(value);
                    break;
                case "sha1":
                    rom.setSha1(value);
                    break;
                case "merge":
                    rom.setMerge(value);
                    break;
                case "region":
                    rom.setRegion(value);
                    break;
                case "offset":
                    rom.setOffset(value);
                    break;
                case "status":
                    rom.setStatus(value);
                    break;
                case "optional":
                    rom.setOptional(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Rom Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return rom;
    }

    private MameDisk parseDisk(Node node) {
        MameDisk disk = new MameDisk();
        diskCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    disk.setName(value);
                    break;
                case "sha1":
                    disk.setSha1(value);
                    break;
                case "merge":
                    disk.setMerge(value);
                    break;
                case "region":
                    disk.setRegion(value);
                    break;
                case "index":
                    disk.setIndex(value);
                    break;
                case "writable":
                    disk.setWritable(value);
                    break;
                case "status":
                    disk.setStatus(value);
                    break;
                case "optional":
                    disk.setOptional(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Disk Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return disk;
    }

    private MameDeviceref parseDeviceref(Node node) {
        MameDeviceref ref = new MameDeviceref();
        devicerefCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    ref.setName(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Deviceref Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return ref;
    }

    private MameSample parseSample(Node node) {
        MameSample sample = new MameSample();
        sampleCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    sample.setName(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Sample Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return sample;
    }

    private MameChip parseChip(Node node) {
        MameChip chip = new MameChip();
        chipCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    chip.setName(value);
                    break;
                case "tag":
                    chip.setTag(value);
                    break;
                case "type":
                    chip.setType(value);
                    break;
                case "clock":
                    chip.setClock(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Chip Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return chip;
    }

    private MameDisplay parseDisplay(Node node) {
        MameDisplay display = new MameDisplay();
        displayCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "tag":
                    display.setTag(value);
                    break;
                case "type":
                    display.setType(value);
                    break;
                case "rotate":
                    display.setRotate(value);
                    break;
                case "flipx":
                    display.setFlipx(value);
                    break;
                case "width":
                    display.setWidth(value);
                    break;
                case "height":
                    display.setHeight(value);
                    break;
                case "refresh":
                    display.setRefresh(value);
                    break;
                case "pixclock":
                    display.setPixclock(value);
                    break;
                case "htotal":
                    display.setHtotal(value);
                    break;
                case "hbend":
                    display.setHbend(value);
                    break;
                case "hbstart":
                    display.setHbstart(value);
                    break;
                case "vtotal":
                    display.setVtotal(value);
                    break;
                case "vbend":
                    display.setVbend(value);
                    break;
                case "vbstart":
                    display.setVbstart(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Display Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return display;
    }

    private MameSound parseSound(Node node) {
        MameSound sound = new MameSound();
        soundCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "channels":
                    sound.setChannels(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Sound Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return sound;
    }

    private MameInput parseInput(Node node) {
        MameInput input = new MameInput();
        inputCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "service":
                    input.setService(value);
                    break;
                case "tilt":
                    input.setTilt(value);
                    break;
                case "players":
                    input.setPlayers(value);
                    break;
                case "coins":
                    input.setCoins(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Input Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node child = list.item(i);
            if (child.getNodeName() != null) {
                if (child.getNodeName().equals("control")) {
                    input.addControl(parseInputControl(child));
                } else if (child.getNodeName().equals("#text")) {
                } else {
                    this.dispatchWarnMessage("Unknown Input Node : " + child.getNodeName() + "->[" + child.getTextContent() + "]", iPercentComplete);
                }
            }
        }
        return input;
    }

    private MameInputControl parseInputControl(Node node) {
        MameInputControl control = new MameInputControl();
        controlCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "type":
                    control.setType(value);
                    break;
                case "player":
                    control.setPlayer(value);
                    break;
                case "buttons":
                    control.setButtons(value);
                    break;
                case "minimum":
                    control.setMinimum(value);
                    break;
                case "maximum":
                    control.setMaximum(value);
                    break;
                case "sensitivity":
                    control.setSensitivity(value);
                    break;
                case "keydelta":
                    control.setKeydelta(value);
                    break;
                case "reverse":
                    control.setReverse(value);
                    break;
                case "ways":
                    control.setWays(value);
                    break;
                case "ways2":
                    control.setWays2(value);
                    break;
                case "ways3":
                    control.setWays3(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Control Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return control;
    }

    private MameDipswitch parseDipswitch(Node node) {
        MameDipswitch dipswitch = new MameDipswitch();
        dipswitchCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    dipswitch.setName(value);
                    break;
                case "tag":
                    dipswitch.setTag(value);
                    break;
                case "mask":
                    dipswitch.setMask(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Dipswitch Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node child = list.item(i);
            if (child.getNodeName() != null) {
                if (child.getNodeName().equals("dipvalue")) {
                    dipswitch.addDipvalue(parseDipvalue(child));
                } else if (child.getNodeName().equals("#text")) {
                } else {
                    this.dispatchWarnMessage("Unknown Dipswitch Node: " + child.getNodeName() + "->[" + child.getTextContent() + "]", iPercentComplete);
                }
            }
        }
        return dipswitch;
    }

    private MameDipvalue parseDipvalue(Node node) {
        MameDipvalue dipvalue = new MameDipvalue();
        dipvalueCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    dipvalue.setName(value);
                    break;
                case "value":
                    dipvalue.setValue(value);
                    break;
                case "default":
                    dipvalue.setDefault(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Dipvalue Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return dipvalue;
    }

    private MameConfiguration parseConfiguration(Node node) {
        MameConfiguration configuration = new MameConfiguration();
        configurationCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    configuration.setName(value);
                    break;
                case "tag":
                    configuration.setTag(value);
                    break;
                case "mask":
                    configuration.setMask(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Configuration Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node child = list.item(i);
            if (child.getNodeName() != null) {
                if (child.getNodeName().equals("confsetting")) {
                    configuration.addConfsetting(parseConfsetting(child));
                } else if (child.getNodeName().equals("#text")) {
                } else {
                    this.dispatchWarnMessage("Unknown Configuration Node: " + child.getNodeName() + "->[" + child.getTextContent() + "]", iPercentComplete);
                }
            }
        }
        return configuration;
    }

    private MameConfsetting parseConfsetting(Node node) {
        MameConfsetting confsetting = new MameConfsetting();
        confsettingCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    confsetting.setName(value);
                    break;
                case "value":
                    confsetting.setValue(value);
                    break;
                case "default":
                    confsetting.setDefault(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Confsetting Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return confsetting;
    }

    private MamePort parsePort(Node node) {
        MamePort port = new MamePort();
        portCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "tag":
                    port.setTag(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Port Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node child = list.item(i);
            if (child.getNodeName() != null) {
                if (child.getNodeName().equals("analog")) {
                    port.addAnalog(parseAnalog(child));
                } else if (child.getNodeName().equals("#text")) {
                } else {
                    this.dispatchWarnMessage("Unknown Port Node: " + child.getNodeName() + "->[" + child.getTextContent() + "]", iPercentComplete);
                }
            }
        }
        return port;
    }

    private MameAnalog parseAnalog(Node node) {
        MameAnalog analog = new MameAnalog();
        analogCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "mask":
                    analog.setMask(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Analog Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return analog;
    }

    private MameAdjuster parseAdjuster(Node node) {
        MameAdjuster adjuster = new MameAdjuster();
        adjusterCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    adjuster.setName(value);
                    break;
                case "default":
                    adjuster.setDefault(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Adjuster Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return adjuster;
    }

    private MameDriver parseDriver(Node node) {
        MameDriver driver = new MameDriver();
        driverCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "status":
                    driver.setStatus(value);
                    break;
                case "emulation":
                    driver.setEmulation(value);
                    break;
                case "color":
                    driver.setColor(value);
                    break;
                case "sound":
                    driver.setSound(value);
                    break;
                case "graphic":
                    driver.setGraphic(value);
                    break;
                case "cocktail":
                    driver.setCocktail(value);
                    break;
                case "protection":
                    driver.setProtection(value);
                    break;
                case "savestate":
                    driver.setSavestate(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Attribute Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return driver;
    }

    private MameDevice parseDevice(Node node) {
        MameDevice device = new MameDevice();
        deviceCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "type":
                    device.setType(value);
                    break;
                case "tag":
                    device.setTag(value);
                    break;
                case "fixed_image":
                    device.setFixed_image(value);
                    break;
                case "mandatory":
                    device.setMandatory(value);
                    break;
                case "interface":
                    device.setInterface(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Device Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node child = list.item(i);
            if (child.getNodeName() != null) {
                if (child.getNodeName().equals("instance")) {
                    device.addInstance(parseDeviceInstance(child));
                } else if (child.getNodeName().equals("extension")) {
                    device.addExtension(parseDeviceExtension(child));
                } else if (child.getNodeName().equals("#text")) {
                } else {
                    this.dispatchWarnMessage("Unknown Node Attribute: " + child.getNodeName() + "->[" + child.getTextContent() + "]", iPercentComplete);
                }
            }
        }
        return device;
    }

    private MameDeviceInstance parseDeviceInstance(Node node) {
        MameDeviceInstance instance = new MameDeviceInstance();
        instanceCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    instance.setName(value);
                    break;
                case "briefname":
                    instance.setBriefname(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Device Instance Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return instance;
    }

    private MameDeviceExtension parseDeviceExtension(Node node) {
        MameDeviceExtension extension = new MameDeviceExtension();
        extensionCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    extension.setName(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Device Extension Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return extension;
    }

    private MameSlot parseSlot(Node node) {
        MameSlot slot = new MameSlot();
        slotCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    slot.setName(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Slot Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node child = list.item(i);
            if (child.getNodeName() != null) {
                if (child.getNodeName().equals("slotoption")) {
                    slot.addSlotoption(parseSlotoption(child));
                } else if (child.getNodeName().equals("#text")) {
                } else {
                    this.dispatchWarnMessage("Unknown Slot Node: " + child.getNodeName() + "->[" + child.getTextContent() + "]", iPercentComplete);
                }
            }
        }
        return slot;
    }

    private MameSlotoption parseSlotoption(Node node) {
        MameSlotoption slotoption = new MameSlotoption();
        slotoptionCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    slotoption.setName(value);
                    break;
                case "devname":
                    slotoption.setDevname(value);
                    break;
                case "default":
                    slotoption.setDefault(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Slotoption Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return slotoption;
    }

    private MameSoftwarelist parseSoftwarelist(Node node) {
        MameSoftwarelist softwarelist = new MameSoftwarelist();
        softwarelistCount++;
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "name":
                    softwarelist.setName(value);
                    break;
                case "status":
                    softwarelist.setStatus(value);
                    break;
                case "filter":
                    softwarelist.setFilter(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Softwarelist Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return softwarelist;
    }

    private MameRamoption parseRamoption(Node node) {
        MameRamoption ramoption = new MameRamoption();
        ramoptionCount++;
        ramoption.setContent(node.getTextContent().trim());
        NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node n = attrs.item(i);
            String name = n.getNodeName();
            String value = n.getNodeValue();
            switch (name) {
                case "default":
                    ramoption.setDefault(value);
                    break;
                default:
                    this.dispatchWarnMessage("Unknown Ramoption Attribute: " + name + "=" + value, iPercentComplete);
            }
        }
        return ramoption;
    }

    private static class MameParserMonitor implements GameParserMonitor {
        @Override
        public void onMessage(GameProcessingMessage message, int percent) {
            System.out.println(percent + "%, " + message.toString());
        }
    }
}
