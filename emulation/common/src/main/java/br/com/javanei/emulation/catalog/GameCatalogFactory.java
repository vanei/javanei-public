package br.com.javanei.emulation.catalog;

import br.com.javanei.emulation.InvalidDatFileFormatException;
import br.com.javanei.emulation.catalog.clrmamepro.parser.ClrMameProParser;
import br.com.javanei.emulation.common.PlatformVO;
import java.io.BufferedReader;
import java.io.StringReader;

public final class GameCatalogFactory {
    public static CatalogParser getParser(PlatformVO platform, String fileName, byte[] fileContent) throws Exception {
        try (BufferedReader reader = new BufferedReader(new StringReader(new String(fileContent)))) {
            String line = reader.readLine().trim();
            String[] ss = line.split(" ");
            if (ss[0].trim().equals("clrmamepro")) {
                return new ClrMameProParser(platform, fileName, fileContent);
            } else {
                throw new InvalidDatFileFormatException(ss[0].trim());
            }
        }
    }
}
