package br.com.javanei.emulation.common;

import br.com.javanei.emulation.platform.StorageFormatEnum;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "platform")
public final class PlatformVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private String validExtension = "*";
    private String repositoryDir = "./{PlatformName}";
    private boolean multiFile = false;
    private boolean allowZip = true;
    private StorageFormatEnum storageFormat = StorageFormatEnum.zip;
    private Long thegamesdbId;
    private Long igdbId;
    private Long giantbombId;

    public PlatformVO() {
    }

    public PlatformVO(String name) {
        this.name = name;
    }

    public PlatformVO(String name, String validExtension, String repositoryDir, boolean multiFile, boolean allowZip,
                      StorageFormatEnum storageFormat) {
        this.name = name;
        this.validExtension = validExtension;
        this.repositoryDir = repositoryDir;
        this.multiFile = multiFile;
        this.allowZip = allowZip;
        this.storageFormat = storageFormat;
    }

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValidExtension() {
        return validExtension;
    }

    public void setValidExtension(String validExtension) {
        this.validExtension = validExtension;
    }

    public String getRepositoryDir() {
        return repositoryDir;
    }

    public void setRepositoryDir(String repositoryDir) {
        this.repositoryDir = repositoryDir;
    }

    public boolean isMultiFile() {
        return multiFile;
    }

    public void setMultiFile(boolean multiFile) {
        this.multiFile = multiFile;
    }

    public boolean isAllowZip() {
        return allowZip;
    }

    public void setAllowZip(boolean allowZip) {
        this.allowZip = allowZip;
    }

    public StorageFormatEnum getStorageFormat() {
        return storageFormat;
    }

    public void setStorageFormat(StorageFormatEnum storageFormat) {
        this.storageFormat = storageFormat;
    }

    public Long getThegamesdbId() {
        return thegamesdbId;
    }

    public void setThegamesdbId(Long thegamesdbId) {
        this.thegamesdbId = thegamesdbId;
    }

    public Long getIgdbId() {
        return igdbId;
    }

    public void setIgdbId(Long igdbId) {
        this.igdbId = igdbId;
    }

    public Long getGiantbombId() {
        return giantbombId;
    }

    public void setGiantbombId(Long giantbombId) {
        this.giantbombId = giantbombId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<platform>");
        appendIfNotNull(sb, this.name, "name");
        appendIfNotNull(sb, this.validExtension, "validExtension");
        appendIfNotNull(sb, this.repositoryDir, "repositoryDir");
        appendIfNotNull(sb, this.multiFile, "multiFile");
        appendIfNotNull(sb, this.allowZip, "allowZip");
        appendIfNotNull(sb, this.storageFormat, "storageFormat");
        appendIfNotNull(sb, this.thegamesdbId, "thegamesdbId");
        appendIfNotNull(sb, this.igdbId, "igdbId");
        appendIfNotNull(sb, this.giantbombId, "giantbombId");
        sb.append("</platform>");
        return sb.toString();
    }

    public String toJSON() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"name\":\"").append(this.name).
                append("\",\"validExtension\":\"").append(this.validExtension).
                append("\",\"repositoryDir\":\"").append(this.repositoryDir).
                append("\",\"multiFile\":").append(this.multiFile).
                append(",\"allowZip\":").append(this.allowZip).
                append(",\"storageFormat\":\"").append(this.storageFormat.getName()).append("\"");
        if (thegamesdbId != null)
            sb.append(",\"thegamesdbId\":\"").append(this.thegamesdbId).append("\"");
        if (igdbId != null)
            sb.append(",\"igdbId\":\"").append(this.igdbId).append("\"");
        if (giantbombId != null)
            sb.append(",\"giantbombId\":\"").append(this.giantbombId).append("\"");
        sb.append("}");
        return sb.toString();
    }
}
