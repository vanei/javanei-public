package br.com.javanei.emulation.game;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class GameProcessingResult implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement
    private final List<GameProcessingMessage> messages = new LinkedList<>();
    @XmlElement
    private Object result;

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public List<GameProcessingMessage> getMessages() {
        return messages;
    }

    public void addMessage(GameProcessingMessage message) {
        this.messages.add(message);
    }

    public void addMessages(List<GameProcessingMessage> msgs) {
        this.messages.addAll(msgs);
    }

    public void sortMessages() {
        Collections.sort(this.messages);
    }

    @Override
    public String toString() {
        return "result {" +
                "result=" + result +
                '}';
    }
}
