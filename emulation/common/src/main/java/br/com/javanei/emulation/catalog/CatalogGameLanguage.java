package br.com.javanei.emulation.catalog;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "language")
public class CatalogGameLanguage {

    private static final long serialVersionUID = 1L;

    private static final Map<String, CatalogGameLanguage> languages = new HashMap<>();

    static {
        languages.put("En", new CatalogGameLanguage("En"));
        languages.put("Fr", new CatalogGameLanguage("Fr"));
        languages.put("De", new CatalogGameLanguage("De"));
        languages.put("Es", new CatalogGameLanguage("Es"));
        languages.put("It", new CatalogGameLanguage("It"));
        languages.put("Sv", new CatalogGameLanguage("Sv"));
        languages.put("No", new CatalogGameLanguage("No"));
        languages.put("Da", new CatalogGameLanguage("Da"));
        languages.put("Nl", new CatalogGameLanguage("Nl"));
        languages.put("Gd", new CatalogGameLanguage("Gd"));
        languages.put("Ja", new CatalogGameLanguage("Ja"));
        languages.put("Hr", new CatalogGameLanguage("Hr"));
        languages.put("Pt", new CatalogGameLanguage("Pt"));
        languages.put("Pl", new CatalogGameLanguage("Pl"));
        languages.put("Zh", new CatalogGameLanguage("Zh"));
        languages.put("Fi", new CatalogGameLanguage("Fi"));
        languages.put("Ca", new CatalogGameLanguage("Ca"));
        languages.put("Ko", new CatalogGameLanguage("Ko"));
    }

    private final String language;

    public CatalogGameLanguage(String lang) {
        this.language = lang;
    }

    public static boolean isLanguage(String name) {
        return languages.containsKey(name);
    }

    public static boolean isLanguages(String langList) {
        boolean result = langList != null && !langList.trim().isEmpty();
        if (result) {
            String[] ss = langList.split(",");
            for (String s : ss) {
                if (s.indexOf("+") > 0) {
                    String[] ss2 = s.split("\\+");
                    for (String s2 : ss2) {
                        if (!isLanguage(s2.trim())) {
                            result = false;
                            break;
                        }
                    }
                } else {
                    if (!isLanguage(s.trim())) {
                        result = false;
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static CatalogGameLanguage fromName(String name) {
        CatalogGameLanguage lang = languages.get(name);
        if (lang == null) {
            //TODO: Criar uma exception
            throw new IllegalArgumentException(name);
        }
        return lang;
    }

    public static List<CatalogGameLanguage> fromNames(String langList) {
        String[] ss = langList.split(",");
        List<CatalogGameLanguage> result = new LinkedList<>();
        for (String s : ss) {
            if (s.indexOf("+") > 0) {
                String[] ss2 = s.split("\\+");
                for (String s2 : ss2) {
                    CatalogGameLanguage gl = fromName(s2.trim());
                    if (!result.contains(gl)) {
                        result.add(gl);
                    }
                }
            } else {
                CatalogGameLanguage gl = fromName(s.trim());
                if (!result.contains(gl)) {
                    result.add(gl);
                }
            }
        }
        return result;
    }

    public static CatalogGameLanguage getLanguage(String name) {
        return languages.get(name);
    }

    @Override
    public String toString() {
        return this.language;
    }
}
