package br.com.javanei.emulation.mame.common;

import java.io.Serializable;

public class MameDisk implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String sha1;
    private String merge;
    private String region;
    private Integer index;
    private Boolean writable; // (yes|no) "no"
    private String status; // (baddump|nodump|good) "good"
    private Boolean optional; // (yes|no) "no"

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    public String getMerge() {
        return merge;
    }

    public void setMerge(String merge) {
        this.merge = merge;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setIndex(String index) {
        if (index != null)
            this.index = new Integer(index);
    }

    public Boolean getWritable() {
        return writable;
    }

    public void setWritable(Boolean writable) {
        if (writable != null)
            this.writable = writable;
    }

    public void setWritable(String writable) {
        if (writable != null)
            this.writable = writable.equalsIgnoreCase("yes") || writable.equalsIgnoreCase("true");
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getOptional() {
        return optional;
    }

    public void setOptional(Boolean optional) {
        this.optional = optional;
    }

    public void setOptional(String optional) {
        if (optional != null)
            this.optional = optional.equalsIgnoreCase("yes") || optional.equalsIgnoreCase("true");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t<disk");
        if (this.name != null) {
            sb.append(" name=\"").append(this.name).append("\"");
        }
        if (this.merge != null) {
            sb.append(" merge=\"").append(this.merge).append("\"");
        }
        if (this.sha1 != null) {
            sb.append(" sha1=\"").append(this.sha1).append("\"");
        }
        if (this.status != null) {
            sb.append(" status=\"").append(this.status).append("\"");
        }
        if (this.region != null) {
            sb.append(" region=\"").append(this.region).append("\"");
        }
        if (this.index != null) {
            sb.append(" index=\"").append(this.index).append("\"");
        }
        if (this.writable != null) {
            sb.append(" writable=\"").append(this.writable ? "yes" : "no").append("\"");
        }
        if (this.optional != null) {
            sb.append(" optional=\"").append(this.optional ? "yes" : "no").append("\"");
        }
        sb.append("/>").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
