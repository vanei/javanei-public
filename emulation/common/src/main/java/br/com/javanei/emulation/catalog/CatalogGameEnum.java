package br.com.javanei.emulation.catalog;

public enum CatalogGameEnum {

    MAME("MAME"),
    GoodSet("GoodSet"),
    TOSEC("TOSEC"),
    NoIntro("NoIntro"), // Usa o dat do ClrMamePro ou RomCenter
    Hyperspin("Hyperspin");

    private final String name;

    private CatalogGameEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
