package br.com.javanei.emulation.frontend.launchbox.metadata;

public enum LaunchBoxGenre {
    Action("Action"),
    Adventure("Adventure"),
    Horror("Horror"),
    Role_Playing("Role-Playing"),
    Flight_Simulator("Flight Simulator"),
    Platform("Platform"),
    Puzzle("Puzzle"),
    Racing("Racing"),
    Fighting("Fighting"),
    Strategy("Strategy"),
    Sports("Sports"),
    Shooter("Shooter"),
    Sandbox("Sandbox"),
    Vehicle_Simulation("Vehicle Simulation"),
    Casino("Casino"),
    Stealth("Stealth"),
    Construction_and_Management_Simulation("Construction and Management Simulation"),
    Life_Simulation("Life Simulation"),
    MMO("MMO"),
    Music("Music"),
    Quiz("Quiz");

    private final String name;

    private LaunchBoxGenre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
