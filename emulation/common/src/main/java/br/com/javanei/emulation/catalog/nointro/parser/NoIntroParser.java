package br.com.javanei.emulation.catalog.nointro.parser;

import br.com.javanei.emulation.InvalidDatFileFormatException;
import br.com.javanei.emulation.catalog.CatalogParser;
import br.com.javanei.emulation.catalog.clrmamepro.parser.ClrMameProParser;
import br.com.javanei.emulation.common.PlatformVO;
import java.io.BufferedReader;
import java.io.StringReader;

public class NoIntroParser {
    public CatalogParser getImporter(PlatformVO platform, String fileName, byte[] fileContent) throws Exception {
        try (BufferedReader reader = new BufferedReader(new StringReader(new String(fileContent)))) {
            String line = reader.readLine().trim();
            String[] ss = line.split(" ");
            if (ss[0].trim().equals("clrmamepro")) {
                return new ClrMameProParser(platform, fileName, fileContent);
            } else {
                throw new InvalidDatFileFormatException(ss[0]);
            }
        }
    }
}
