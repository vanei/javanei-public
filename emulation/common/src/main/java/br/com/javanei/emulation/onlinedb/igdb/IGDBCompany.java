package br.com.javanei.emulation.onlinedb.igdb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "company")
public class IGDBCompany implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String slug;
    private String url;
    private Long createdAt;
    private Long updatedAt;
    private Integer startDateCategory;
    private Integer changeDateCategory;
    private Integer country;
    private String website;
    private String description;
    private String twitter;
    private Long startDate;
    private Long changeDate;
    private Long parent;
    private Long changedCompanyId;
    //logo
    private List<Long> published = new LinkedList<>();
    private List<Long> developed = new LinkedList<>();

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getStartDateCategory() {
        return startDateCategory;
    }

    public void setStartDateCategory(Integer startDateCategory) {
        this.startDateCategory = startDateCategory;
    }

    public Integer getChangeDateCategory() {
        return changeDateCategory;
    }

    public void setChangeDateCategory(Integer changeDateCategory) {
        this.changeDateCategory = changeDateCategory;
    }

    public List<Long> getPublished() {
        return published;
    }

    public void setPublished(List<Long> published) {
        this.published = published;
    }

    public void addPublished(Long published) {
        this.published.add(published);
    }

    public List<Long> getDeveloped() {
        return developed;
    }

    public void setDeveloped(List<Long> developed) {
        this.developed = developed;
    }

    public void addDeveloped(Long developed) {
        this.developed.add(developed);
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Long getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Long changeDate) {
        this.changeDate = changeDate;
    }

    public Long getChangedCompanyId() {
        return changedCompanyId;
    }

    public void setChangedCompanyId(Long changedCompanyId) {
        this.changedCompanyId = changedCompanyId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<company>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, description, "description");
        appendIfNotNull(sb, slug, "slug");
        appendIfNotNull(sb, url, "url");
        appendIfNotNull(sb, createdAt, "createdAt");
        appendIfNotNull(sb, updatedAt, "updatedAt");
        appendIfNotNull(sb, startDateCategory, "startDateCategory");
        appendIfNotNull(sb, changeDateCategory, "changeDateCategory");
        appendIfNotNull(sb, country, "country");
        appendIfNotNull(sb, website, "website");
        appendIfNotNull(sb, twitter, "twitter");
        appendIfNotNull(sb, startDate, "startDate");
        appendIfNotNull(sb, changeDate, "changeDate");
        appendIfNotNull(sb, parent, "parent");
        appendIfNotNull(sb, changedCompanyId, "changedCompanyId");
        if (!published.isEmpty()) {
            sb.append("<published>");
            for (int i = 0; i < published.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(published.get(i));
            }
            sb.append("</published>");
        }
        if (!developed.isEmpty()) {
            sb.append("<developed>");
            for (int i = 0; i < developed.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(developed.get(i));
            }
            sb.append("</developed>");
        }
        sb.append("<company>");
        return sb.toString();
    }
}
