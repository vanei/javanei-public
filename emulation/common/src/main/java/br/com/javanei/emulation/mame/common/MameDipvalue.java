package br.com.javanei.emulation.mame.common;

import br.com.javanei.common.util.StringUtil;
import java.io.Serializable;

public class MameDipvalue implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private Long value;
    private Boolean _default; // (yes|no) "no"

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public void setValue(String value) {
        this.value = new Long(value);
    }

    public Boolean getDefault() {
        return _default;
    }

    public void setDefault(Boolean _default) {
        this._default = _default;
    }

    public void setDefault(String _default) {
        if (_default != null)
            this._default = _default.equalsIgnoreCase("yes") || _default.equalsIgnoreCase("true");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t\t<dipvalue");
        if (this.name != null) {
            sb.append(" name=\"").append(StringUtil.escapeXMLEntities(this.name)).append("\"");
        }
        if (this.value != null) {
            sb.append(" value=\"").append(this.value).append("\"");
        }
        if (this._default != null) {
            sb.append(" default=\"").append(this._default ? "yes" : "no").append("\"");
        }
        sb.append("/>").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
