package br.com.javanei.emulation.catalog;

import br.com.javanei.emulation.catalog.nointro.parser.NoIntroNameParser;

public final class GameNameParserFactory {
    public static GameNameParser getParser(CatalogGameEnum catalog) throws Exception {
        if (catalog == CatalogGameEnum.NoIntro) {
            return new NoIntroNameParser();
        }
        //TODO: Criar Exception
        throw new Exception(catalog.toString());
    }

    public static GameNameParser getParser(String catalog) throws Exception {
        return getParser(CatalogGameEnum.valueOf(catalog));
    }
}
