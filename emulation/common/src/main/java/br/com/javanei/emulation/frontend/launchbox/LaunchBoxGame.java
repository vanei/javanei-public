package br.com.javanei.emulation.frontend.launchbox;

import java.io.Serializable;
import java.util.Objects;

public class LaunchBoxGame implements Serializable {
    private static final long serialVersionUID = 1L;

    private String applicationPath;
    private String commandLine;
    private Boolean completed;
    private String configurationCommandLine;
    private String configurationPath;
    private String dateAdded;
    private String dateModified;
    private String developer;
    private String dosBoxConfigurationPath;
    private String emulator;
    private Boolean favorite;
    private String ID;
    private String manualPath;
    private String musicPath;
    private String notes;
    private String platform;
    private String publisher;
    private String rating;
    private String releaseDate;
    private String rootFolder;
    private Boolean scummVMAspectCorrection;
    private Boolean scummVMFullscreen;
    private String scummVMGameDataFolderPath;
    private String scummVMGameType;
    private String sortTitle;
    private String source;
    private Float starRating;
    private String status;
    private String databaseID;
    private String wikipediaURL;
    private String title;
    private Boolean useDosBox;
    private Boolean useScummVM;
    private String version;
    private String series;
    private String playMode;
    private String region;
    private Integer playCount;
    private Boolean portable;
    private String videoPath;
    private Boolean hide;
    private Boolean broken;
    private String genre;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getApplicationPath() {
        return applicationPath;
    }

    public void setApplicationPath(String applicationPath) {
        this.applicationPath = applicationPath;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getConfigurationCommandLine() {
        return configurationCommandLine;
    }

    public void setConfigurationCommandLine(String configurationCommandLine) {
        this.configurationCommandLine = configurationCommandLine;
    }

    public String getConfigurationPath() {
        return configurationPath;
    }

    public void setConfigurationPath(String configurationPath) {
        this.configurationPath = configurationPath;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getDosBoxConfigurationPath() {
        return dosBoxConfigurationPath;
    }

    public void setDosBoxConfigurationPath(String dosBoxConfigurationPath) {
        this.dosBoxConfigurationPath = dosBoxConfigurationPath;
    }

    public String getEmulator() {
        return emulator;
    }

    public void setEmulator(String emulator) {
        this.emulator = emulator;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getManualPath() {
        return manualPath;
    }

    public void setManualPath(String manualPath) {
        this.manualPath = manualPath;
    }

    public String getMusicPath() {
        return musicPath;
    }

    public void setMusicPath(String musicPath) {
        this.musicPath = musicPath;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Boolean getScummVMAspectCorrection() {
        return scummVMAspectCorrection;
    }

    public void setScummVMAspectCorrection(Boolean scummVMAspectCorrection) {
        this.scummVMAspectCorrection = scummVMAspectCorrection;
    }

    public Boolean getScummVMFullscreen() {
        return scummVMFullscreen;
    }

    public void setScummVMFullscreen(Boolean scummVMFullscreen) {
        this.scummVMFullscreen = scummVMFullscreen;
    }

    public String getScummVMGameDataFolderPath() {
        return scummVMGameDataFolderPath;
    }

    public void setScummVMGameDataFolderPath(String scummVMGameDataFolderPath) {
        this.scummVMGameDataFolderPath = scummVMGameDataFolderPath;
    }

    public String getScummVMGameType() {
        return scummVMGameType;
    }

    public void setScummVMGameType(String scummVMGameType) {
        this.scummVMGameType = scummVMGameType;
    }

    public String getSortTitle() {
        return sortTitle;
    }

    public void setSortTitle(String sortTitle) {
        this.sortTitle = sortTitle;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Float getStarRating() {
        return starRating;
    }

    public void setStarRating(Float starRating) {
        this.starRating = starRating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDatabaseID() {
        return databaseID;
    }

    public void setDatabaseID(String databaseID) {
        this.databaseID = databaseID;
    }

    public String getWikipediaURL() {
        return wikipediaURL;
    }

    public void setWikipediaURL(String wikipediaURL) {
        this.wikipediaURL = wikipediaURL;
    }

    public Boolean getUseDosBox() {
        return useDosBox;
    }

    public void setUseDosBox(Boolean useDosBox) {
        this.useDosBox = useDosBox;
    }

    public Boolean getUseScummVM() {
        return useScummVM;
    }

    public void setUseScummVM(Boolean useScummVM) {
        this.useScummVM = useScummVM;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getPlayMode() {
        return playMode;
    }

    public void setPlayMode(String playMode) {
        this.playMode = playMode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getPlayCount() {
        return playCount;
    }

    public void setPlayCount(Integer playCount) {
        this.playCount = playCount;
    }

    public Boolean getPortable() {
        return portable;
    }

    public void setPortable(Boolean portable) {
        this.portable = portable;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public Boolean getHide() {
        return hide;
    }

    public void setHide(Boolean hide) {
        this.hide = hide;
    }

    public Boolean getBroken() {
        return broken;
    }

    public void setBroken(Boolean broken) {
        this.broken = broken;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LaunchBoxGame that = (LaunchBoxGame) o;
        return Objects.equals(platform, that.platform) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(platform, title);
    }
}
