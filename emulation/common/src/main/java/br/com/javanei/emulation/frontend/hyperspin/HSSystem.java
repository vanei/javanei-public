package br.com.javanei.emulation.frontend.hyperspin;

public class HSSystem {
    public final String name;
    public final boolean validateCRC;
    public final boolean allowZip;
    public final String dbDir;
    public final String dbFile;
    public final String romsDir;

    public String listname;
    public String lastlistupdate;
    public String listversion;
    public String exporterversion;

    public HSSystem(String name, boolean validateCRC, boolean allowZip, String dbDir, String romsDir) {
        this.name = name;
        this.validateCRC = validateCRC;
        this.allowZip = allowZip;
        this.dbDir = dbDir;
        this.romsDir = romsDir;
        this.dbFile = this.dbDir + "/" + name + ".xml";
    }

    @Override
    public String toString() {
        return "HSSystem{" +
                "name='" + name + '\'' +
                ", validateCRC=" + validateCRC +
                ", allowZip=" + allowZip +
                ", dbFile='" + dbFile + '\'' +
                ", romsDir='" + romsDir + '\'' +
                '}';
    }
}
