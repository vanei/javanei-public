package br.com.javanei.emulation.frontend.launchbox.metadata;

public enum LaunchBoxRegion {
    North_America("North America"),
    Europe("Europe"),
    Asia("Asia"),
    South_America("South America"),
    World("World"),
    Oceania("Oceania");

    private final String name;

    private LaunchBoxRegion(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
