package br.com.javanei.emulation.catalog;

import br.com.javanei.emulation.game.GameProcessingMessage;
import java.util.List;

public interface GameNameParser {

    public List<GameProcessingMessage> parseGameName(String platform, CatalogGameVO game) throws Exception;
}
