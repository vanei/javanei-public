package br.com.javanei.emulation.onlinedb.thegamesdb;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "platform")
public class TGDBPlatform implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String console;
    private String controller;
    private String overview;
    private String developer;
    private String manufacturer;
    private String cpu;
    private String memory;
    private String graphics;
    private String sound;
    private String display;
    private String media;
    private String maxcontrollers;
    private String rating;
    private String youtube;
    private String alias;
    //Images

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConsole() {
        return console;
    }

    public void setConsole(String console) {
        this.console = console;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getGraphics() {
        return graphics;
    }

    public void setGraphics(String graphics) {
        this.graphics = graphics;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getMaxcontrollers() {
        return maxcontrollers;
    }

    public void setMaxcontrollers(String maxcontrollers) {
        this.maxcontrollers = maxcontrollers;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        rating = rating;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<platform>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, alias, "alias");
        appendIfNotNull(sb, console, "console");
        appendIfNotNull(sb, controller, "controller");
        appendIfNotNull(sb, overview, "overview");
        appendIfNotNull(sb, developer, "developer");
        appendIfNotNull(sb, manufacturer, "manufacturer");
        appendIfNotNull(sb, cpu, "cpu");
        appendIfNotNull(sb, memory, "memory");
        appendIfNotNull(sb, graphics, "graphics");
        appendIfNotNull(sb, sound, "sound");
        appendIfNotNull(sb, display, "display");
        appendIfNotNull(sb, maxcontrollers, "maxcontrollers");
        appendIfNotNull(sb, rating, "rating");
        appendIfNotNull(sb, youtube, "youtube");
        sb.append("</platform>");
        return sb.toString();
    }
}
