package br.com.javanei.emulation.platform;

public enum StorageFormatEnum {
    file("file"),
    zip("zip"),
    singleZip("singleZip");

    private final String name;

    private StorageFormatEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public StorageFormatEnum fromName(String name) {
        if (name.equals(file.name()))
            return file;
        else if (name.equals(zip.name()))
            return zip;
        else if (name.equals(singleZip.name()))
            return singleZip;
        //TODO: Lançar exception
        return null;
    }
}
