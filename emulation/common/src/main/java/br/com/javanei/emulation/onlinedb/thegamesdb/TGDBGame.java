package br.com.javanei.emulation.onlinedb.thegamesdb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "game")
public class TGDBGame implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String gameTitle;
    private String platformId;
    private String platform;
    private String releaseDate;
    private String overview;
    private String esrb;
    //Genres
    private String players;
    private String CoOp;
    private String youtube;
    private String publisher;
    private String developer;
    private String rating;
    private String actors;
    private List<String> alternateTitles = new LinkedList<>();
    private List<String> genres = new LinkedList<>();
    private List<TGDBGame> similar = new LinkedList<>();
    //Similar
    //Images

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGameTitle() {
        return gameTitle;
    }

    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getEsrb() {
        return esrb;
    }

    public void setEsrb(String esrb) {
        this.esrb = esrb;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public String getCoOp() {
        return CoOp;
    }

    public void setCoOp(String coOp) {
        CoOp = coOp;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public List<String> getAlternateTitles() {
        return alternateTitles;
    }

    public void setAlternateTitles(List<String> alternateTitles) {
        this.alternateTitles = alternateTitles;
    }

    public void addAlternateTitle(String title) {
        this.alternateTitles.add(title);
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public void addGenre(String genre) {
        this.genres.add(genre);
    }

    public List<TGDBGame> getSimilar() {
        return similar;
    }

    public void setSimilar(List<TGDBGame> similar) {
        this.similar = similar;
    }

    public void addSimilar(TGDBGame similar) {
        this.similar.add(similar);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<game>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, gameTitle, "gameTitle");
        appendIfNotNull(sb, platformId, "platformId");
        appendIfNotNull(sb, platform, "platform");
        appendIfNotNull(sb, releaseDate, "releaseDate");
        appendIfNotNull(sb, overview, "overview");
        appendIfNotNull(sb, esrb, "esrb");
        appendIfNotNull(sb, players, "players");
        appendIfNotNull(sb, CoOp, "Co-Op");
        appendIfNotNull(sb, youtube, "youtube");
        appendIfNotNull(sb, publisher, "publisher");
        appendIfNotNull(sb, developer, "developer");
        appendIfNotNull(sb, rating, "rating");
        appendIfNotNull(sb, actors, "actors");
        if (!this.alternateTitles.isEmpty()) {
            sb.append("<alternateTitles>");
            for (String s : this.alternateTitles) {
                appendIfNotNull(sb, s, "title");
            }
            sb.append("</alternateTitles>");
        }
        if (!this.genres.isEmpty()) {
            sb.append("<genres>");
            for (String s : this.genres) {
                appendIfNotNull(sb, s, "genre");
            }
            sb.append("</genres>");
        }
        if (!this.similar.isEmpty()) {
            sb.append("<similar>");
            for (TGDBGame similar : this.similar) {
                sb.append(similar.toString());
            }
            sb.append("</similar>");
        }
        sb.append("</game>");
        return sb.toString();
    }
}
