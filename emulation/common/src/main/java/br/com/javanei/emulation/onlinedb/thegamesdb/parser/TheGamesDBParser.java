package br.com.javanei.emulation.onlinedb.thegamesdb.parser;

import br.com.javanei.common.util.HttpRequestResult;
import br.com.javanei.common.util.HttpUtil;
import br.com.javanei.emulation.onlinedb.thegamesdb.TGDBGame;
import br.com.javanei.emulation.onlinedb.thegamesdb.TGDBPlatform;
import br.com.javanei.emulation.onlinedb.thegamesdb.TheGamesDBConstant;
import java.io.ByteArrayInputStream;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TheGamesDBParser {
    public static final List<TGDBPlatform> getPlatforms() throws Exception {
        List<TGDBPlatform> result = new LinkedList<>();
        HttpRequestResult httpResult = HttpUtil.get(TheGamesDBConstant.BASE_API_URL + "GetPlatformsList.php",
                TheGamesDBConstant.getConnectionHeader(), null);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(httpResult.getContent()));
        NodeList dataNodes = doc.getChildNodes();
        for (int i = 0; i < dataNodes.getLength(); i++) {
            Node dataNode = dataNodes.item(i);
            if (dataNode.getChildNodes().getLength() == 0)
                continue;
            if (dataNode.getNodeName().equals("Data")) {
                NodeList platformsNodes = dataNode.getChildNodes();
                for (int j = 0; j < platformsNodes.getLength(); j++) {
                    Node platformsNode = platformsNodes.item(j);
                    if (platformsNode.getChildNodes().getLength() == 0)
                        continue;
                    if (platformsNode.getNodeName().equals("Platforms")) {
                        NodeList platformNodes = platformsNode.getChildNodes();
                        for (int k = 0; k < platformNodes.getLength(); k++) {
                            Node platformNode = platformNodes.item(k);
                            if (platformNode.getChildNodes().getLength() == 0)
                                continue;
                            if (platformNode.getNodeName().equals("Platform")) {
                                result.add(parsePlatformNode(platformNode));
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    public static List<TGDBGame> getPlatformGamesByAlias(String platformAlias) throws Exception {
        List<TGDBGame> result = new LinkedList<>();
        HttpRequestResult httpResult = HttpUtil.get(TheGamesDBConstant.BASE_API_URL
                        + "PlatformGames.php?platform=" + URLEncoder.encode(platformAlias, "UTF-8"),
                TheGamesDBConstant.getConnectionHeader(), null);

        return parseGetGame(httpResult);
    }

    public static TGDBPlatform getPlatform(Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(TheGamesDBConstant.BASE_API_URL
                        + "GetPlatform.php?id=" + id,
                TheGamesDBConstant.getConnectionHeader(), null);

        List<TGDBPlatform> list = parseGetPlatform(httpResult);
        TGDBPlatform result = (list.size() > 0 ? list.get(0) : null);
        return result;
    }

    public static TGDBGame getGame(Long id) throws Exception {
        HttpRequestResult httpResult = HttpUtil.get(TheGamesDBConstant.BASE_API_URL
                        + "GetGame.php?id=" + id,
                TheGamesDBConstant.getConnectionHeader(), null);

        List<TGDBGame> list = parseGetGame(httpResult);
        TGDBGame result = (list.size() > 0 ? list.get(0) : null);
        return result;
    }

    public static List<TGDBGame> getGameByName(String name, String platform) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(TheGamesDBConstant.BASE_API_URL).append("GetGame.php?name=").append(URLEncoder.encode(name, "UTF-8"));
        if (platform != null) {
            sb.append("&platform=").append(URLEncoder.encode(platform, "UTF-8"));
        }
        HttpRequestResult httpResult = HttpUtil.get(sb.toString(), TheGamesDBConstant.getConnectionHeader(), null);

        List<TGDBGame> result = parseGetGame(httpResult);
        return result;
    }

    public static List<TGDBGame> getGameByExactName(String name, String platform) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(TheGamesDBConstant.BASE_API_URL).append("GetGame.php?exactname=").append(URLEncoder.encode(name, "UTF-8"));
        if (platform != null) {
            sb.append("&platform=").append(URLEncoder.encode(platform, "UTF-8"));
        }
        HttpRequestResult httpResult = HttpUtil.get(sb.toString(), TheGamesDBConstant.getConnectionHeader(), null);

        List<TGDBGame> result = parseGetGame(httpResult);
        return result;
    }

    public static List<TGDBGame> getPlatformGames(Long id) throws Exception {
        List<TGDBGame> result = new LinkedList<>();
        HttpRequestResult httpResult = HttpUtil.get(TheGamesDBConstant.BASE_API_URL
                        + "GetPlatformGames.php?platform=" + id,
                TheGamesDBConstant.getConnectionHeader(), null);

        return parseGetGame(httpResult);
    }

    private static List<TGDBGame> parseGetGame(HttpRequestResult httpResult) throws Exception {
        List<TGDBGame> result = new LinkedList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(httpResult.getContent()));
        NodeList dataNodes = doc.getChildNodes();
        for (int i = 0; i < dataNodes.getLength(); i++) {
            Node dataNode = dataNodes.item(i);
            if (dataNode.getChildNodes().getLength() == 0)
                continue;
            if (dataNode.getNodeName().equals("Data")) {
                NodeList gameList = dataNode.getChildNodes();
                for (int j = 0; j < gameList.getLength(); j++) {
                    Node gameNode = gameList.item(j);
                    if (gameNode.getChildNodes().getLength() == 0)
                        continue;
                    if (gameNode.getNodeName().equals("Game")) {
                        result.add(parseGameNode(gameNode));
                    }
                }
            }
        }

        return result;
    }

    private static List<TGDBPlatform> parseGetPlatform(HttpRequestResult httpResult) throws Exception {
        List<TGDBPlatform> result = new LinkedList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(httpResult.getContent()));
        NodeList dataNodes = doc.getChildNodes();
        for (int i = 0; i < dataNodes.getLength(); i++) {
            Node dataNode = dataNodes.item(i);
            if (dataNode.getChildNodes().getLength() == 0)
                continue;
            if (dataNode.getNodeName().equals("Data")) {
                NodeList platformList = dataNode.getChildNodes();
                for (int j = 0; j < platformList.getLength(); j++) {
                    Node gameNode = platformList.item(j);
                    if (gameNode.getChildNodes().getLength() == 0)
                        continue;
                    if (gameNode.getNodeName().equals("Platform")) {
                        result.add(parsePlatformNode(gameNode));
                    }
                }
            }
        }

        return result;
    }

    /**
     * Faz o parser de uma plataforma.
     *
     * @param node
     * @return
     * @throws Exception
     */
    private static final TGDBPlatform parsePlatformNode(Node node) throws Exception {
        TGDBPlatform platform = new TGDBPlatform();
        NodeList contentList = node.getChildNodes();
        for (int l = 0; l < contentList.getLength(); l++) {
            Node content = contentList.item(l);
            switch (content.getNodeName().toLowerCase()) {
                case "id":
                    platform.setId(new Long(content.getTextContent().trim()));
                    break;
                case "platform":
                case "name":
                    platform.setName(content.getTextContent().trim());
                    break;
                case "alias":
                    platform.setAlias(content.getTextContent().trim());
                    break;
                case "console":
                    platform.setConsole(content.getTextContent().trim());
                    break;
                case "controller":
                    platform.setController(content.getTextContent().trim());
                    break;
                case "overview":
                    platform.setOverview(content.getTextContent().trim());
                    break;
                case "developer":
                    platform.setDeveloper(content.getTextContent().trim());
                    break;
                case "manufacturer":
                    platform.setManufacturer(content.getTextContent().trim());
                    break;
                case "cpu":
                    platform.setCpu(content.getTextContent().trim());
                    break;
                case "memory":
                    platform.setMemory(content.getTextContent().trim());
                    break;
                case "graphics":
                    platform.setGraphics(content.getTextContent().trim());
                    break;
                case "sound":
                    platform.setSound(content.getTextContent().trim());
                    break;
                case "display":
                    platform.setDisplay(content.getTextContent().trim());
                    break;
                case "media":
                    platform.setMedia(content.getTextContent().trim());
                    break;
                case "maxcontrollers":
                    platform.setMaxcontrollers(content.getTextContent().trim());
                    break;
                case "rating":
                    platform.setRating(content.getTextContent().trim());
                    break;
                case "youtube":
                    platform.setYoutube(content.getTextContent().trim());
                    break;
                case "images":
                    //TODO:
                    break;
                case "#text":
                    break;
                default:
                    System.err.println("EEEEEE Tag nao tratada: " + content.getNodeName());
                    break;
            }
        }
        return platform;
    }

    /**
     * Faz o parser de um Game
     *
     * @param node
     * @return
     * @throws Exception
     */
    private static final TGDBGame parseGameNode(Node node) throws Exception {
        TGDBGame game = new TGDBGame();
        NodeList contentList = node.getChildNodes();
        for (int l = 0; l < contentList.getLength(); l++) {
            Node content = contentList.item(l);
            switch (content.getNodeName().toLowerCase()) {
                case "id":
                    game.setId(new Long(content.getTextContent().trim()));
                    break;
                case "gametitle":
                    game.setGameTitle(content.getTextContent().trim());
                    break;
                case "platformid":
                    game.setPlatformId(content.getTextContent().trim());
                    break;
                case "platform":
                    game.setPlatform(content.getTextContent().trim());
                    break;
                case "releasedate":
                    game.setReleaseDate(content.getTextContent().trim());
                    break;
                case "overview":
                    game.setOverview(content.getTextContent().trim());
                    break;
                case "esrb":
                    game.setEsrb(content.getTextContent().trim());
                    break;
                case "players":
                    game.setPlayers(content.getTextContent().trim());
                    break;
                case "co-op":
                    game.setCoOp(content.getTextContent().trim());
                    break;
                case "youtube":
                    game.setYoutube(content.getTextContent().trim());
                    break;
                case "publisher":
                    game.setPublisher(content.getTextContent().trim());
                    break;
                case "developer":
                    game.setDeveloper(content.getTextContent().trim());
                    break;
                case "rating":
                    game.setRating(content.getTextContent().trim());
                    break;
                case "images":
                    //TODO:
                    break;
                case "actors":
                    game.setActors(content.getTextContent().trim());
                    break;
                case "alternatetitles":
                    NodeList nl = content.getChildNodes();
                    for (int m = 0; m < nl.getLength(); m++) {
                        Node n = nl.item(m);
                        if (n.getNodeName().equalsIgnoreCase("title")) {
                            game.addAlternateTitle(n.getTextContent().trim());
                        }
                    }
                    break;
                case "genres":
                    NodeList nlg = content.getChildNodes();
                    for (int m = 0; m < nlg.getLength(); m++) {
                        Node n = nlg.item(m);
                        if (n.getNodeName().equalsIgnoreCase("genre")) {
                            game.addGenre(n.getTextContent().trim());
                        }
                    }
                    break;
                case "similar":
                    NodeList nls = content.getChildNodes();
                    for (int m = 0; m < nls.getLength(); m++) {
                        Node n = nls.item(m);
                        if (n.getNodeName().equalsIgnoreCase("game")) {
                            TGDBGame similar = new TGDBGame();
                            NodeList nlsg = n.getChildNodes();
                            for (int o = 0; o < nlsg.getLength(); o++) {
                                Node nodeg = nlsg.item(o);
                                if (nodeg.getNodeName().equalsIgnoreCase("id")) {
                                    similar.setId(new Long(nodeg.getTextContent().trim()));
                                } else if (nodeg.getNodeName().equalsIgnoreCase("platformid")) {
                                    similar.setPlatformId(nodeg.getTextContent().trim());
                                }
                            }
                            game.addSimilar(similar);
                        }
                    }
                    break;
                case "#text":
                case "thumb":
                    break;
                default:
                    System.err.println("###### " + content.getNodeName());
                    break;
            }
        }
        return game;
    }
}
