package br.com.javanei.emulation.mame.common.mamesoftwarelist;

import java.io.Serializable;

public class MameSoftwareDisk implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String sha1;
    private String status; // (baddump|nodump|good) "good"
    private Boolean writeable; //(yes|no) "no";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getWriteable() {
        return writeable;
    }

    public void setWriteable(Boolean writeable) {
        this.writeable = writeable;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\t\t\t\t\t<disk name=\"").append(this.name).append("\"");
        if (this.sha1 != null) {
            sb.append(" sha1=\"").append(this.sha1).append("\"");
        }
        if (this.status != null) {
            sb.append(" status=\"").append(this.status).append("\"");
        }
        if (this.writeable != null) {
            sb.append(" writeable=\"").append(this.writeable ? "yes" : "no").append("\"");
        }
        sb.append("/>").append(System.getProperty("line.separator"));

        return sb.toString();
    }
}
