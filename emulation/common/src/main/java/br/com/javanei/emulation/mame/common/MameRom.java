package br.com.javanei.emulation.mame.common;

import br.com.javanei.common.util.StringUtil;
import java.io.Serializable;

public class MameRom implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String bios;
    private Integer size;
    private String crc;
    private String sha1;
    private String merge;
    private String region;
    private String offset;
    private String status; // (baddump|nodump|good) "good"
    private Boolean optional; // (yes|no) "no"

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBios() {
        return bios;
    }

    public void setBios(String bios) {
        this.bios = bios;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public void setSize(String size) {
        this.size = new Integer(size);
    }

    public String getCrc() {
        return crc;
    }

    public void setCrc(String crc) {
        this.crc = crc;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    public String getMerge() {
        return merge;
    }

    public void setMerge(String merge) {
        this.merge = merge;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getOptional() {
        return optional;
    }

    public void setOptional(Boolean optional) {
        this.optional = optional;
    }

    public void setOptional(String optional) {
        if (optional != null)
            this.optional = optional.equalsIgnoreCase("yes") || optional.equalsIgnoreCase("true");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t<rom");
        if (this.name != null) {
            sb.append(" name=\"").append(StringUtil.escapeXMLEntities(this.name)).append("\"");
        }
        if (this.merge != null) {
            sb.append(" merge=\"").append(StringUtil.escapeXMLEntities(this.merge)).append("\"");
        }
        if (this.bios != null) {
            sb.append(" bios=\"").append(this.bios).append("\"");
        }
        if (this.size != null) {
            sb.append(" size=\"").append(this.size).append("\"");
        }
        if (this.crc != null) {
            sb.append(" crc=\"").append(this.crc).append("\"");
        }
        if (this.sha1 != null) {
            sb.append(" sha1=\"").append(this.sha1).append("\"");
        }
        if (this.status != null) {
            sb.append(" status=\"").append(this.status).append("\"");
        }
        if (this.region != null) {
            sb.append(" region=\"").append(this.region).append("\"");
        }
        if (this.offset != null) {
            sb.append(" offset=\"").append(this.offset).append("\"");
        }
        if (this.optional != null) {
            sb.append(" optional=\"").append(this.optional ? "yes" : "no").append("\"");
        }
        sb.append("/>").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
