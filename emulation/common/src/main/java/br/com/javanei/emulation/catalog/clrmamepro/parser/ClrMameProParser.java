package br.com.javanei.emulation.catalog.clrmamepro.parser;

import br.com.javanei.emulation.InvalidDatFileFormatException;
import br.com.javanei.emulation.UnknownTagException;
import br.com.javanei.emulation.catalog.CatalogGameEnum;
import br.com.javanei.emulation.catalog.CatalogGameFileVO;
import br.com.javanei.emulation.catalog.CatalogGameVO;
import br.com.javanei.emulation.catalog.CatalogParser;
import br.com.javanei.emulation.catalog.GameNameParserFactory;
import br.com.javanei.emulation.catalog.GameParserInfo;
import br.com.javanei.emulation.common.PlatformVO;
import br.com.javanei.emulation.game.GameProcessingMessage;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

public class ClrMameProParser extends CatalogParser {

    private final byte[] fileContent;
    private final PlatformVO platform;
    private final GameParserInfo gameParserInfo;
    private final List<GameProcessingMessage> messages;
    private final String fileName;
    private CatalogGameEnum catalog;

    public ClrMameProParser(PlatformVO platform, String fileName, byte[] fileContent) {
        this.platform = platform;
        this.fileContent = fileContent;
        this.gameParserInfo = new GameParserInfo();
        this.messages = new LinkedList<>();
        this.fileName = fileName;
    }

    public GameParserInfo execute() throws Exception {
        List<CatalogGameVO> games = new LinkedList<>();
        try (BufferedReader reader = new BufferedReader(new StringReader(new String(fileContent)))) {
            String line = reader.readLine().trim();
            String[] ss = line.split(" ");
            if (!ss[0].trim().equals("clrmamepro")) {
                throw new InvalidDatFileFormatException(ss[0].trim());
            }
            while (line != null && !line.isEmpty() && !line.equals(")")) {
                if (line.startsWith("name")) {
                    this.gameParserInfo.setName(line.substring(line.indexOf("\"") + 1, line.length() - 1));
                } else if (line.startsWith("description")) {
                    this.gameParserInfo.setDescription(line.substring(line.indexOf("\"") + 1, line.length() - 1));
                } else if (line.startsWith("version")) {
                    if (line.indexOf("\"") > 0) {
                        this.gameParserInfo.setVersion(line.substring(line.indexOf("\"") + 1, line.length()));
                    } else {
                        this.gameParserInfo.setVersion(line.split(" ")[1]);
                    }
                } else if (line.startsWith("comment")) {
                    this.gameParserInfo.setComment(line.substring(line.indexOf("\"") + 1, line.length() - 1));
                    if (this.gameParserInfo.getComment().contains("no-intro")) {
                        this.catalog = CatalogGameEnum.NoIntro;
                    }
                }
                line = reader.readLine().trim();
            }

            line = reader.readLine();
            List<String> lines = new LinkedList<>();
            while (line != null) {
                line = line.trim();
                if (!line.isEmpty()) {
                    lines.add(line);
                }
                line = reader.readLine();
            }

            CatalogGameVO game = null;
            for (int i = 0; i < lines.size(); i++) {
                line = lines.get(i);
                try {
                    if (line.startsWith("game")) {
                        game = new CatalogGameVO();
                        game.setCatalogName(this.gameParserInfo.getName());
                        game.setCatalogFile(this.fileName);
                    } else if (line.startsWith("name")) {
                        if (game != null) {
                            game.setName(line.substring(line.indexOf("\"") + 1, line.length() - 1));
                        }
                    } else if (line.startsWith("description")) {
                        if (game != null) {
                            game.setDescription(line.substring(line.indexOf("\"") + 1, line.length() - 1));
                        }
                    } else if (line.startsWith("rom")) {
                        if (game != null) {
                            String romLine = line.substring(line.indexOf("(") + 1, line.length() - 1).trim();
                            game.addRom(processROMLine(romLine));
                        }
                    } else if (line.startsWith("serial")) {
                        if (game != null) {
                            game.setSerial(line.substring(line.indexOf("\"") + 1, line.length() - 1));
                        }
                    } else if (line.equals(")")) {
                        if (game != null) {
                            try {
                                game.setCatalog(this.catalog);
                                game.setCatalogVersion(gameParserInfo.getVersion());
                                List<GameProcessingMessage> msgs = GameNameParserFactory.getParser(this.catalog).parseGameName(this.platform.getName(), game);
                                fireMessage(game.getName() + ": OK", msgs);
                                games.add(game);
                            } catch (Exception ex) {
                                fireMessageError(game.getName(), ex);
                            }
                        }
                    } else {
                        fireMessageError(null, new UnknownTagException(line));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    fireMessageError(null, ex);
                    game = null;
                }
            }

        } catch (Exception ex) {
            this.fireMessageError(null, ex);
        }
        this.gameParserInfo.setGames(games);
        this.gameParserInfo.addMessages(this.messages);

        return this.gameParserInfo;
    }

    private CatalogGameFileVO processROMLine(String romLine) throws Exception {
        int pos = romLine.indexOf("\"") + 1;
        int endpos = romLine.indexOf("\"", pos + 1);
        CatalogGameFileVO gf = new CatalogGameFileVO(romLine.substring(pos, endpos));

        String[] ss = romLine.substring(endpos + 2).trim().split(" ");
        for (int i = 0; i < ss.length; i++) {
            switch (ss[i]) {
                case "size":
                    gf.setSize(Long.parseLong(ss[++i]));
                    break;
                case "crc":
                    gf.setCrc(ss[++i]);
                    break;
                case "md5":
                    gf.setMd5(ss[++i]);
                    break;
                case "sha1":
                    gf.setSha1(ss[++i]);
                    break;
                case "flags":
                    gf.setFlags(ss[++i]);
                    break;
                default:
                    throw new UnknownTagException(ss[i]);
            }
        }

        return gf;
    }

    private void fireMessageError(String game, Exception ex) {
        StringBuilder sb = new StringBuilder();
        if (game != null) {
            sb.append(game).append(": ");
        }
        sb.append(ex.getClass().getName()).append(": ").append(ex.getLocalizedMessage()).append("\n");
        GameProcessingMessage msg = new GameProcessingMessage(GameProcessingMessage.Type.ERROR, sb.toString());
        this.messages.add(msg);
    }

    private void fireMessage(String message, List<GameProcessingMessage> msgs) {
        GameProcessingMessage msg = new GameProcessingMessage(GameProcessingMessage.Type.INFO, message);
        if (msgs != null && !msgs.isEmpty()) {
            msg.addMessages(msgs);
        }
        this.messages.add(msg);
    }
}
