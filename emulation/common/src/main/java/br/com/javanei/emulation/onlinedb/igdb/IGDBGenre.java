package br.com.javanei.emulation.onlinedb.igdb;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "genre")
public class IGDBGenre implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String slug;
    private String url;
    private Long createdAt;
    private Long updatedAt;
    //games

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<genre>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, slug, "slug");
        appendIfNotNull(sb, url, "url");
        appendIfNotNull(sb, createdAt, "createdAt");
        appendIfNotNull(sb, updatedAt, "updatedAt");
        sb.append("</genre>");
        return sb.toString();
    }
}
