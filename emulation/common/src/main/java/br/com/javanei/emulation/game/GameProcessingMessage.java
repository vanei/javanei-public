package br.com.javanei.emulation.game;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class GameProcessingMessage implements Serializable, Comparable<GameProcessingMessage> {
    private static final long serialVersionUID = 1L;

    @XmlElement
    private final Type type;
    @XmlElement
    private final String message;
    @XmlElement
    private final List<GameProcessingMessage> messages = new LinkedList<>();

    public GameProcessingMessage(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public void addMessage(GameProcessingMessage msg) {
        this.messages.add(msg);
    }

    public void addMessages(List<GameProcessingMessage> msgs) {
        this.messages.addAll(msgs);
    }

    public List<GameProcessingMessage> getMessages() {
        return messages;
    }

    public boolean isError() {
        if (this.type.equals(Type.ERROR)) {
            return true;
        }
        return this.messages.stream().anyMatch((msg) -> (msg.isError()));
    }

    public boolean isWarn() {
        if (!this.isError()) {
            if (this.type.equals(Type.WARN)) {
                return true;
            }
        }
        return this.messages.stream().anyMatch((msg) -> (msg.isWarn()));
    }

    public boolean isInfo() {
        return !this.isError() && !this.isWarn();
    }

    public List<GameProcessingMessage> sortMessagesByType() {
        Collections.sort(this.messages);
        return this.messages;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(type).append(": ").append(message);
        if (!this.messages.isEmpty()) {
            this.messages.stream().forEach((msg) -> {
                sb.append("\n\t").append(msg.toString());
            });
        }
        return sb.toString();
    }

    @Override
    public int compareTo(GameProcessingMessage o2) {
        int result = 0;
        if (this.isError()) {
            if (o2.isError()) {
                result = 0;
            } else {
                result = 1;
            }
        } else if (this.isWarn()) {
            if (o2.isError()) {
                result = -1;
            } else if (o2.isWarn()) {
                result = 0;
            } else {
                result = 1;
            }
        } else {
            if (o2.isInfo()) {
                result = 0;
            } else {
                result = -1;
            }
        }
        return result;
    }

    public enum Type {

        INFO,
        WARN,
        ERROR
    }
}
