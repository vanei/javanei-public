package br.com.javanei.emulation.catalog;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @see https://pt.wikipedia.org/wiki/Lista_de_jogos_para_Sega_CD
 * @see https://en.wikipedia.org/wiki/List_of_Sega_CD_games
 * @see http://gamesdbase.com/list.aspx
 * @see http://www.oldgamesfinder.com/
 */
@XmlRootElement(name = "developer")
public class CatalogGameDeveloper implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Map<String, CatalogGameDeveloper> developers = new HashMap<>();

    static {
        developers.put("Absolute Entertainment", new CatalogGameDeveloper("Absolute Entertainment"));
        developers.put("Access Software", new CatalogGameDeveloper("Access Software"));
        developers.put("Activision", new CatalogGameDeveloper("Activision"));
        developers.put("Alexandria, Inc.", new CatalogGameDeveloper("Alexandria, Inc."));
        developers.put("American Laser Games", new CatalogGameDeveloper("American Laser Games"));
        developers.put("Archer Communications", new CatalogGameDeveloper("Archer Communications"));
        developers.put("Arnowitz Studios", new CatalogGameDeveloper("Arnowitz Studios"));
        developers.put("Atlus Software", new CatalogGameDeveloper("Atlus Software"));
        developers.put("Beam Software", new CatalogGameDeveloper("Beam Software"));
        developers.put("Bullfrog Games", new CatalogGameDeveloper("Bullfrog Games"));
        developers.put("CSK Research Institute", new CatalogGameDeveloper("CSK Research Institute"));
        developers.put("CapDisc", new CatalogGameDeveloper("CapDisc"));
        developers.put("Capcom", new CatalogGameDeveloper("Capcom"));
        developers.put("Clockwork Games", new CatalogGameDeveloper("Clockwork Games"));
        developers.put("Compile", new CatalogGameDeveloper("Compile"));
        developers.put("Core Design", new CatalogGameDeveloper("Core Design"));
        developers.put("Cryo Interactive", new CatalogGameDeveloper("Cryo Interactive"));
        developers.put("DAPS", new CatalogGameDeveloper("DAPS"));
        developers.put("Data East", new CatalogGameDeveloper("Data East"));
        developers.put("Delphine Software", new CatalogGameDeveloper("Delphine Software"));
        developers.put("Digital Pictures", new CatalogGameDeveloper("Digital Pictures"));
        developers.put("Domark Software", new CatalogGameDeveloper("Domark Software"));
        developers.put("Don Bluth Studios", new CatalogGameDeveloper("Don Bluth Studios"));
        developers.put("Drew Pictures", new CatalogGameDeveloper("Drew Pictures"));
        developers.put("Dynamix", new CatalogGameDeveloper("Dynamix"));
        developers.put("Electronic Arts", new CatalogGameDeveloper("Electronic Arts"));
        developers.put("Epicenter Interactive", new CatalogGameDeveloper("Epicenter Interactive"));
        developers.put("Extended Play Productions", new CatalogGameDeveloper("Extended Play Productions"));
        developers.put("Falcom", new CatalogGameDeveloper("Falcom"));
        developers.put("Fuji Television", new CatalogGameDeveloper("Fuji Television"));
        developers.put("Game Arts", new CatalogGameDeveloper("Game Arts"));
        developers.put("High Score Productions", new CatalogGameDeveloper("High Score Productions"));
        developers.put("Holocronet", new CatalogGameDeveloper("Holocronet"));
        developers.put("Hot B", new CatalogGameDeveloper("Hot B"));
        developers.put("Hudson Soft", new CatalogGameDeveloper("Hudson Soft"));
        developers.put("Human Entertainment", new CatalogGameDeveloper("Human Entertainment"));
        developers.put("ICOM Simulations", new CatalogGameDeveloper("ICOM Simulations"));
        developers.put("Imagineering", new CatalogGameDeveloper("Imagineering"));
        developers.put("Imagitec Design", new CatalogGameDeveloper("Imagitec Design"));
        developers.put("Infogrames", new CatalogGameDeveloper("Infogrames"));
        developers.put("JASPAC", new CatalogGameDeveloper("JASPAC"));
        developers.put("JVC", new CatalogGameDeveloper("JVC"));
        developers.put("Koei", new CatalogGameDeveloper("Koei"));
        developers.put("Kogado Studio", new CatalogGameDeveloper("Kogado Studio"));
        developers.put("Konami", new CatalogGameDeveloper("Konami"));
        developers.put("LucasArts", new CatalogGameDeveloper("LucasArts"));
        developers.put("Malibu Interactive", new CatalogGameDeveloper("Malibu Interactive"));
        developers.put("Maxis", new CatalogGameDeveloper("Maxis"));
        developers.put("Micro Cabin", new CatalogGameDeveloper("Micro Cabin"));
        developers.put("Micro Design", new CatalogGameDeveloper("Micro Design"));
        developers.put("Micronet", new CatalogGameDeveloper("Micronet"));
        developers.put("Midway Games", new CatalogGameDeveloper("Midway Games"));
        developers.put("NCS", new CatalogGameDeveloper("NCS"));
        developers.put("Namco", new CatalogGameDeveloper("Namco"));
        developers.put("Natsume", new CatalogGameDeveloper("Natsume"));
        developers.put("New Level Software, Inc.", new CatalogGameDeveloper("New Level Software, Inc."));
        developers.put("New World Computing", new CatalogGameDeveloper("New World Computing"));
        developers.put("Nihon Bussan", new CatalogGameDeveloper("Nihon Bussan"));
        developers.put("Novotrade", new CatalogGameDeveloper("Novotrade"));
        developers.put("Origin Systems", new CatalogGameDeveloper("Origin Systems"));
        developers.put("Pandora's Box", new CatalogGameDeveloper("Pandora's Box"));
        developers.put("Park Place Productions", new CatalogGameDeveloper("Park Place Productions"));
        developers.put("Popcorn Software", new CatalogGameDeveloper("Popcorn Software"));
        developers.put("Probe Software", new CatalogGameDeveloper("Probe Software"));
        developers.put("Psygnosis", new CatalogGameDeveloper("Psygnosis"));
        developers.put("Rastersoft", new CatalogGameDeveloper("Rastersoft"));
        developers.put("Renovation Products", new CatalogGameDeveloper("Renovation Products"));
        developers.put("Ringler Studios", new CatalogGameDeveloper("Ringler Studios"));
        developers.put("Riot", new CatalogGameDeveloper("Riot"));
        developers.put("Rocket Science Games", new CatalogGameDeveloper("Rocket Science Games"));
        developers.put("SIMS", new CatalogGameDeveloper("SIMS"));
        developers.put("SNK", new CatalogGameDeveloper("SNK"));
        developers.put("Saban Entertainment", new CatalogGameDeveloper("Saban Entertainment"));
        developers.put("Saddleback Graphics", new CatalogGameDeveloper("Saddleback Graphics"));
        developers.put("Sales Curve", new CatalogGameDeveloper("Sales Curve"));
        developers.put("Sculptured Software", new CatalogGameDeveloper("Sculptured Software"));
        developers.put("Sega", new CatalogGameDeveloper("Sega"));
        developers.put("Sega Interactive", new CatalogGameDeveloper("Sega Interactive"));
        developers.put("Sensible Software", new CatalogGameDeveloper("Sensible Software"));
        developers.put("Shiny Entertainment", new CatalogGameDeveloper("Shiny Entertainment"));
        developers.put("Sierra", new CatalogGameDeveloper("Sierra"));
        developers.put("Sierra On-Line", new CatalogGameDeveloper("Sierra On-Line"));
        developers.put("Sims Computing Inc.", new CatalogGameDeveloper("Sims Computing Inc."));
        developers.put("Software Toolworks", new CatalogGameDeveloper("Software Toolworks"));
        developers.put("Sonic Co.", new CatalogGameDeveloper("Sonic Co."));
        developers.put("Sonic Team", new CatalogGameDeveloper("Sonic Team"));
        developers.put("Sony Imagesoft", new CatalogGameDeveloper("Sony Imagesoft"));
        developers.put("Sony Music Entertainment", new CatalogGameDeveloper("Sony Music Entertainment"));
        developers.put("Stargate Productions", new CatalogGameDeveloper("Stargate Productions"));
        developers.put("Sur de Wave", new CatalogGameDeveloper("Sur de Wave"));
        developers.put("System Sacom", new CatalogGameDeveloper("System Sacom"));
        developers.put("Taito", new CatalogGameDeveloper("Taito"));
        developers.put("Tecmo", new CatalogGameDeveloper("Tecmo"));
        developers.put("The Code Monkeys", new CatalogGameDeveloper("The Code Monkeys"));
        developers.put("The Learning Company", new CatalogGameDeveloper("The Learning Company"));
        developers.put("Tiertex Design Studios", new CatalogGameDeveloper("Tiertex Design Studios"));
        developers.put("Toei Animation", new CatalogGameDeveloper("Toei Animation"));
        developers.put("Traveller's Tales", new CatalogGameDeveloper("Traveller's Tales"));
        developers.put("Victor Entertainment", new CatalogGameDeveloper("Victor Entertainment"));
        developers.put("Victor Interactive Software", new CatalogGameDeveloper("Victor Interactive Software"));
        developers.put("Virtual Studio", new CatalogGameDeveloper("Virtual Studio"));
        developers.put("Western Technologies", new CatalogGameDeveloper("Western Technologies"));
        developers.put("Westwood Associates", new CatalogGameDeveloper("Westwood Associates"));
        developers.put("Wolf Team", new CatalogGameDeveloper("Wolf Team"));
        // Commodore
        developers.put("Software Country", new CatalogGameDeveloper("Software Country"));
        // NES
        developers.put("Hwang Shinwei", new CatalogGameDeveloper("Hwang Shinwei"));
        // Game Boy
        developers.put("Sachen", new CatalogGameDeveloper("Sachen"));
    }

    private final String developer;

    private CatalogGameDeveloper(String developer) {
        this.developer = developer;
    }

    public static boolean isDeveloper(String name) {
        return developers.containsKey(name);
    }

    public static CatalogGameDeveloper fromName(String name) {
        CatalogGameDeveloper dev = developers.get(name);
        if (dev == null) {
            //TODO: Criar uma exception
            throw new IllegalArgumentException(name);
        }
        return dev;
    }

    public static CatalogGameDeveloper getDeveloper(String name) {
        return developers.get(name);
    }

    @Override
    public String toString() {
        return this.developer;
    }

    public String getDeveloper() {
        return developer;
    }
}
