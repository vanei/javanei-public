package br.com.javanei.emulation.mame.common;

import java.io.Serializable;

public class MameSlotoption implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String devname;
    private Boolean _default; // (yes|no) "no"

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDevname() {
        return devname;
    }

    public void setDevname(String devname) {
        this.devname = devname;
    }

    public Boolean getDefault() {
        return _default;
    }

    public void setDefault(Boolean _default) {
        this._default = _default;
    }

    public void setDefault(String _default) {
        if (_default != null)
            this._default = _default.equalsIgnoreCase("yes") || _default.equalsIgnoreCase("true");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t\t<slotoption");
        if (this.name != null) {
            sb.append(" name=\"").append(this.name).append("\"");
        }
        if (this.devname != null) {
            sb.append(" devname=\"").append(this.devname).append("\"");
        }
        if (this._default != null) {
            sb.append(" default=\"").append(this._default ? "yes" : "no").append("\"");
        }
        sb.append("/>").append(Mame.LINE_SEPARATOR);
        return sb.toString();
    }
}
