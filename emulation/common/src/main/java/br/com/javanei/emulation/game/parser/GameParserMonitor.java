package br.com.javanei.emulation.game.parser;

import br.com.javanei.emulation.game.GameProcessingMessage;

public interface GameParserMonitor {
    default public void onMessage(GameProcessingMessage message, int percent) {
        //Nothing to to.
    }
}
