package br.com.javanei.emulation.onlinedb.giantbomb;

public final class GiantBombConstant {
    public static final String BASE_URL = "http://www.giantbomb.com/api/";
    public static final String PLATFORMS_BASE_URL = BASE_URL + "platforms/";
    public static final String PLATFORM_BASE_URL = BASE_URL + "platform/";
    public static final String GAMES_BASE_URL = BASE_URL + "games/";
    public static final String GAME_BASE_URL = BASE_URL + "game/";
    public static final String GENRES_BASE_URL = BASE_URL + "genres/";
    public static final String GENRE_BASE_URL = BASE_URL + "genre/";
    public static final String COMPANIES_BASE_URL = BASE_URL + "companies/";
    public static final String COMPANY_BASE_URL = BASE_URL + "company/";
    public static final String REGIONS_BASE_URL = BASE_URL + "regions/";
    public static final String REGION_BASE_URL = BASE_URL + "region/";
}
