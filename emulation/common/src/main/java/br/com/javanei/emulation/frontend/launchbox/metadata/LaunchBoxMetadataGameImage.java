package br.com.javanei.emulation.frontend.launchbox.metadata;

import java.io.Serializable;
import java.util.Objects;

public class LaunchBoxMetadataGameImage implements Serializable {
    private static final long serialVersionUID = 1L;

    private String databaseID;
    private String fileName;
    private String type;
    private String region;

    public String getDatabaseID() {
        return databaseID;
    }

    public void setDatabaseID(String databaseID) {
        this.databaseID = databaseID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LaunchBoxMetadataGameImage that = (LaunchBoxMetadataGameImage) o;
        return Objects.equals(databaseID, that.databaseID) &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(type, that.type) &&
                Objects.equals(region, that.region);
    }

    @Override
    public int hashCode() {
        return Objects.hash(databaseID, fileName, type, region);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("  <GameImage>\n");
        sb.append("    <DatabaseID>").append(this.databaseID).append("</DatabaseID>\n");
        sb.append("    <FileName>").append(this.fileName).append("</FileName>\n");
        sb.append("    <Type>").append(this.type).append("</Type>\n");
        if (this.region != null) {
            sb.append("    <Region>").append(this.region).append("</Region>\n");
        }
        sb.append("  </GameImage>\n");
        return sb.toString();
    }
}
