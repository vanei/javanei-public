package br.com.javanei.emulation.onlinedb.igdb;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "platform")
public class IGDBPlatform implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String slug;
    private String url;
    private Long createdAt;
    private Long updatedAt;
    private Integer generation;
    private String alternativeName;
    private String summary;
    private String website;

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long created_at) {
        this.createdAt = created_at;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updated_at) {
        this.updatedAt = updated_at;
    }

    public Integer getGeneration() {
        return generation;
    }

    public void setGeneration(Integer generation) {
        this.generation = generation;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternative_name) {
        this.alternativeName = alternative_name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<platform>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, slug, "slug");
        appendIfNotNull(sb, url, "url");
        appendIfNotNull(sb, createdAt, "createdAt");
        appendIfNotNull(sb, updatedAt, "updatedAt");
        appendIfNotNull(sb, generation, "generation");
        appendIfNotNull(sb, alternativeName, "alternativeName");
        appendIfNotNull(sb, summary, "summary");
        appendIfNotNull(sb, website, "website");
        sb.append("</platform>");
        return sb.toString();
    }
}
