package br.com.javanei.emulation.onlinedb.igdb;

public enum IGDBGamePegiRatingEnum {
    RP(1),
    EC(2),
    E(3),
    E10Plus(4),
    T(5),
    M(6),
    AO(7);

    private final Integer value;

    private IGDBGamePegiRatingEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
