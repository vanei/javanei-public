package br.com.javanei.emulation.onlinedb.giantbomb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "company")
public class GBombCompany implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String apiDetailUrl;
    private String abbreviation;
    private List<String> aliases = new LinkedList<>();
    //characters
    //concepts
    private String dateAdded;
    private String dateFounded;
    private String dateLastUpdated;
    private String deck;
    private String description;
    //developed_games
    //developer_releases
    //distributor_releases
    //image
    private String locationAddress;
    private String locationCity;
    private String locationCountry;
    private String locationState;
    //locations
    //objects
    //people
    private String phone;
    //published_games
    //publisher_releases
    private String siteDetailUrl;
    private String website;

    private static void appendIfNotNull(StringBuilder sb, Object value, String tag) {
        if (value != null)
            sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiDetailUrl() {
        return apiDetailUrl;
    }

    public void setApiDetailUrl(String apiDetailUrl) {
        this.apiDetailUrl = apiDetailUrl;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public void addAlias(String alias) {
        this.aliases.add(alias);
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateFounded() {
        return dateFounded;
    }

    public void setDateFounded(String dateFounded) {
        this.dateFounded = dateFounded;
    }

    public String getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(String dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationCountry() {
        return locationCountry;
    }

    public void setLocationCountry(String locationCountry) {
        this.locationCountry = locationCountry;
    }

    public String getLocationState() {
        return locationState;
    }

    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSiteDetailUrl() {
        return siteDetailUrl;
    }

    public void setSiteDetailUrl(String siteDetailUrl) {
        this.siteDetailUrl = siteDetailUrl;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<company>");
        appendIfNotNull(sb, id, "id");
        appendIfNotNull(sb, name, "name");
        appendIfNotNull(sb, deck, "deck");
        appendIfNotNull(sb, description, "description");
        appendIfNotNull(sb, apiDetailUrl, "apiDetailUrl");
        appendIfNotNull(sb, abbreviation, "abbreviation");
        appendIfNotNull(sb, dateAdded, "dateAdded");
        appendIfNotNull(sb, dateFounded, "dateFounded");
        appendIfNotNull(sb, dateLastUpdated, "dateLastUpdated");
        appendIfNotNull(sb, locationAddress, "locationAddress");
        appendIfNotNull(sb, locationCity, "locationCity");
        appendIfNotNull(sb, locationCountry, "locationCountry");
        appendIfNotNull(sb, locationState, "locationState");
        appendIfNotNull(sb, phone, "phone");
        appendIfNotNull(sb, siteDetailUrl, "siteDetailUrl");
        appendIfNotNull(sb, website, "website");
        if (!this.aliases.isEmpty()) {
            sb.append("<aliases>");
            for (int i = 0; i < this.aliases.size(); i++) {
                if (i > 0) sb.append("\n");
                sb.append(this.aliases.get(i));
            }
            sb.append("</aliases>");
        }
        sb.append("</company>");
        return sb.toString();
    }
}
