package br.com.javanei.emulation.onlinedb.igdb;

public enum IGDBGameStatusEnum {
    ALPHA(2),
    BETA(3),
    EarlyAccess(4),
    Offline(5),
    Cancelled(6);

    private final Integer value;

    private IGDBGameStatusEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
