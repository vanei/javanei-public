package br.com.javanei.emulation.game.common;

public enum ThreeStatesEnum {

    True("true"),
    False("false"),
    Unknown("unknown");

    private final String name;

    private ThreeStatesEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
