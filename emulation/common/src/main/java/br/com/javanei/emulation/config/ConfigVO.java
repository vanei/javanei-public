package br.com.javanei.emulation.config;

public class ConfigVO {

    /**
     * Diretório base onde ficam salvos os arquivos de ROMs
     */
    private String repositoryBaseDir;
    /**
     * Pasta raiz do hyperspin, quando configurado.
     */
    private String hyperspinBaseDir;
    /**
     * Pasta raiz das ROMs do hyperspin, quando configurado.
     */
    private String hyperspinROMBaseDir;

    private String igdbApiKey;

    private String giantbombApiKey;

    public String getRepositoryBaseDir() {
        return repositoryBaseDir;
    }

    public void setRepositoryBaseDir(String repositoryBaseDir) {
        if (repositoryBaseDir == null || repositoryBaseDir.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.repositoryBaseDir = repositoryBaseDir.trim();
    }

    public String getHyperspinBaseDir() {
        return hyperspinBaseDir;
    }

    public void setHyperspinBaseDir(String hyperspinBaseDir) {
        this.hyperspinBaseDir = hyperspinBaseDir;
    }

    public String getHyperspinROMBaseDir() {
        return hyperspinROMBaseDir;
    }

    public void setHyperspinROMBaseDir(String hyperspinROMBaseDir) {
        this.hyperspinROMBaseDir = hyperspinROMBaseDir;
    }

    public String getIgdbApiKey() {
        return igdbApiKey;
    }

    public void setIgdbApiKey(String igdbApiKey) {
        this.igdbApiKey = igdbApiKey;
    }

    public String getGiantbombApiKey() {
        return giantbombApiKey;
    }

    public void setGiantbombApiKey(String giantbombApiKey) {
        this.giantbombApiKey = giantbombApiKey;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<EmulDB>\n");
        if (this.repositoryBaseDir != null && !this.repositoryBaseDir.isEmpty()) {
            sb.append("\t<repositoryBaseDir>").append(this.repositoryBaseDir).append("</repositoryBaseDir>\n");
        }
        if (this.hyperspinBaseDir != null && !this.hyperspinBaseDir.isEmpty()) {
            sb.append("\t<hyperspinBaseDir>").append(this.hyperspinBaseDir).append("</hyperspinBaseDir>\n");
        }
        if (this.hyperspinROMBaseDir != null && !this.hyperspinROMBaseDir.isEmpty()) {
            sb.append("\t<hyperspinROMBaseDir>").append(this.hyperspinROMBaseDir).append("</hyperspinROMBaseDir>\n");
        }
        if (this.igdbApiKey != null && !this.igdbApiKey.isEmpty()) {
            sb.append("\t<igdbApiKey>").append(this.igdbApiKey).append("</igdbApiKey>\n");
        }
        if (this.giantbombApiKey != null && !this.giantbombApiKey.isEmpty()) {
            sb.append("\t<giantbombApiKey>").append(this.giantbombApiKey).append("</giantbombApiKey>\n");
        }
        sb.append("</EmulDB>\n");
        return sb.toString();
    }
}
