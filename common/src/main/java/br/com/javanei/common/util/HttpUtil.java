package br.com.javanei.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public final class HttpUtil {
    public static final HttpRequestResult get(String url, Map<String, String> requestProps, String content) throws IOException {
        return send(url, "GET", requestProps, content);
    }

    public static final HttpRequestResult put(String url, Map<String, String> requestProps, String content) throws IOException {
        return send(url, "PUT", requestProps, content);
    }

    public static final HttpRequestResult post(String url, Map<String, String> requestProps, String content) throws IOException {
        return send(url, "POST", requestProps, content);
    }

    private static HttpRequestResult send(String _url, String _method, Map<String, String> _requestProps,
                                          String _content) throws IOException {
        URL url = new URL(_url);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(_method);
        connection.setDoOutput(true);
        if (_requestProps != null) {
            for (String key : _requestProps.keySet()) {
                connection.setRequestProperty(key, _requestProps.get(key));
            }
        }
        if (_content != null) {
            OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
            osw.write(_content);
            osw.flush();
            osw.close();
        }
        HttpRequestResult result = new HttpRequestResult(connection.getResponseCode());
        byte[] b = new byte[4096];
        InputStream is = connection.getInputStream();
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int size = is.read(b);
            while (size > 0) {
                out.write(b, 0, size);
                size = is.read(b);
            }
            result.setContent(out.toByteArray());
        }
        return result;
    }
}
