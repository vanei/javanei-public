package br.com.javanei.common.util;

import java.io.Serializable;

public class HttpRequestResult implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;
    private byte[] content;

    public HttpRequestResult() {
    }

    public HttpRequestResult(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
