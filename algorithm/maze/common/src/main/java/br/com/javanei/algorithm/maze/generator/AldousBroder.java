package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.Grid;
import java.util.Random;

public class AldousBroder {
    public void generate(Grid grid) {
        Random rand = new Random(System.currentTimeMillis());

        Cell cell = grid.getRandomCell();
        int unvisited = grid.getSize() - 1;

        while (unvisited > 0) {
            int i = rand.nextInt(cell.getNeighbors().size());
            Cell neighbor = cell.getNeighbors().get(i);

            if (neighbor.getLinks().isEmpty()) {
                cell.link(neighbor, true);
                unvisited--;
            }

            cell = neighbor;
        }
    }
}
