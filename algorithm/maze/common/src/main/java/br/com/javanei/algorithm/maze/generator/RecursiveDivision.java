package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.CellRect;
import br.com.javanei.algorithm.maze.GridRect;
import br.com.javanei.algorithm.maze.Position;
import java.util.Random;

public class RecursiveDivision {
    private Random rand = new Random(System.currentTimeMillis());
    private GridRect grid;

    public void generate(GridRect grid) {
        this.grid = grid;

        for (Cell cell : grid.getAllCellsEnabled()) {
            for (Cell neighbor : cell.getNeighbors()) {
                cell.link(neighbor, false);
            }
        }

        divide(0, 0, grid.getRows(), grid.getColumns(), null);
    }

    private void divide(int row, int column, int height, int width, DividerCriteria dividerCriteria) {
        if (dividerCriteria == null) dividerCriteria = new RoomDivider();
        if (dividerCriteria.divider(height, width)) return;

        if (height > width)
            divideHorizontally(row, column, height, width);
        else
            divideVertically(row, column, height, width);
    }

    private void divideHorizontally(int row, int column, int height, int width) {
        int divideSouthOf = rand.nextInt(height - 1);
        int passageAt = rand.nextInt(width);

        for (int x = 0; x < width; x++) {
            if (passageAt != x) {
                CellRect cell = grid.getCell(new Position(row + divideSouthOf, column + x));
                cell.unlink(cell.getSouth(), true);
            }
        }

        divide(row, column, divideSouthOf + 1, width, null);
        divide(row + divideSouthOf + 1, column, height - divideSouthOf - 1, width, null);
    }

    private void divideVertically(int row, int column, int height, int width) {
        int divideEastOf = rand.nextInt(width - 1);
        int passageAt = rand.nextInt(height);

        for (int y = 0; y < height; y++) {
            if (passageAt != y) {
                CellRect cell = grid.getCell(new Position(row + y, column + divideEastOf));
                cell.unlink(cell.getEast(), true);
            }
        }

        divide(row, column, height, divideEastOf + 1, null);
        divide(row, column + divideEastOf + 1, height, width - divideEastOf - 1, null);
    }

    private abstract class DividerCriteria {
        public abstract boolean divider(int height, int width);
    }

    private class DefaultDivider extends DividerCriteria {
        @Override
        public boolean divider(int height, int width) {
            return (height <= 1 || width <= 1);
        }
    }

    private class RoomDivider extends DividerCriteria {
        @Override
        public boolean divider(int height, int width) {
            return (height <= 1 || width <= 1 || (height < 5 && rand.nextInt(4) == 0));
        }
    }
}
