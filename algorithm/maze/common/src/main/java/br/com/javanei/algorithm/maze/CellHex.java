package br.com.javanei.algorithm.maze;

import java.util.LinkedList;
import java.util.List;

public class CellHex extends Cell {
    private final List<Cell> neighbors = new LinkedList<>();
    private Cell north;
    private Cell south;
    private Cell northeast;
    private Cell northwest;
    private Cell southeast;
    private Cell southwest;

    public CellHex(Position position) {
        super(position);
    }

    public CellHex(Position position, boolean isEnabled) {
        super(position, isEnabled);
    }

    @Override
    public List<Cell> getNeighbors() {
        return this.neighbors;
    }


    public Cell getNorth() {
        return north != null && north.isEnabled ? north : null;
    }

    public void setNorth(Cell north) {
        if (this.north != null) {
            this.neighbors.remove(this.north);
        }
        this.north = north;
        if (north != null && north.isEnabled) {
            this.neighbors.add(north);
        }
    }

    public Cell getSouth() {
        return south != null && south.isEnabled ? south : null;
    }

    public void setSouth(Cell south) {
        if (this.south != null) {
            this.neighbors.remove(this.south);
        }
        this.south = south;
        if (south != null && south.isEnabled) {
            this.neighbors.add(south);
        }
    }

    public Cell getNortheast() {
        return northeast != null && northeast.isEnabled ? northeast : null;
    }

    public void setNortheast(Cell northeast) {
        if (this.northeast != null) {
            this.neighbors.remove(this.northeast);
        }
        this.northeast = northeast;
        if (northeast != null && northeast.isEnabled) {
            this.neighbors.add(northeast);
        }
    }

    public Cell getNorthwest() {
        return northwest != null && northwest.isEnabled ? northwest : null;
    }

    public void setNorthwest(Cell northwest) {
        if (this.northwest != null) {
            this.neighbors.remove(this.northwest);
        }
        this.northwest = northwest;
        if (northwest != null && northwest.isEnabled) {
            this.neighbors.add(northwest);
        }
    }

    public Cell getSoutheast() {
        return southeast != null && southeast.isEnabled ? southeast : null;
    }

    public void setSoutheast(Cell southeast) {
        if (this.southeast != null) {
            this.neighbors.remove(this.southeast);
        }
        this.southeast = southeast;
        if (southeast != null && southeast.isEnabled) {
            this.neighbors.add(southeast);
        }
    }

    public Cell getSouthwest() {
        return southwest != null && southwest.isEnabled ? southwest : null;
    }

    public void setSouthwest(Cell southwest) {
        if (this.southwest != null) {
            this.neighbors.remove(this.southwest);
        }
        this.southwest = southwest;
        if (southwest != null && southwest.isEnabled) {
            this.neighbors.add(southwest);
        }
    }
}
