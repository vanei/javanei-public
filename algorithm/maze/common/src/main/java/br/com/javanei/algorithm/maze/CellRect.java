package br.com.javanei.algorithm.maze;

import java.util.LinkedList;
import java.util.List;

public class CellRect extends Cell {
    private final List<Cell> neighbors = new LinkedList<>();
    private CellRect north;
    private CellRect south;
    private CellRect east;
    private CellRect west;

    public CellRect(Position position) {
        super(position);
    }

    public CellRect(Position position, boolean isEnabled) {
        super(position, isEnabled);
    }

    @Override
    public List<Cell> getNeighbors() {
        return this.neighbors;
    }

    public CellRect getNorth() {
        return north != null && north.isEnabled ? north : null;
    }

    public void setNorth(CellRect north) {
        if (this.north != null) {
            this.neighbors.remove(this.north);
        }
        this.north = north;
        if (north != null && north.isEnabled) {
            this.neighbors.add(north);
        }
    }

    public CellRect getSouth() {
        return south != null && south.isEnabled ? south : null;
    }

    public void setSouth(CellRect south) {
        if (this.south != null) {
            this.neighbors.remove(this.south);
        }
        this.south = south;
        if (south != null && south.isEnabled) {
            this.neighbors.add(south);
        }
    }

    public CellRect getEast() {
        return east != null && east.isEnabled ? east : null;
    }

    public void setEast(CellRect east) {
        if (this.east != null) {
            this.neighbors.remove(this.east);
        }
        this.east = east;
        if (east != null && east.isEnabled) {
            this.neighbors.add(east);
        }
    }

    public CellRect getWest() {
        return west != null && west.isEnabled ? west : null;
    }

    public void setWest(CellRect west) {
        if (this.west != null) {
            this.neighbors.remove(this.west);
        }
        this.west = west;
        if (west != null && west.isEnabled) {
            this.neighbors.add(west);
        }
    }

    public int getRow() {
        return this.position.row;
    }

    public int getColumn() {
        return this.position.col;
    }
}
