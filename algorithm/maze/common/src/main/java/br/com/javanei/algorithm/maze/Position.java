package br.com.javanei.algorithm.maze;

import java.util.Objects;

public class Position {
    protected final int row;
    protected final int col;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position point = (Position) o;
        return row == point.row &&
                col == point.col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }

    @Override
    public String toString() {
        return "{" + row + col + '}';
    }
}
