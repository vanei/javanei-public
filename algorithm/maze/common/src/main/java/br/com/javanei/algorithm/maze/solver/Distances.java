package br.com.javanei.algorithm.maze.solver;

import br.com.javanei.algorithm.maze.Cell;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Calcula distancias usando o algoritmo de "dijkstra".
 */
public class Distances {
    private Cell root;
    private Map<Cell, Integer> cells;
    private Cell mostDistantCell;
    private Integer maxDistance;
    private boolean isCalculated = false;

    public Distances(Cell root) {
        this.root = root;
        this.cells = new LinkedHashMap<>();
        this.cells.put(root, 0);
    }

    public Integer getDistance(Cell cell) {
        if (!this.isCalculated) calculateDistance();
        return this.cells.get(cell);
    }

    public String getContentsOf(Cell cell) {
        if (!this.isCalculated) calculateDistance();
        Integer r = this.cells.get(cell);
        if (r < 10) {
            return " " + r + " ";
        } else if (r < 100) {
            return " " + r;
        } else {
            return String.valueOf(r);
        }
    }

    public Cell getMostDistantCell() {
        if (!this.isCalculated) calculateDistance();
        return mostDistantCell;
    }

    public Integer getMaxDistance() {
        if (!this.isCalculated) calculateDistance();
        return maxDistance;
    }

    public List<Cell> pathTo(Cell goal) {
        if (!this.isCalculated) calculateDistance();
        Cell current = goal;

        Map<Cell, Integer> breadcrumbs = new LinkedHashMap<>();
        breadcrumbs.put(current, this.cells.get(current));

        do {
            for (Cell neighbor : current.getLinks()) {
                if (this.cells.get(neighbor) < this.cells.get(current)) {
                    breadcrumbs.put(neighbor, this.cells.get(neighbor));
                    current = neighbor;
                    break;
                }
            }
        } while (!current.equals(this.root));

        List<Cell> r = new LinkedList<>();
        r.addAll(breadcrumbs.keySet());
        return r;
    }

    /**
     * Calcula todas as distancias a partir da celula root.
     */
    private void calculateDistance() {
        this.isCalculated = true;
        List<Cell> frontier = new LinkedList<>();
        frontier.add(this.root);

        while (!frontier.isEmpty()) {
            List<Cell> newFrontier = new LinkedList<>();

            for (Cell cell : frontier) {
                for (Cell linked : cell.getLinks()) {
                    if (this.cells.get(linked) != null) {
                        continue;
                    }
                    this.cells.put(linked, this.cells.get(cell) + 1);
                    newFrontier.add(linked);
                }
            }
            frontier = newFrontier;
        }
        this.calculateMax();
    }

    /**
     * Encontra a célula mais distante, bem como o valor da distância.
     */
    private void calculateMax() {
        int max_distance = 0;
        Cell max_cell = this.root;

        for (Cell cell : cells.keySet()) {
            int distance = this.cells.get(cell);
            if (distance > max_distance) {
                max_cell = cell;
                max_distance = distance;
            }
        }
        this.mostDistantCell = max_cell;
        this.maxDistance = max_distance;
    }
}
