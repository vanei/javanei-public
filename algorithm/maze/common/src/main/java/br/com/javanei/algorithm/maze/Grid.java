package br.com.javanei.algorithm.maze;

import java.util.List;

public abstract class Grid {
    protected int rows;
    protected int columns;

    public Grid(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    //
    public abstract int getSize();

    public abstract Cell getCell(Position position);

    public abstract List<Cell> getAllCells();

    public abstract List<Cell> getAllCellsEnabled();

    public abstract List<List<Cell>> getAllRows();

    public abstract List<List<Cell>> getAllRowsEnabled();

    public abstract Cell getRandomCell();
}
