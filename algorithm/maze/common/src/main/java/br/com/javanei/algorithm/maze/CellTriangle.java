package br.com.javanei.algorithm.maze;

import java.util.LinkedList;
import java.util.List;

public class CellTriangle extends Cell {
    private final List<Cell> neighbors = new LinkedList<>();
    private CellTriangle north;
    private CellTriangle south;
    private CellTriangle east;
    private CellTriangle west;

    public CellTriangle(Position position) {
        super(position);
    }

    public CellTriangle(Position position, boolean isEnabled) {
        super(position, isEnabled);
    }

    @Override
    public List<Cell> getNeighbors() {
        return this.neighbors;
    }

    public boolean isUpright() {
        return (this.position.getRow() + this.position.getCol()) % 2 == 0;
    }

    public CellTriangle getNorth() {
        return north != null && north.isEnabled ? north : null;
    }

    public void setNorth(CellTriangle north) {
        if (this.north != null) {
            this.neighbors.remove(this.north);
        }
        if (!isUpright()) {
            this.north = north;
            if (north != null && north.isEnabled) {
                this.neighbors.add(north);
            }
        }
    }

    public CellTriangle getSouth() {
        return south != null && south.isEnabled ? south : null;
    }

    public void setSouth(CellTriangle south) {
        if (this.south != null) {
            this.neighbors.remove(this.south);
        }
        if (isUpright()) {
            this.south = south;
            if (south != null && south.isEnabled) {
                this.neighbors.add(south);
            }
        }
    }

    public CellTriangle getEast() {
        return east != null && east.isEnabled ? east : null;
    }

    public void setEast(CellTriangle east) {
        if (this.east != null) {
            this.neighbors.remove(this.east);
        }
        this.east = east;
        if (east != null && east.isEnabled) {
            this.neighbors.add(east);
        }
    }

    public CellTriangle getWest() {
        return west != null && west.isEnabled ? west : null;
    }

    public void setWest(CellTriangle west) {
        if (this.west != null) {
            this.neighbors.remove(this.west);
        }
        this.west = west;
        if (west != null && west.isEnabled) {
            this.neighbors.add(west);
        }
    }

    public int getRow() {
        return this.position.row;
    }

    public int getColumn() {
        return this.position.col;
    }
}
