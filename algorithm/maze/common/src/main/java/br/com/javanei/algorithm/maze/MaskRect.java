package br.com.javanei.algorithm.maze;

import java.util.Random;

public class MaskRect {
    private final int rows;
    private final int columns;
    private final boolean[][] bits;

    public MaskRect(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.bits = new boolean[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                bits[row][col] = true;
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public boolean isEnabled(int row, int col) {
        if (row < this.rows && col < this.columns) {
            return this.bits[row][col];
        }
        return false;
    }

    public void setEnabled(int row, int col, boolean is_on) {
        if (row < this.rows && col < this.columns) {
            this.bits[row][col] = is_on;
        }
    }

    public int count() {
        int result = 0;
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                if (bits[row][col]) {
                    result++;
                }
            }
        }
        return result;
    }

    public Position getRandomPosition() {
        Random rand = new Random(System.currentTimeMillis());
        while (true) {
            int row = rand.nextInt(rows);
            int col = rand.nextInt(columns);
            if (bits[row][col]) {
                return new Position(row, col);
            }
        }
    }
}
