package br.com.javanei.algorithm.maze;

import java.util.Objects;

public class PositionHex extends Position {
    protected int northDiagonal;
    protected int southDiagonal;

    public PositionHex(int row, int col, int northDiagonal, int southDiagonal) {
        super(row, col);
        this.northDiagonal = northDiagonal;
        this.southDiagonal = southDiagonal;
    }

    public int getNorthDiagonal() {
        return northDiagonal;
    }

    public int getSouthDiagonal() {
        return southDiagonal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PositionHex that = (PositionHex) o;
        return row == that.row &&
                col == that.col &&
                northDiagonal == that.northDiagonal &&
                southDiagonal == that.southDiagonal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), northDiagonal, southDiagonal);
    }

    @Override
    public String toString() {
        return "PositionHex {" +
                "row=" + row +
                ", col=" + col +
                ", northDiagonal=" + northDiagonal +
                ", southDiagonal=" + southDiagonal +
                '}';
    }
}
