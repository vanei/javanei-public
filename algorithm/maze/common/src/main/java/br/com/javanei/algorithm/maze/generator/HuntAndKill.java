package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.Grid;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class HuntAndKill {
    public void generate(Grid grid) {
        Random rand = new Random(System.currentTimeMillis());

        Cell current = grid.getRandomCell();

        while (current != null) {
            List<Cell> unvisitedNeighbors = new LinkedList<>();
            for (Cell cell : current.getNeighbors()) {
                if (cell.getLinks().isEmpty()) {
                    unvisitedNeighbors.add(cell);
                }
            }

            if (!unvisitedNeighbors.isEmpty()) {
                Cell neighbor = unvisitedNeighbors.get(rand.nextInt(unvisitedNeighbors.size()));
                current.link(neighbor, true);
                current = neighbor;
            } else {
                current = null;

                for (Cell cell : grid.getAllCellsEnabled()) {
                    List<Cell> visitedNeighbors = new LinkedList<>();
                    for (Cell cell1 : cell.getNeighbors()) {
                        if (!cell1.getLinks().isEmpty()) {
                            visitedNeighbors.add(cell1);
                        }
                    }
                    if (cell.getLinks().isEmpty() && !visitedNeighbors.isEmpty()) {
                        current = cell;

                        Cell neighbor = visitedNeighbors.get(rand.nextInt(visitedNeighbors.size()));
                        current.link(neighbor, true);

                        break;
                    }
                }
            }
        }
    }
}
