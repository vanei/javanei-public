package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.Grid;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

public class TruePrim {
    private static final Random rand = new Random(System.currentTimeMillis());

    private Stack<Cell> active;
    private Map<Cell, Integer> costs;

    public void generate(Grid grid, Cell startAt) {
        active = new Stack<>();
        active.push(startAt);
        costs = new LinkedHashMap<>();

        for (Cell cell : grid.getAllCellsEnabled()) {
            costs.put(cell, rand.nextInt(100));
        }

        while (!active.isEmpty()) {
            Cell cell = min(active);
            List<Cell> availableNeighbors = new LinkedList<>();
            for (Cell c : cell.getNeighbors()) {
                if (c.getLinks().isEmpty()) {
                    availableNeighbors.add(c);
                }
            }

            if (!availableNeighbors.isEmpty()) {
                Cell neighbor = min(availableNeighbors);
                cell.link(neighbor, true);
                active.push(neighbor);
            } else {
                //active.remove(cell);
                Stack<Cell> list = new Stack<>();
                list.addAll(active);
                list.remove(cell);
                active = new Stack<>();
                active.addAll(list);
            }
        }
    }

    public void generate(Grid grid) {
        this.generate(grid, grid.getRandomCell());
    }

    private Cell min(List<Cell> cells) {
        Cell lowest = null;
        int lowestValue = Integer.MAX_VALUE;
        for (Cell cell : cells) {
            int cost = costs.get(cell);
            if (cost < lowestValue) {
                lowestValue = cost;
                lowest = cell;
            }
        }
        return lowest;
    }
}
