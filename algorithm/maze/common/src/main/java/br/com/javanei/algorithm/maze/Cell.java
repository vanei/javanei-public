package br.com.javanei.algorithm.maze;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public abstract class Cell {
    public final Position position;
    public final boolean isEnabled;

    protected final Map<Cell, Boolean> links;

    public Cell(Position position) {
        this(position, true);
    }

    public Cell(Position position, boolean isEnabled) {
        this.position = position;
        this.isEnabled = isEnabled;
        this.links = new LinkedHashMap<Cell, Boolean>();
    }

    /**
     * Retorna as células com as quais tem ligação.
     *
     * @return
     */
    public List<Cell> getLinks() {
        List<Cell> r = new LinkedList<>();
        r.addAll(this.links.keySet());
        return r;
    }

    /**
     * Liga a célula corrente com outra célula. Ou seja, gera um caminho entre elas.
     *
     * @param cell
     * @param bidir
     */
    public void link(Cell cell, boolean bidir) {
        if (!this.links.containsKey(cell)) {
            this.links.put(cell, Boolean.TRUE);
        }
        if (bidir) {
            cell.link(this, false);
        }
    }

    /**
     * Remove a ligação da célula corrente com outra célula. Ou seja, fecha o caminho entre elas.
     *
     * @param cell
     * @param bidir
     */
    public void unlink(Cell cell, boolean bidir) {
        if (this.links.containsKey(cell)) {
            this.links.remove(cell);
        }
        if (bidir) {
            cell.unlink(this, false);
        }
    }

    /**
     * Verifica se a célula corrente está ligada a uma determinada outra célula.
     *
     * @param cell
     * @return
     */
    public boolean isLinked(Cell cell) {
        return this.links.containsKey(cell);
    }

    //

    /**
     * Retorna as células vizinhas
     *
     * @return
     */
    public abstract List<Cell> getNeighbors();

    public Position getPosition() {
        return this.position;
    }
//

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return position.row == cell.position.row &&
                position.col == cell.position.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position.row, position.col);
    }

    @Override
    public String toString() {
        return "{" + position.row + "," + position.col + "}=" + isEnabled;
    }
}
