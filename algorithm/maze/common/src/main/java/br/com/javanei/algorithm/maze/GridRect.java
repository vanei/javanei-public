package br.com.javanei.algorithm.maze;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GridRect extends Grid {
    private final MaskRect mask;
    protected CellRect[][] grid;

    private GridRect(int rows, int columns, MaskRect mask, boolean initialize) {
        super(rows, columns);
        this.mask = mask;
        if (initialize) {
            this.prepareGrid();
            this.configureCells();
        }
    }

    protected GridRect(MaskRect mask, boolean initialize) {
        this(mask.getRows(), mask.getColumns(), mask, initialize);
    }

    public GridRect(MaskRect mask) {
        this(mask.getRows(), mask.getColumns(), mask, true);
    }

    protected GridRect(int rows, int columns, boolean initialize) {
        this(rows, columns, null, initialize);
    }

    public GridRect(int rows, int columns) {
        this(rows, columns, null, true);
    }

    @Override
    public CellRect getCell(Position position) {
        return this.getCell(position.row, position.col);
    }

    public CellRect getCell(int row, int col) {
        if (row >= 0 && col >= 0 && row < rows && col < columns) {
            return this.grid[row][col];
        }
        return null;
    }

    protected void prepareGrid() {
        this.grid = new CellRect[rows][columns];
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                grid[row][col] = new CellRect(new Position(row, col), (this.mask == null || this.mask.isEnabled(row, col)));
            }
        }
    }

    protected void configureCells() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                CellRect cell = this.grid[row][col];
                if (cell != null) {
                    cell.setNorth(this.getCell(row - 1, col));
                    cell.setSouth(this.getCell(row + 1, col));
                    cell.setWest(this.getCell(row, col - 1));
                    cell.setEast(this.getCell(row, col + 1));
                }
            }
        }
    }

    @Override
    public int getSize() {
        return this.rows * this.columns;
    }

    @Override
    public List<Cell> getAllCells() {
        List<Cell> r = new LinkedList<>();
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                r.add(this.grid[row][col]);
            }
        }
        return r;
    }

    @Override
    public List<Cell> getAllCellsEnabled() {
        List<Cell> r = new LinkedList<>();
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                if (this.grid[row][col].isEnabled)
                    r.add(this.grid[row][col]);
            }
        }
        return r;
    }

    @Override
    public List<List<Cell>> getAllRows() {
        List<List<Cell>> r = new LinkedList<>();
        for (int row = 0; row < rows; row++) {
            List<Cell> l = new LinkedList<>();
            for (int col = 0; col < columns; col++) {
                l.add(this.grid[row][col]);
            }
            r.add(l);
        }
        return r;
    }

    @Override
    public List<List<Cell>> getAllRowsEnabled() {
        List<List<Cell>> r = new LinkedList<>();
        for (int row = 0; row < rows; row++) {
            List<Cell> l = new LinkedList<>();
            for (int col = 0; col < columns; col++) {
                if (grid[row][col].isEnabled)
                    l.add(this.grid[row][col]);
            }
            r.add(l);
        }
        return r;
    }

    @Override
    public Cell getRandomCell() {
        Random rand = new Random(System.currentTimeMillis());
        Position pos = (this.mask != null)
                ? this.mask.getRandomPosition() :
                new Position(rand.nextInt(this.rows), rand.nextInt(this.columns));
        return this.grid[pos.row][pos.col];
    }
}
