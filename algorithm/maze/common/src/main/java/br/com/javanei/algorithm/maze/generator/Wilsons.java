package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.Grid;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

//TODO: Não está funcionando
public class Wilsons {
    public void generate(Grid grid) {
        Random rand = new Random(System.currentTimeMillis());

        List<Cell> unvisited = new LinkedList<>();
        unvisited.addAll(grid.getAllCellsEnabled());

        Cell first = unvisited.get(rand.nextInt(unvisited.size()));
        unvisited.remove(first);

        while (!unvisited.isEmpty()) {
            Cell cell = unvisited.get(rand.nextInt(unvisited.size()));
            List<Cell> path = new LinkedList<>();
            path.add(cell);

            List<Cell> sorteadas = new LinkedList<>();
            while (unvisited.contains(cell)) {
                // Aparentemente o método "sample" do ruby não repete o item da lista!!
                do {
                    cell = cell.getNeighbors().get(rand.nextInt(cell.getNeighbors().size()));
                } while (sorteadas.contains(cell));
                sorteadas.add(cell);
                int position = path.indexOf(cell);
                if (position >= 0) {
                    path = path.subList(0, position + 1);
                } else {
                    path.add(cell);
                }
            }
            for (int index = 0; index <= path.size() - 2; index++) {
                path.get(index).link(path.get(index + 1), true);
                unvisited.remove(path.get(index));
            }
        }
    }
}
