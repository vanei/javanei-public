package br.com.javanei.algorithm.maze;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GridHex extends Grid {
    private final MaskRect mask;
    protected CellHex[][] grid;

    private GridHex(int rows, int columns, MaskRect mask, boolean initialize) {
        super(rows, columns);
        this.mask = mask;
        if (initialize) {
            this.prepareGrid();
            this.configureCells();
        }
    }

    public GridHex(MaskRect mask) {
        this(mask.getRows(), mask.getColumns(), mask, true);
    }

    public GridHex(int rows, int columns) {
        this(rows, columns, null, true);
    }

    protected void prepareGrid() {
        this.grid = new CellHex[this.rows][columns];
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                if (col % 2 == 0) {
                    grid[row][col] = new CellHex(new PositionHex(row, col, row - 1, row),
                            (this.mask == null || this.mask.isEnabled(row, col)));
                } else {
                    grid[row][col] = new CellHex(new PositionHex(row, col, row, row + 1),
                            (this.mask == null || this.mask.isEnabled(row, col)));
                }
            }
        }
    }

    protected void configureCells() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                CellHex cell = this.grid[row][col];
                PositionHex pos = (PositionHex) cell.getPosition();
                cell.setNorthwest(this.getCell(pos.getNorthDiagonal(), col - 1));
                cell.setNorth(this.getCell(row - 1, col));
                cell.setNortheast(this.getCell(pos.getNorthDiagonal(), col + 1));
                cell.setSouthwest(this.getCell(pos.getSouthDiagonal(), col - 1));
                cell.setSouth(this.getCell(row + 1, col));
                cell.setSoutheast(this.getCell(pos.getSouthDiagonal(), col + 1));
            }
        }
    }

    @Override
    public int getSize() {
        return this.rows * this.columns;
    }

    @Override
    public CellHex getCell(Position position) {
        return this.getCell(position.row, position.col);
    }

    public CellHex getCell(int row, int col) {
        if (row >= 0 && col >= 0 && row < rows && col < columns) {
            return this.grid[row][col];
        }
        return null;
    }

    @Override
    public List<Cell> getAllCells() {
        List<Cell> r = new LinkedList<>();
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                r.add(this.grid[row][col]);
            }
        }
        return r;
    }

    @Override
    public List<Cell> getAllCellsEnabled() {
        List<Cell> r = new LinkedList<>();
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                if (this.grid[row][col].isEnabled)
                    r.add(this.grid[row][col]);
            }
        }
        return r;
    }

    @Override
    public List<List<Cell>> getAllRows() {
        List<List<Cell>> r = new LinkedList<>();
        for (int row = 0; row < rows; row++) {
            List<Cell> l = new LinkedList<>();
            for (int col = 0; col < columns; col++) {
                l.add(this.grid[row][col]);
            }
            r.add(l);
        }
        return r;
    }

    @Override
    public List<List<Cell>> getAllRowsEnabled() {
        List<List<Cell>> r = new LinkedList<>();
        for (int row = 0; row < rows; row++) {
            List<Cell> l = new LinkedList<>();
            for (int col = 0; col < columns; col++) {
                if (grid[row][col].isEnabled)
                    l.add(this.grid[row][col]);
            }
            r.add(l);
        }
        return r;
    }

    @Override
    public Cell getRandomCell() {
        Random rand = new Random(System.currentTimeMillis());
        Position pos = (this.mask != null)
                ? this.mask.getRandomPosition() :
                new Position(rand.nextInt(this.rows), rand.nextInt(this.columns));
        return this.grid[pos.row][pos.col];
    }
}
