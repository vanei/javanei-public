package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.CellRect;
import br.com.javanei.algorithm.maze.GridRect;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class BinaryTree {
    public void generate(GridRect grid) {
        Random rand = new Random(System.currentTimeMillis());

        for (Cell c : grid.getAllCellsEnabled()) {
            CellRect cell = (CellRect) c;
            List<Cell> neighbors = new LinkedList<>();
            if (cell.getNorth() != null) neighbors.add(cell.getNorth());
            if (cell.getEast() != null) neighbors.add(cell.getEast());

            if (neighbors.size() > 0) {
                int index = rand.nextInt(neighbors.size());
                Cell neighbor = neighbors.get(index);

                cell.link(neighbor, true);
            }
        }
    }
}
