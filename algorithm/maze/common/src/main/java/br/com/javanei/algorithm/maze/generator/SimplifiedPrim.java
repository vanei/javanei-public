package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.Grid;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class SimplifiedPrim {
    private static final Random rand = new Random(System.currentTimeMillis());

    public void generate(Grid grid, Cell startAt) {
        Stack<Cell> active = new Stack<>();
        active.push(startAt);

        while (!active.isEmpty()) {
            Cell cell = active.get(rand.nextInt(active.size()));
            List<Cell> availableNeighbors = new LinkedList<>();
            for (Cell c : cell.getNeighbors()) {
                if (c.getLinks().isEmpty()) {
                    availableNeighbors.add(c);
                }
            }

            if (!availableNeighbors.isEmpty()) {
                Cell neighbor = availableNeighbors.get(rand.nextInt(availableNeighbors.size()));
                cell.link(neighbor, true);
                active.push(neighbor);
            } else {
                active.remove(cell);
            }
        }
    }

    public void generate(Grid grid) {
        this.generate(grid, grid.getRandomCell());
    }
}
