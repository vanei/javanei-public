package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.CellRect;
import br.com.javanei.algorithm.maze.GridRect;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Sidewinder {
    public static void generate(GridRect grid) {
        Random rand = new Random(System.currentTimeMillis());

        for (List<Cell> row : grid.getAllRowsEnabled()) {
            List<CellRect> run = new LinkedList<>();
            for (Cell c : row) {
                CellRect cell = (CellRect) c;
                run.add(cell);

                boolean atEasternBoundary = (cell.getEast() == null);
                boolean atNorthernBoundary = (cell.getNorth() == null);

                boolean shouldCloseOut = atEasternBoundary ||
                        (!atNorthernBoundary && rand.nextInt(2) == 0);
                if (shouldCloseOut) {
                    CellRect member = run.get(rand.nextInt(run.size()));
                    if (member.getNorth() != null) member.link(member.getNorth(), true);
                    run.clear();
                } else {
                    cell.link(cell.getEast(), true);
                }
            }
        }
    }
}
