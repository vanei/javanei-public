package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.CellRect;
import br.com.javanei.algorithm.maze.GridRect;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Stack;

public class Kruskals {
    public void generate(GridRect grid) {
        Random rand = new Random(System.currentTimeMillis());

        State state = new State(grid);
        Collections.shuffle(state.getNeighbors());
        Stack<CellPair> neighbors = new Stack<>();
        neighbors.addAll(state.getNeighbors());

        while (!neighbors.isEmpty()) {
            CellPair pair = neighbors.pop();
            CellRect left = pair.getLeft();
            CellRect right = pair.getRight();
            if (state.canMerge(left, right)) {
                state.merge(left, right);
            }
        }
    }

    private class CellPair {
        private CellRect left;
        private CellRect right;

        public CellPair(CellRect left, CellRect right) {
            this.left = left;
            this.right = right;
        }

        public CellRect getLeft() {
            return left;
        }

        public CellRect getRight() {
            return right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CellPair cellPair = (CellPair) o;
            return Objects.equals(left, cellPair.left) &&
                    Objects.equals(right, cellPair.right);
        }

        @Override
        public int hashCode() {
            return Objects.hash(left, right);
        }

        @Override
        public String toString() {
            return "CellPair {" +
                    "left=" + left +
                    ", right=" + right +
                    '}';
        }
    }

    private class State {
        private List<CellPair> neighbors;
        private Map<Cell, Integer> setForCell;
        private Map<Integer, List<CellRect>> cellsInSet;
        private GridRect grid;

        public State(GridRect grid) {
            this.grid = grid;
            this.neighbors = new LinkedList<>();
            this.setForCell = new LinkedHashMap<>();
            this.cellsInSet = new LinkedHashMap<>();

            for (Cell c : grid.getAllCellsEnabled()) {
                CellRect cell = (CellRect) c;
                int set = setForCell.size();
                setForCell.put(cell, set);
                List<CellRect> l = new LinkedList<>();
                l.add(cell);
                cellsInSet.put(set, l);

                if (cell.getSouth() != null) {
                    neighbors.add(new CellPair(cell, (CellRect) cell.getSouth()));
                }
                if (cell.getEast() != null) {
                    neighbors.add(new CellPair(cell, (CellRect) cell.getEast()));
                }
            }
        }

        private boolean canMerge(CellRect left, CellRect right) {
            return setForCell.get(left).intValue() != setForCell.get(right).intValue();
        }

        private void merge(CellRect left, CellRect right) {
            left.link(right, true);

            int winner = setForCell.get(left);
            int loser = setForCell.get(right);
            List<CellRect> losers = new LinkedList<>();
            if (this.cellsInSet.get(loser) != null && !this.cellsInSet.get(loser).isEmpty())
                losers.addAll(this.cellsInSet.get(loser));
            else
                losers.add(right);

            for (CellRect cell : losers.toArray(new CellRect[]{})) {
                cellsInSet.get(winner).add(cell);
                setForCell.put(cell, winner);
            }

            cellsInSet.remove(loser);
        }

        public List<CellPair> getNeighbors() {
            return neighbors;
        }
    }
}
