package br.com.javanei.algorithm.maze.generator;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.Grid;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class RecursiveBacktracker {
    public void generate(Grid grid) {
        this.generate(grid, grid.getRandomCell());
    }

    public void generate(Grid grid, Cell start_at) {
        Random rand = new Random(System.currentTimeMillis());

        Stack<Cell> stack = new Stack<>();
        stack.push(start_at);

        while (!stack.isEmpty()) {
            Cell current = stack.lastElement();
            List<Cell> neighbors = new LinkedList<>();
            for (Cell cell : current.getNeighbors()) {
                if (cell.getLinks().isEmpty()) {
                    neighbors.add(cell);
                }
            }

            if (neighbors.isEmpty()) {
                stack.pop();
            } else {
                Cell neighbor = neighbors.get(rand.nextInt(neighbors.size()));
                current.link(neighbor, true);
                stack.push(neighbor);
            }
        }
    }
}
