package br.com.javanei.algorithm.maze.test;

import br.com.javanei.algorithm.maze.MaskRect;

public abstract class AbstractTest {
    protected MaskRect getMaskRect(int rows, int columns) {
        MaskRect mask = new MaskRect(rows, columns);
        // Canto superior esquerdo
        mask.setEnabled(0, 0, false);
        mask.setEnabled(0, 1, false);
        mask.setEnabled(1, 1, false);
        mask.setEnabled(1, 2, false);

        // Canto superior direito
        mask.setEnabled(0, columns - 1, false);
        mask.setEnabled(0, columns - 2, false);
        mask.setEnabled(1, columns - 2, false);
        mask.setEnabled(1, columns - 3, false);

        // Canto inferior esquerdo
        mask.setEnabled(rows - 1, 0, false);
        mask.setEnabled(rows - 1, 1, false);
        mask.setEnabled(rows - 2, 0, false);
        mask.setEnabled(rows - 2, 1, false);

        // Canto inferior direito
        mask.setEnabled(rows - 1, columns - 1, false);
        mask.setEnabled(rows - 1, columns - 2, false);
        mask.setEnabled(rows - 2, columns - 1, false);
        mask.setEnabled(rows - 2, columns - 2, false);

        // Centro
        int row = rows / 2 - 2;
        int col = columns / 2 - 2;
        mask.setEnabled(row, col, false);
        mask.setEnabled(row, col + 1, false);
        mask.setEnabled(row + 1, col, false);
        mask.setEnabled(row + 1, col + 1, false);
        return mask;
    }
}
