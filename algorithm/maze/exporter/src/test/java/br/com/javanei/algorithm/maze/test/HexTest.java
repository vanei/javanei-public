package br.com.javanei.algorithm.maze.test;

import br.com.javanei.algorithm.maze.GridHex;
import br.com.javanei.algorithm.maze.exporter.ImageExporter;
import br.com.javanei.algorithm.maze.generator.RecursiveBacktracker;
import br.com.javanei.algorithm.maze.solver.Distances;
import java.io.File;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HexTest extends AbstractTest {
    private GridHex gridHex;
    private RecursiveBacktracker generate;
    private Distances distances;
    private ImageExporter imageExporter;

    @Before
    public void setup() {
        gridHex = new GridHex(20, 20);
        generate = new RecursiveBacktracker();
        generate.generate(gridHex);
        distances = new Distances(gridHex.getCell(0, 0));
        imageExporter = new ImageExporter(gridHex, 20);
    }

    @Test
    public void testImage() throws Exception {
        imageExporter.saveImagePNG(imageExporter.export(), new File("target/Hex.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testImageFilled() throws Exception {
        imageExporter.saveImagePNG(imageExporter.export(distances), new File("target/HexFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testImagePath() throws Exception {
        imageExporter.saveImagePNG(imageExporter.export(distances, gridHex.getCell(19, 19)),
                new File("target/HexPath.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testMask() throws Exception {
        GridHex grid = new GridHex(getMaskRect(20, 20));
        RecursiveBacktracker rbt = new RecursiveBacktracker();
        rbt.generate(grid);
        Distances dist = new Distances(grid.getCell(0, 2));
        ImageExporter exporter = new ImageExporter(grid, 20);
        exporter.saveImagePNG(exporter.export(dist, grid.getCell(17, 19)),
                new File("target/MaskHex.png"));
    }
}
