package br.com.javanei.algorithm.maze.test;

import br.com.javanei.algorithm.maze.GridTriangle;
import br.com.javanei.algorithm.maze.exporter.ImageExporter;
import br.com.javanei.algorithm.maze.generator.RecursiveBacktracker;
import br.com.javanei.algorithm.maze.solver.Distances;
import java.io.File;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TriangleTest extends AbstractTest {
    private GridTriangle gridTriangle;
    private RecursiveBacktracker generate;
    private Distances distances;
    private ImageExporter imageExporter;

    @Before
    public void setup() {
        gridTriangle = new GridTriangle(20, 20);
        generate = new RecursiveBacktracker();
        generate.generate(gridTriangle);
        distances = new Distances(gridTriangle.getCell(0, 0));
        imageExporter = new ImageExporter(gridTriangle, 20);
    }

    @Test
    public void testImage() throws Exception {
        imageExporter.saveImagePNG(imageExporter.export(), new File("target/Triangle.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testImageFilled() throws Exception {
        imageExporter.saveImagePNG(imageExporter.export(distances), new File("target/TriangleFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testImagePath() throws Exception {
        imageExporter.saveImagePNG(imageExporter.export(distances, gridTriangle.getCell(19, 19)),
                new File("target/TrianglePath.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testMask() throws Exception {
        GridTriangle grid = new GridTriangle(getMaskRect(20, 20));
        RecursiveBacktracker rbt = new RecursiveBacktracker();
        rbt.generate(grid);
        Distances dist = new Distances(grid.getCell(0, 2));
        ImageExporter exporter = new ImageExporter(grid, 20);
        exporter.saveImagePNG(exporter.export(dist, grid.getCell(17, 19)),
                new File("target/MaskTriangle.png"));
    }
}
