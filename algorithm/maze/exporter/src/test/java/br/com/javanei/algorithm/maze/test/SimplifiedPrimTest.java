package br.com.javanei.algorithm.maze.test;

import br.com.javanei.algorithm.maze.GridRect;
import br.com.javanei.algorithm.maze.exporter.ImageExporter;
import br.com.javanei.algorithm.maze.exporter.StringExporter;
import br.com.javanei.algorithm.maze.generator.SimplifiedPrim;
import br.com.javanei.algorithm.maze.solver.Distances;
import java.io.File;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimplifiedPrimTest extends AbstractTest {
    private GridRect gridRect;
    private SimplifiedPrim generate;
    private Distances distances;
    private ImageExporter imageExporter;

    @Before
    public void init() {
        gridRect = new GridRect(20, 20);
        generate = new SimplifiedPrim();
        generate.generate(gridRect);
        distances = new Distances(gridRect.getCell(0, 0));
        imageExporter = new ImageExporter(gridRect, 20);
    }

    @Test
    public void testString() {
        System.out.print(new StringExporter(gridRect).export());
        Assert.assertTrue(true);
    }

    @Test
    public void testStringFilled() {
        System.out.print(new StringExporter(gridRect).export(distances));
        Assert.assertTrue(true);
    }

    @Test
    public void testStringPath() {
        System.out.print(new StringExporter(gridRect).export(distances, gridRect.getCell(19, 19)));
        Assert.assertTrue(true);
    }

    @Test
    public void testImagePath() {
        try {
            imageExporter.saveImagePNG(imageExporter.export(distances, gridRect.getCell(19, 19)),
                    new File("target/SimplifiedPrimPath.png"));
            Assert.assertTrue(true);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testImageColored() {
        try {
            imageExporter.saveImagePNG(imageExporter.export(distances),
                    new File("target/SimplifiedPrimColored.png"));
            Assert.assertTrue(true);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
    }
}
