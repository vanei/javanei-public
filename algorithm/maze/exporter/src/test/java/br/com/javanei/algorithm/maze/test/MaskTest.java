package br.com.javanei.algorithm.maze.test;

import br.com.javanei.algorithm.maze.GridRect;
import br.com.javanei.algorithm.maze.exporter.ImageExporter;
import br.com.javanei.algorithm.maze.exporter.StringExporter;
import br.com.javanei.algorithm.maze.generator.HuntAndKill;
import br.com.javanei.algorithm.maze.generator.Kruskals;
import br.com.javanei.algorithm.maze.generator.RecursiveBacktracker;
import br.com.javanei.algorithm.maze.generator.SimplifiedPrim;
import br.com.javanei.algorithm.maze.generator.TruePrim;
import br.com.javanei.algorithm.maze.solver.Distances;
import java.io.File;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MaskTest extends AbstractTest {
    //AldousBroder: NAO FUNCIONA
    //BinaryTree: NAO FUNCIONA
    //RecursiveDivision: NAO FUNCIONA
    //Wilsons: NAO GERA CERTO
    //Sidewinder: NAO GERA CERTO
    //WARN: Uso de máscaras não funciona direito.

    private GridRect hkgrid;
    private HuntAndKill hk;
    private Distances hkDistances;
    private StringExporter hkExporter;
    private ImageExporter hkImageExporter;
    private GridRect krgrid;
    private Kruskals kr;
    private Distances krdistances;
    private StringExporter krexporter;
    private ImageExporter krimageExporter;
    private GridRect spgrid;
    private SimplifiedPrim sprim;
    private Distances spdistances;
    private StringExporter spexporter;
    private ImageExporter spimageExporter;
    private GridRect tpgrid;
    private TruePrim tprim;
    private Distances tpdistances;
    private StringExporter tpexporter;
    private ImageExporter tpimageExporter;
    private GridRect rbgrid;
    private RecursiveBacktracker rb;
    private Distances rbdistances;
    private StringExporter rbexporter;
    private ImageExporter rbimageExporter;

    @Before
    public void setupHuntAndKill() {
        hkgrid = new GridRect(super.getMaskRect(20, 20));
        hk = new HuntAndKill();
        hk.generate(hkgrid);
        hkDistances = new Distances(hkgrid.getCell(0, 2));
        hkExporter = new StringExporter(hkgrid);
        hkImageExporter = new ImageExporter(hkgrid, 20);
    }

    @Test
    public void testHuntAndKillString() {
        System.out.println("HuntAndKill ==================================");
        System.out.println(hkExporter.export());
        Assert.assertTrue(true);
    }

    @Test
    public void testHuntAndKillStringFilled() {
        System.out.println("HuntAndKillFilled ==================================");
        System.out.println(hkExporter.export(hkDistances, hkDistances.getMostDistantCell()));
        Assert.assertTrue(true);
    }

    @Test
    public void testHuntAndKillStringPath() {
        System.out.println("HuntAndKillPath ==================================");
        System.out.println(hkExporter.export(hkDistances));
        Assert.assertTrue(true);
    }

    @Test
    public void testHuntAndKillMask() throws Exception {
        hkImageExporter.saveImagePNG(hkImageExporter.export(), new File("target/MaskHuntAndKill.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testHuntAndKillMaskFilled() throws Exception {
        hkImageExporter.saveImagePNG(hkImageExporter.export(hkDistances), new File("target/MaskHuntAndKillFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testHuntAndKillMaskPath() throws Exception {
        hkImageExporter.saveImagePNG(hkImageExporter.export(hkDistances, hkgrid.getCell(19, 17)), new File("target/MaskHuntAndKillPath.png"));
        Assert.assertTrue(true);
    }

    @Before
    public void setupKruskals() {
        krgrid = new GridRect(super.getMaskRect(20, 20));
        kr = new Kruskals();
        kr.generate(krgrid);
        krdistances = new Distances(krgrid.getCell(0, 2));
        krexporter = new StringExporter(krgrid);
        krimageExporter = new ImageExporter(krgrid, 20);
    }

    @Test
    public void testKruskalsString() {
        System.out.println("Kruskals ==================================");
        System.out.println(krexporter.export());
        Assert.assertTrue(true);
    }

    @Test
    public void testKruskalsStringFilled() {
        System.out.println("KruskalsFilled ==================================");
        System.out.println(krexporter.export(krdistances));
        Assert.assertTrue(true);
    }

    @Test
    public void testKruskalsStringPath() {
        System.out.println("KruskalsPath ==================================");
        System.out.println(krexporter.export(krdistances, krdistances.getMostDistantCell()));
        Assert.assertTrue(true);
    }

    @Test
    public void testKruskalsMask() throws Exception {
        krimageExporter.saveImagePNG(krimageExporter.export(), new File("target/MaskKruskals.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testKruskalsMaskFilled() throws Exception {
        krimageExporter.saveImagePNG(krimageExporter.export(krdistances), new File("target/MaskKruskalsFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testKruskalsMaskPath() throws Exception {
        krimageExporter.saveImagePNG(krimageExporter.export(krdistances, krgrid.getCell(19, 17)),
                new File("target/MaskKruskalsPath.png"));
        Assert.assertTrue(true);
    }

    @Before
    public void setupSimplifiedPrim() {
        spgrid = new GridRect(super.getMaskRect(20, 20));
        sprim = new SimplifiedPrim();
        sprim.generate(spgrid);
        spdistances = new Distances(spgrid.getCell(0, 2));
        spexporter = new StringExporter(spgrid);
        spimageExporter = new ImageExporter(spgrid, 20);
    }

    @Test
    public void testSimplifiedPrimString() {
        System.out.println("SimplifiedPrim ==================================");
        System.out.println(spexporter.export());
        Assert.assertTrue(true);
    }

    @Test
    public void testSimplifiedPrimStringFilled() {
        System.out.println("SimplifiedPrimFilled ==================================");
        System.out.println(spexporter.export(spdistances));
        Assert.assertTrue(true);
    }

    @Test
    public void testSimplifiedPrimStringPath() {
        System.out.println("SimplifiedPrimPath ==================================");
        System.out.println(spexporter.export(spdistances, spdistances.getMostDistantCell()));
        Assert.assertTrue(true);
    }

    @Test
    public void testSimplifiedPrimMask() throws Exception {
        spimageExporter.saveImagePNG(spimageExporter.export(), new File("target/MaskSimplified.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testSimplifiedPrimMaskFilled() throws Exception {
        spimageExporter.saveImagePNG(spimageExporter.export(spdistances), new File("target/MaskSimplifiedFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testSimplifiedPrimMaskPath() throws Exception {
        spimageExporter.saveImagePNG(spimageExporter.export(spdistances, spgrid.getCell(19, 17)),
                new File("target/MaskSimplifiedPath.png"));
        Assert.assertTrue(true);
    }

    @Before
    public void setupTruePrim() {
        tpgrid = new GridRect(super.getMaskRect(20, 20));
        tprim = new TruePrim();
        tprim.generate(tpgrid);
        tpdistances = new Distances(tpgrid.getCell(0, 2));
        tpexporter = new StringExporter(tpgrid);
        tpimageExporter = new ImageExporter(tpgrid, 20);
    }

    @Test
    public void testTruePrim() {
        System.out.println("TruePrim ==================================");
        System.out.println(tpexporter.export());
        Assert.assertTrue(true);
    }

    @Test
    public void testTruePrimFilled() {
        System.out.println("TruePrimFilled ==================================");
        System.out.println(tpexporter.export(tpdistances));
        Assert.assertTrue(true);
    }

    @Test
    public void testTruePrimPath() {
        System.out.println("TruePrimPath ==================================");
        System.out.println(tpexporter.export(tpdistances, tpgrid.getCell(19, 17)));
        Assert.assertTrue(true);
    }

    @Test
    public void testTruePrimMask() throws Exception {
        tpimageExporter.saveImagePNG(tpimageExporter.export(), new File("target/MaskTruePrim.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testTruePrimMaskFilled() throws Exception {
        tpimageExporter.saveImagePNG(tpimageExporter.export(tpdistances), new File("target/MaskTruePrimFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testTruePrimMaskPath() throws Exception {
        tpimageExporter.saveImagePNG(tpimageExporter.export(tpdistances, tpgrid.getCell(19, 17)),
                new File("target/MaskTruePrimPath.png"));
        Assert.assertTrue(true);
    }

    @Before
    public void setupRecursiveBacktracker() {
        rbgrid = new GridRect(super.getMaskRect(20, 20));
        rb = new RecursiveBacktracker();
        rb.generate(rbgrid);
        rbdistances = new Distances(rbgrid.getCell(0, 2));
        rbexporter = new StringExporter(rbgrid);
        rbimageExporter = new ImageExporter(rbgrid, 20);
    }

    @Test
    public void testRecursiveBacktrackerString() {
        System.out.println("RecursiveBacktracker ==================================");
        System.out.println(rbexporter.export());
        Assert.assertTrue(true);
    }

    @Test
    public void testRecursiveBacktrackerStringFilled() {
        System.out.println("RecursiveBacktrackerFilled ==================================");
        System.out.println(rbexporter.export(rbdistances));
        Assert.assertTrue(true);
    }

    @Test
    public void testRecursiveBacktrackerStringPath() {
        System.out.println("RecursiveBacktrackerPath ==================================");
        System.out.println(rbexporter.export(rbdistances, rbdistances.getMostDistantCell()));
        Assert.assertTrue(true);
    }

    @Test
    public void testRecursiveBacktrackerMask() throws Exception {
        rbimageExporter.saveImagePNG(rbimageExporter.export(), new File("target/MaskRecursiveBacktracker.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testRecursiveBacktrackerMaskFilled() throws Exception {
        rbimageExporter.saveImagePNG(rbimageExporter.export(rbdistances),
                new File("target/MaskRecursiveBacktrackerFilled.png"));
        Assert.assertTrue(true);
    }

    @Test
    public void testRecursiveBacktrackerMaskPath() throws Exception {
        rbimageExporter.saveImagePNG(rbimageExporter.export(rbdistances, rbdistances.getMostDistantCell()),
                new File("target/MaskRecursiveBacktrackerPath.png"));
        Assert.assertTrue(true);
    }
}
