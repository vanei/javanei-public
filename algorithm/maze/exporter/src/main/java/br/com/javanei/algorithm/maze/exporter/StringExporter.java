package br.com.javanei.algorithm.maze.exporter;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.CellRect;
import br.com.javanei.algorithm.maze.GridRect;
import br.com.javanei.algorithm.maze.solver.Distances;
import java.util.LinkedList;
import java.util.List;

public class StringExporter {
    private GridRect grid;

    public StringExporter(GridRect grid) {
        this.grid = grid;
    }

    public String export() {
        return this.export(null, null);
    }

    public String export(Distances distances) {
        return export(distances, null);
    }

    public String export(Distances distances, Cell goal) {
        StringBuilder sb = new StringBuilder();
        List<Cell> pathTo = new LinkedList<>();
        if (goal != null) {
            pathTo = distances.pathTo(goal);
        }

        int rows = grid.getRows();
        int columns = grid.getColumns();

        StringBuilder[] sbRows = new StringBuilder[rows];
        for (int row = 0; row < rows; row++) {
            if (row == 0) {
                CellRect cell = grid.getCell(0, 0);
                sb.append("+");
                for (int col = 0; col < columns; col++) {
                    cell = grid.getCell(row, col);
                    sb.append(cell.getNorth() != null && cell.isLinked(cell.getNorth()) ? "EEE" : "---");
                    sb.append("+");
                }
                sb.append("\n");
            }
            StringBuilder sbRowDivider = new StringBuilder();
            sbRowDivider.append("+");
            sbRows[row] = new StringBuilder();
            for (int col = 0; col < columns; col++) {
                CellRect cell = grid.getCell(row, col);
                sbRowDivider.append(cell.getSouth() != null && cell.isLinked(cell.getSouth()) ? "   " : "---");
                sbRowDivider.append("+");
                if (col == 0) {
                    sbRows[row].append(cell.getWest() != null && cell.isLinked(cell.getWest()) ? "E" : "|");
                }
                if (cell.isEnabled) {
                    if (distances != null && (goal == null || pathTo.contains(cell))) {
                        sbRows[row].append(distances.getContentsOf(cell));
                    } else {
                        sbRows[row].append("   ");
                    }
                } else {
                    sbRows[row].append("XXX");
                }
                sbRows[row].append(cell.getEast() != null && cell.isLinked(cell.getEast()) ? " " : "|");
            }
            sb.append(sbRows[row]).append("\n").append(sbRowDivider).append("\n");
        }

        return sb.toString();
    }
}
