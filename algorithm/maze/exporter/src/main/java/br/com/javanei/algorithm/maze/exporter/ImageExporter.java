package br.com.javanei.algorithm.maze.exporter;

import br.com.javanei.algorithm.maze.Cell;
import br.com.javanei.algorithm.maze.CellHex;
import br.com.javanei.algorithm.maze.CellRect;
import br.com.javanei.algorithm.maze.CellTriangle;
import br.com.javanei.algorithm.maze.GridHex;
import br.com.javanei.algorithm.maze.GridRect;
import br.com.javanei.algorithm.maze.GridTriangle;
import br.com.javanei.algorithm.maze.solver.Distances;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;

public class ImageExporter {
    private GridRect gridRect;
    private GridHex gridHex;
    private GridTriangle gridTriangle;
    private int cellSize;

    public ImageExporter(GridRect gridRect) {
        this(gridRect, 10);
    }

    public ImageExporter(GridRect gridRect, int cellSize) {
        this.gridRect = gridRect;
        this.cellSize = cellSize;
    }

    public ImageExporter(GridHex gridHex) {
        this(gridHex, 10);
    }

    public ImageExporter(GridHex gridHex, int cellSize) {
        this.gridHex = gridHex;
        this.cellSize = cellSize;
    }

    public ImageExporter(GridTriangle gridTriangle) {
        this(gridTriangle, 10);
    }

    public ImageExporter(GridTriangle gridTriangle, int cellSize) {
        this.gridTriangle = gridTriangle;
        this.cellSize = cellSize;
    }

    public BufferedImage export() {
        return this.export(null, null);
    }

    public BufferedImage export(Distances distances) {
        return export(distances, null);
    }

    public BufferedImage export(Distances distances, Cell goal) {
        if (this.gridTriangle != null) {
            return this.exportTriangle(distances, goal);
        } else if (this.gridHex != null) {
            return this.exportHex(distances, goal);
        } else {
            return this.exportRect(distances, goal);
        }
    }

    private BufferedImage exportTriangle(Distances distances, Cell goal) {
        List<Cell> pathTo = new LinkedList<>();
        if (goal != null) {
            pathTo = distances.pathTo(goal);
        }

        double half_width = cellSize / 2.0;
        double height = cellSize * Math.sqrt(3) / 2.0;
        double half_height = height / 2.0;
        int imgWidth = (int) Math.round(cellSize * (this.gridTriangle.getColumns() + 1) / 2.0);
        int imgHeight = (int) Math.round(height * this.gridTriangle.getRows());

        Color background = Color.WHITE;
        Color wall = Color.BLACK;

        BufferedImage image = new BufferedImage(imgWidth + 1, imgHeight + 1, BufferedImage.TYPE_INT_RGB);
        Graphics2D img = image.createGraphics();
        img.setBackground(background);
        img.clearRect(0, 0, imgWidth, imgHeight);

        for (int mode = 0; mode <= 1; mode++) {
            for (Cell c : this.gridTriangle.getAllCells()) {
                CellTriangle cell = (CellTriangle) c;
                double cx = half_width + cell.getColumn() * half_width;
                double cy = half_height + cell.getRow() * height;
                int west_x = (int) Math.round(cx - half_width);
                int mid_x = (int) Math.round(cx);
                int east_x = (int) Math.round(cx + half_width);

                int base_y;
                int apex_y;
                if (cell.isUpright()) {
                    apex_y = (int) Math.round(cy - half_height);
                    base_y = (int) Math.round(cy + half_height);
                } else {
                    apex_y = (int) Math.round(cy + half_height);
                    base_y = (int) Math.round(cy - half_height);
                }

                if (mode == 0) {
                    // Desenhando background
                    Color color = goal == null ? getBackgroundColorFor(cell, distances) : getBackgroundColorFor(cell, pathTo);
                    img.setColor(color != null ? color : background);

                    int[] xp = new int[]{west_x, mid_x, east_x};
                    int[] yp = new int[]{base_y, apex_y, base_y};
                    img.fillPolygon(xp, yp, xp.length);
                } else {
                    // Desenhando wall
                    img.setColor(wall);

                    if (cell.getWest() == null)
                        img.drawLine(west_x, base_y, mid_x, apex_y);
                    if (cell.getEast() == null || !cell.isLinked(cell.getEast()))
                        img.drawLine(east_x, base_y, mid_x, apex_y);
                    boolean no_south = cell.isUpright() && cell.getSouth() == null;
                    boolean not_linked = !cell.isUpright() &&
                            (cell.getNorth() == null || !cell.isLinked(cell.getNorth()));
                    if (no_south || not_linked) {
                        img.drawLine(east_x, base_y, west_x, base_y);
                    }
                }
            }
        }
        return image;
    }

    private BufferedImage exportHex(Distances distances, Cell goal) {
        List<Cell> pathTo = new LinkedList<>();
        if (goal != null) {
            pathTo = distances.pathTo(goal);
        }

        double a_size = cellSize / 2.0;
        double b_size = cellSize * Math.sqrt(3) / 2.0;
        //double width = cellSize * 2;
        double height = b_size * 2;

        int imgWidth = (int) Math.round(3 * a_size * this.gridHex.getColumns() + a_size + 0.5);
        int imgHeight = (int) Math.round(height * this.gridHex.getRows() + b_size + 0.5);

        Color background = Color.WHITE;
        Color wall = Color.BLACK;

        BufferedImage image = new BufferedImage(imgWidth + 1, imgHeight + 1, BufferedImage.TYPE_INT_RGB);
        Graphics2D img = image.createGraphics();
        img.setBackground(background);
        img.clearRect(0, 0, imgWidth, imgHeight);

        for (int mode = 0; mode <= 1; mode++) {
            for (Cell c : this.gridHex.getAllCells()) {
                CellHex cell = (CellHex) c;
                double cx = cellSize + 3 * cell.getPosition().getCol() * a_size;
                double cy = b_size + cell.getPosition().getRow() * height;
                if (cell.getPosition().getCol() % 2 != 0)
                    cy += b_size;

                // f/n = far/near
                // n/s/e/w = north/south/east/west
                int x_fw = (int) Math.round(cx - cellSize);
                int x_nw = (int) Math.round(cx - a_size);
                int x_ne = (int) Math.round(cx + a_size);
                int x_fe = (int) Math.round(cx + cellSize);

                // m = middle
                int y_n = (int) Math.round(cy - b_size);
                int y_m = (int) Math.round(cy);
                int y_s = (int) Math.round(cy + b_size);

                if (mode == 0) {
                    // Desenhando background
                    Color color = (goal == null ? getBackgroundColorFor(cell, distances) : getBackgroundColorFor(cell, pathTo));
                    img.setColor(color != null ? color : background);
                    int[] xp = new int[]{x_fw, x_nw, x_ne, x_fe, x_ne, x_nw};
                    int[] yp = new int[]{y_m, y_n, y_n, y_m, y_s, y_s};
                    img.fillPolygon(xp, yp, xp.length);
                } else {
                    // Desenhando wall
                    img.setColor(wall);
                    if (cell.getSouthwest() == null)
                        img.drawLine(x_fw, y_m, x_nw, y_s);
                    if (cell.getNorthwest() == null)
                        img.drawLine(x_fw, y_m, x_nw, y_n);
                    if (cell.getNorth() == null)
                        img.drawLine(x_nw, y_n, x_ne, y_n);
                    if (cell.getNortheast() == null || !cell.isLinked(cell.getNortheast()))
                        img.drawLine(x_ne, y_n, x_fe, y_m);
                    if (cell.getSoutheast() == null || !cell.isLinked(cell.getSoutheast()))
                        img.drawLine(x_fe, y_m, x_ne, y_s);
                    if (cell.getSouth() == null || !cell.isLinked(cell.getSouth()))
                        img.drawLine(x_ne, y_s, x_nw, y_s);
                }
            }
        }
        return image;
    }

    private BufferedImage exportRect(Distances distances, Cell goal) {
        List<Cell> pathTo = new LinkedList<>();
        if (goal != null) {
            pathTo = distances.pathTo(goal);
        }

        int imgWidth = this.cellSize * this.gridRect.getColumns();
        int imgHeight = this.cellSize * this.gridRect.getRows();

        Color background = Color.WHITE;
        Color wall = Color.BLACK;

        BufferedImage image = new BufferedImage(imgWidth + 1, imgHeight + 1, BufferedImage.TYPE_INT_RGB);
        Graphics2D img = image.createGraphics();
        img.setBackground(background);
        img.clearRect(0, 0, imgWidth, imgHeight);

        for (int mode = 0; mode <= 1; mode++) {
            for (Cell c : this.gridRect.getAllCells()) {
                if (c == null)
                    continue;
                CellRect cell = (CellRect) c;
                int x1 = cell.getColumn() * this.cellSize;
                int y1 = cell.getRow() * this.cellSize;
                int x2 = (cell.getColumn() + 1) * this.cellSize;
                int y2 = (cell.getRow() + 1) * this.cellSize;

                if (mode == 0) {
                    // Desenhando background
                    Color color = goal == null ? getBackgroundColorFor(cell, distances) : getBackgroundColorFor(cell, pathTo);
                    img.setBackground(color != null ? color : background);
                    img.clearRect(x1, y1, x2, y2);
                } else {
                    // Desenhando wall
                    img.setColor(wall);
                    if (cell.getNorth() == null)
                        img.drawLine(x1, y1, x2, y1);
                    if (cell.getWest() == null)
                        img.drawLine(x1, y1, x1, y2);
                    if (cell.getEast() == null || !cell.isLinked(cell.getEast()))
                        img.drawLine(x2, y1, x2, y2);
                    if (cell.getSouth() == null || !cell.isLinked(cell.getSouth()))
                        img.drawLine(x1, y2, x2, y2);
                }
            }
        }

        return image;
    }

    private Color getBackgroundColorFor(Cell cell, List<Cell> path) {
        if (!cell.isEnabled)
            return Color.BLACK;
        return path.contains(cell) ? Color.GREEN : null;
    }

    private Color getBackgroundColorFor(Cell cell, Distances distances) {
        if (!cell.isEnabled)
            return Color.BLACK;
        else if (distances == null)
            return null;
        Integer dist = distances.getDistance(cell);
        float intensity = ((float) (distances.getMaxDistance() - dist)) / distances.getMaxDistance();
        int dark = Math.round(255 * intensity);
        int bright = Math.round(128 + (127 * intensity));
        return new Color(dark, bright, dark);
    }

    public void saveImagePNG(BufferedImage image, File file) throws IOException {
        ImageIO.write(image, "PNG", file);
    }
}
